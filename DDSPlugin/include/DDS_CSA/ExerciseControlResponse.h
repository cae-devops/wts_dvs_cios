/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include <string>
#include "ExerciseControlResponseExport.h"


#define TASK_EXERCISE_CONTROL_RESPONSE "ExerciseControlResponse"



class ExerciseControlResponse
{

public:

	virtual void LoadExerciseResponse(const bool& success) = 0;
	virtual void StartExerciseResponse(const bool& success) = 0;
	virtual void StopExerciseResponse(const bool& success) = 0;
	virtual void PauseExerciseResponse(const bool& success) = 0;
	virtual void ResumeExerciseResponse(const bool& success) = 0;
	virtual void LoadVoiceCommandResponse(const bool& success) = 0;
	virtual void StartTrainingSessionResponse(const bool& success, const unsigned long messageLength, const std::string& message) = 0;
	
};