/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "TurretControlExport.h"

#define TASK_TURRET_CONTROL "TurretControl"


class TurretControl
{

public:

	virtual void UpdateTurretHeading(const unsigned long& entityId, const double& heading) = 0;
	virtual void FireLaser(const unsigned long& entityId) = 0;
	virtual void UpdateWeaponElevation(const unsigned long& entityId, const unsigned long& weaponId, const double& elevation) = 0;
	//To Do - Work out if we need an entityId and how to populate 
	virtual void RestockAmmunition(/*const unsigned long& entityId,*/ const unsigned long& ammoTypeId) = 0;
	virtual void SetFixedGunError(/*const unsigned long& entityId,*/ const double& azimuthError, const double& targetRange, const double& requiredRange) = 0;
};