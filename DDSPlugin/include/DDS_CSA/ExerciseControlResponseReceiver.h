//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ExerciseControlResponseReceiverExport.h"

class ExerciseControlResponse;
class DDSDomainParticipant;

class _EXERCISE_CONTROL_RESPONSE_RECEIVER_BUILD_MODE_ ExerciseControlResponseReceiver
{
public:

	ExerciseControlResponseReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~ExerciseControlResponseReceiver(void);

	void Initialise(ExerciseControlResponse* exerciseControlResponse);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

