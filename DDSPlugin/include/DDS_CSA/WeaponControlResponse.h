/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "WeaponControlResponseExport.h"


#define TASK_WEAPON_CONTROL_RESPONSE "WeaponControlResponse"


class WeaponControlResponse
{

public:

	virtual void CreateWeaponResponse(const unsigned long& requestId, const bool& success, const unsigned long& weaponId) = 0;

};