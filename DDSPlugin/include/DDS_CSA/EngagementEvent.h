/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "EngagementEventExport.h"

#define TASK_ENGAGEMENT_EVENT "EngagementEvent"


class EngagementEvent
{
public:
	virtual void AimingMarkAlignedWithEntity(const unsigned long& entityId) = 0;
	virtual void CentralHitAchievedOnEntity(const unsigned long& entityId) = 0;
	virtual void BurstFiredAtEntity(const unsigned long& entityId) = 0;
	virtual void BurstsFiredAtEntity(const unsigned long& entityId, const unsigned short& burstCount) = 0;
	virtual void LaserFiredAtEntity(const unsigned long& entityId) = 0;
	virtual void LasesFiredAtEntity(const unsigned long& entityId, const unsigned short& laseCount) = 0;
};