/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "StealthUpdateExport.h"


#define TASK_STEALTH_UPDATE "StealthUpdate"

class StealthUpdate
{

public:

	typedef struct
	{
		double ECEF_X;
		double ECEF_Y;
		double ECEF_Z;
	} S_ECEF_POSITION;

	//Height above ground is in metres, heading is in mils?, pitch is in ??
	virtual void StealthBodyUpdate(const S_ECEF_POSITION& position, const double& heading, const double& pitch) = 0;
	virtual void StealthCameraHeadingUpdate(const double& heading) = 0;

};