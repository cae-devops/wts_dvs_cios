/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "MissionFunctionRequestExport.h"
#include <vector>
#define TASK_MISSION_FUNCTION_REQUEST "MissionFunctionRequest"


class MissionFunctionRequest
{
public:

	typedef struct
	{
		double ENU_X;
		double ENU_Y;
		double ENU_Z;
	} S_ENU_POSITION;
	
	typedef struct
	{
		unsigned long requestId;
		S_ENU_POSITION endPosition;
	} REQUEST;

	//Not sure we need the ElementIds? how are these used? Need to check with Jem/Nicole
	virtual void LineOfSightRequest(const unsigned long& requestId, 
		const unsigned long& startElementId, 
		const S_ENU_POSITION& startPosition, 
		const unsigned long& endElementId, 
		const S_ENU_POSITION& endPosition) = 0;

	virtual void MultiLineOfSightRequest(const unsigned long& requestId, 
		const unsigned long& startElementId, 
		const S_ENU_POSITION& startPosition, 
		const unsigned long& endElementId, 
		const unsigned long& noOfLOSReqIds, 
		const REQUEST *requests) = 0;

	virtual void CancelLineOfSightRequest(const unsigned long& numOfRequests, 
		unsigned long *requestIds) = 0;

};
