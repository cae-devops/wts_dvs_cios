/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "EntityUpdateExport.h"

#define TASK_ENTITY_UPDATE "EntityUpdate"


class EntityUpdate
{
public:

	typedef struct
	{
		double ENU_X;
		double ENU_Y;
		double ENU_Z;
	} S_ENU_POSITION;

	typedef struct
	{
		double heading;
		double pitch;
		double roll;
	} S_ENU_ORIENTATION;

	virtual void EntityStatus(const unsigned long& entityId, const unsigned short& statusId) = 0;
	virtual void EntityBodyUpdate(const unsigned long& entityId, const double& speed, const S_ENU_POSITION& position, const S_ENU_ORIENTATION& orientation) = 0;

};