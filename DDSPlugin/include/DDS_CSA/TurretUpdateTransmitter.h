//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "TurretUpdateTransmitterExport.h"
#include "TurretUpdate.h"

 
class DDSDomainParticipant;

class _TURRET_UPDATE_TRANSMITTER_BUILD_MODE_ TurretUpdateTransmitter : public TurretUpdate
{
public:

	TurretUpdateTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~TurretUpdateTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Turret Update API
	void CurrentGunDirector(const unsigned long& entityId, const unsigned long& gunDirectorId) override;
	void TurretHeading(const unsigned long& entityId, const double& heading) override;
	void AmmoStockLevel(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& stockLevel) override;


private:

	DDSDomainParticipant* transmitterParticipant;
};

