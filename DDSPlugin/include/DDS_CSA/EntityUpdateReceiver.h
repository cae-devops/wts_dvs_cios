//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "EntityUpdateReceiverExport.h"

class EntityUpdate;
class DDSDomainParticipant;

class _ENTITY_UPDATE_RECEIVER_BUILD_MODE_ EntityUpdateReceiver
{
public:

	EntityUpdateReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~EntityUpdateReceiver(void);

	void Initialise(EntityUpdate* entityControl);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

