//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "StealthUpdateReceiverExport.h"

class StealthUpdate;
class DDSDomainParticipant;

class _STEALTH_UPDATE_RECEIVER_BUILD_MODE_ StealthUpdateReceiver
{
public:

	StealthUpdateReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~StealthUpdateReceiver(void);

	void Initialise(StealthUpdate* stealthUpdate);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

