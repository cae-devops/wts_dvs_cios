//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "WeaponControlResponseReceiverExport.h"

class WeaponControlResponse;
class DDSDomainParticipant;

class _WEAPON_CONTROL_RECEIVER_BUILD_MODE_ WeaponControlResponseReceiver
{
public:

	WeaponControlResponseReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~WeaponControlResponseReceiver(void);

	void Initialise(WeaponControlResponse* weaponControl);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

