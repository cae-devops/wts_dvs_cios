//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "TurretControlReceiverExport.h"

class TurretControl;
class DDSDomainParticipant;

class _TURRET_CONTROL_RECEIVER_BUILD_MODE_ TurretControlReceiver
{
public:

	TurretControlReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~TurretControlReceiver(void);

	void Initialise(TurretControl* turretControl);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

