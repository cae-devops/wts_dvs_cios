/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "ActiveEntityStatusUpdateExport.h"

#define TASK_ACTIVE_ENTITY_STATUS_UPDATE "ActiveEntityStatusUpdate"


class ActiveEntityStatusUpdate
{
public:

	virtual void EntityDamageStatus(const unsigned long& entityId, const unsigned short& damageStatusId) = 0;
	virtual void EntityEngagementStatus(const unsigned long& entityId, const unsigned short& engagementStatusId) = 0;
};