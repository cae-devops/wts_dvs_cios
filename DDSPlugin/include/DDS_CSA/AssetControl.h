/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "AssetControlExport.h"

#define TASK_ASSET_CONTROL "AssetControl"


class AssetControl
{
public:

	virtual void Shutdown(void) = 0;
};