//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "EngagementEventTransmitterExport.h"
#include "EngagementEvent.h"
#include <vector>

class DDSDomainParticipant;


class _ENGAGEMENT_EVENT_TRANSMITTER_BUILD_MODE_ EngagementEventTransmitter : public EngagementEvent
{
public:

	EngagementEventTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~EngagementEventTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

	
	//Engagement Event API
	void AimingMarkAlignedWithEntity(const unsigned long& entityId);
	void CentralHitAchievedOnEntity(const unsigned long& entityId);
	void BurstFiredAtEntity(const unsigned long& entityId);
	void BurstsFiredAtEntity(const unsigned long& entityId, const unsigned short& burstCount);
	void LaserFiredAtEntity(const unsigned long& entityId);
	void LasesFiredAtEntity(const unsigned long& entityId, const unsigned short& laseCount);

private:

	DDSDomainParticipant* transmitterParticipant;
};

