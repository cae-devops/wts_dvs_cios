//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "WeaponUpdateReceiverExport.h"

class WeaponUpdate;
class DDSDomainParticipant;

class _WEAPON_UPDATE_RECEIVER_BUILD_MODE_ WeaponUpdateReceiver
{
public:

	WeaponUpdateReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~WeaponUpdateReceiver(void);

	void Initialise(WeaponUpdate* weaponUpdate);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

