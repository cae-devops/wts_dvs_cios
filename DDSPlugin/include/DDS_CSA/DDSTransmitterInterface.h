
#include "DDSTransmitterInterfaceExport.h"

//Transmitters
class DDSDomainParticipant;
class ActiveEntityStatusUpdateTransmitter;
class ControlStatusTransmitter;
class EntityControlTransmitter;
class EntityUpdateTransmitter;           
class ExerciseControlTransmitter;  
class ExerciseControlResponseTransmitter; 
class ExerciseControlResponseTransmitter;
class ExerciseGenerationTransmitter;
class ExerciseGenerationResponseTransmitter;
class StealthControlTransmitter;
class StealthUpdateTransmitter;
class TurretUpdateTransmitter;
class TurretControlTransmitter;
class WeaponControlTransmitter;
class WeaponControlResponseTransmitter;
class WeaponUpdateTransmitter;
class AssetControlTransmitter;
class ProjectileUpdateTransmitter;
class MissionFunctionRequestTransmitter;
class MissionFunctionResponseTransmitter;
class RemoteExerciseControlTransmitter;
class EngagementEventTransmitter;
class SightUpdateTransmitter;


class _DDS_DOMAIN_PART_TRANS_BUILD_MODE_ DDSTransmitterInterface
{

public:

	DDSTransmitterInterface(void);
	~DDSTransmitterInterface(void) {};


	// Get transmitter objects
	ActiveEntityStatusUpdateTransmitter*   GetActiveEntityStatusUpdateTransmitter();
	ControlStatusTransmitter*              GetControlStatusTransmitter();
	EntityControlTransmitter*              GetEntityControlTransmitter();
	EntityUpdateTransmitter*               GetEntityUpdateTransmitter();
	ExerciseControlTransmitter*            GetExerciseControlTransmitter();
	ExerciseControlResponseTransmitter*    GetExerciseControlResponseTransmitter();
	ExerciseGenerationTransmitter*         GetExerciseGenerationTransmitter();
	ExerciseGenerationResponseTransmitter* GetExerciseGenerationResponseTransmitter();
	StealthControlTransmitter*             GetStealthControlTransmitter();
	StealthUpdateTransmitter*              GetStealthUpdateTransmitter();
	TurretUpdateTransmitter*               GetTurretUpdateTransmitter();
	TurretControlTransmitter*              GetTurretControlTransmitter();
	WeaponControlTransmitter*              GetWeaponControlTransmitter();
	WeaponControlResponseTransmitter*      GetWeaponControlResponseTransmitter();
	WeaponUpdateTransmitter*               GetWeaponUpdateTransmitter();
	AssetControlTransmitter*			   GetAssetControlTransmitter();
	ProjectileUpdateTransmitter*		   GetProjectileUpdateTransmitter();
	MissionFunctionRequestTransmitter*	   GetMissionFunctionRequestTransmitter();
	MissionFunctionResponseTransmitter*	   GetMissionFunctionResponseTransmitter();
	RemoteExerciseControlTransmitter*	   GetRemoteExerciseControlTransmitter();
	EngagementEventTransmitter*	           GetEngagementEventTransmitter();
	SightUpdateTransmitter*				   GetSightUpdateTransmitter();


	int TransmitterParticipantShutdown(void);

private:

	unsigned long domainId;

	DDSDomainParticipant* transmitterParticipant;  // The one and only -- for transmitters

	ActiveEntityStatusUpdateTransmitter*   activeEntityStatusUpdateTransmitter;
	ControlStatusTransmitter*              controlStatusTransmitter;
	EntityControlTransmitter*              entityControlTransmitter;
	EntityUpdateTransmitter*               entityUpdateTransmitter;
	ExerciseControlTransmitter*            exerciseControlTransmitter;
	ExerciseControlResponseTransmitter*    exerciseControlResponseTransmitter;
	ExerciseGenerationTransmitter*         exerciseGenerationTransmitter;
	ExerciseGenerationResponseTransmitter* exerciseGenerationResponseTransmitter;
	StealthControlTransmitter*             stealthControlTransmitter;
	StealthUpdateTransmitter*              stealthUpdateTransmitter;
	TurretControlTransmitter*              turretControlTransmitter;
	TurretUpdateTransmitter*               turretUpdateTransmitter;
	WeaponControlTransmitter*              weaponControlTransmitter;
	WeaponControlResponseTransmitter*      weaponControlResponseTransmitter;
	WeaponUpdateTransmitter*               weaponUpdateTransmitter;
	AssetControlTransmitter*			   assetControlTransmitter;
	ProjectileUpdateTransmitter*		   projectileUpdateTransmitter;
	MissionFunctionRequestTransmitter*	   missionFunctionRequestTransmitter;
	MissionFunctionResponseTransmitter*	   missionFunctionResponseTransmitter;
	RemoteExerciseControlTransmitter*	   remoteExerciseControlTransmitter;
	EngagementEventTransmitter*	           engagementEventTransmitter;
	SightUpdateTransmitter*				   sightUpdateTransmitter;

};

