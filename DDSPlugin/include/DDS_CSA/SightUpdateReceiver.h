//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "SightUpdateReceiverExport.h"

class SightUpdate;
class DDSDomainParticipant;

class _SIGHT_UPDATE_RECEIVER_BUILD_MODE_ SightUpdateReceiver
{
public:

	SightUpdateReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~SightUpdateReceiver(void);

	void Initialise(SightUpdate* sightUpdate);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

