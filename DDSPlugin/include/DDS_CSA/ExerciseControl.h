/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include <string>
#include <vector>
#include "ExerciseControlExport.h"


#define TASK_EXERCISE_CONTROL "ExerciseControl"



class ExerciseControl
{

public:

	virtual void LoadExercise(const std::string& exerciseName, const std::string& exerciseFile) = 0;
	virtual void StartExercise(void) = 0;
	virtual void StopExercise(void) = 0;
	virtual void PauseExercise(void) = 0;
	virtual void ResumeExercise(void) = 0;
	virtual void LoadVoiceCommand(const std::string& voiceCommandName, const std::string& voiceCommandFile) = 0;
	virtual void StartExerciseGeneration(void) = 0;
	virtual void LeaveExerciseGeneration(void) = 0;
	virtual void StartTrainingSession(void) = 0;
	virtual void LeaveTrainingSession(void) = 0;
	virtual void CourseComplete(void) = 0;
	virtual void PhaseComplete(void) = 0;
	virtual void DisplayMessage(const std::string& message) = 0;
	virtual void CGFOwnership(const unsigned long& ovEntityId, const unsigned long& numOfEntityIds, const unsigned long *entityId) = 0;
	//virtual void CGFOwnership(const unsigned long& ovEntityId, const std::vector<unsigned long>& entityId) = 0;
};