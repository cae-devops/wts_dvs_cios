/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "WeaponUpdateExport.h"
#include <vector>


#define TASK_WEAPON_UPDATE "WeaponUpdate"


typedef struct
{
	double heading;
	double pitch;
	double roll;
	double x;
	double y;
	double z;
} S_POSITION_AND_ORIENTATION;



class WeaponUpdate
{

public:

	virtual void WeaponSelection(const unsigned long& entityId, const unsigned long& weaponTypeId, const unsigned long& ammoTypeId) = 0;
	virtual void WeaponFire(const unsigned long& entityId, const unsigned long& weaponTypeId, const unsigned long& ammoTypeId, const unsigned long& projectileId) = 0;
	virtual void BurstLength(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& numOfProjctileIds, const unsigned long *projectileIds) = 0;
};