//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "EntityControlReceiverExport.h"

class EntityControl;
class DDSDomainParticipant;

class _ENTITY_CONTROL_RECEIVER_BUILD_MODE_ EntityControlReceiver
{
public:

	EntityControlReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~EntityControlReceiver(void);

	void Initialise(EntityControl* entityControl);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

