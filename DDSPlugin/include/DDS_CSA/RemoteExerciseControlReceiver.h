//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "RemoteExerciseControlReceiverExport.h"

class RemoteExerciseControl;
class DDSDomainParticipant;

class _REMOTE_EXERCISE_CONTROL_RECEIVER_BUILD_MODE_ RemoteExerciseControlReceiver
{
public:

	RemoteExerciseControlReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~RemoteExerciseControlReceiver(void);

	void Initialise(RemoteExerciseControl* remoteExerciseControl);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

