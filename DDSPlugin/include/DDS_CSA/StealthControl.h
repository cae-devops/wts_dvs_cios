/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "StealthControlExport.h"


#define TASK_STEALTH_CONTROL "StealthControl"


typedef struct
{
	double ECEF_X;
	double ECEF_Y;
	double ECEF_Z;
} S_ECEF_POSITION;


// This API assumes that there is a presentation of a stealth entity with an independant body and camera
// The camera is attached to the stealth body but can be independantly rotated, pitched and elevated above the body
// The stealth can also be attached to a specific sight view on an entity  

class StealthControl
{

public:


	//How do we map entity IDs?
	virtual void AttachViewToStealthEntity(void) = 0;
	virtual void AttachViewToGunnersSightOnEntity(const unsigned short& entityId) = 0;
	virtual void AttachViewToCommandersSightOnEntity(const unsigned short& entityId) = 0;
	
	//Rates are values between -1 and +1. -ve values identify an anticlockwise heading change or a pitch downwards.
	//Speed values are also between -1 and 1.  These are all mapped to max/min values at the receiver
	//Height above ground is in metres, heading is in mils
	virtual void UpdateStealthBodyControlDemands(const double& requiredSpeed, const double& headingRate, const double& pitchRate) = 0;
	virtual void RepositionStealthBody(const S_ECEF_POSITION& position, const double& heading) = 0;
	virtual void UpdateStealthCameraControlDemands(const double& headingRate, const double& pitchRate) = 0;
	virtual void AlignStealthCameraToHeading(const double& heading) = 0;
	virtual void AlignStealthCameraToLocation(const S_ECEF_POSITION& position) = 0;

	// Stealth body follows the terrain or can fly 
	virtual void EnableTerrainFollowingMode(void) = 0; 
	virtual void DisableTerrainFollowingMode(void) = 0;
	
	virtual void UpdateStealthCameraElevationOffset(const double& heightOffset) = 0;  //where 0 = no elevation offset and 1 = maximum elevation offset
	virtual void UpdateStealthCameraZoom(const double& zoomLevel) = 0;  //where 0 = no zoom and 1 = max zoom available

};