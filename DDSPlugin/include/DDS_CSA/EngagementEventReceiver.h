//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "EngagementEventReceiverExport.h"

class EngagementEvent;
class DDSDomainParticipant;

class _ENGAGEMENT_EVENT_RECEIVER_BUILD_MODE_ EngagementEventReceiver
{
public:

	EngagementEventReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~EngagementEventReceiver(void);

	void Initialise(EngagementEvent* engagementEvent);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

