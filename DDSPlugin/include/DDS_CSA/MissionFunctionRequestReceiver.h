//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "MissionFunctionRequestReceiverExport.h"

class MissionFunctionRequest;
class DDSDomainParticipant;

class _MISSION_FUNCTION_REQUEST_RECEIVER_BUILD_MODE_ MissionFunctionRequestReceiver
{
public:

	MissionFunctionRequestReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~MissionFunctionRequestReceiver(void);

	void Initialise(MissionFunctionRequest* missionFunctionRequest);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

