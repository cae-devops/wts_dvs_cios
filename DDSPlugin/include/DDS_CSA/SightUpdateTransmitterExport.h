/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once


#ifdef _CAEI_BUILD_DLL_
	#ifdef  _SIGHT_UPDATE_TRANSMITTER_BUILD_
		#define _SIGHT_UPDATE_TRANSMITTER_BUILD_MODE_ __declspec(dllexport)
	#else
		#define _SIGHT_UPDATE_TRANSMITTER_BUILD_MODE_ __declspec(dllimport)
	#endif
#else
	#define _SIGHT_UPDATE_TRANSMITTER_BUILD_MODE_
#endif
