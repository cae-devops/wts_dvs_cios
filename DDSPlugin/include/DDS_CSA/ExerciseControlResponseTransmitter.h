//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ExerciseControlResponseTransmitterExport.h"
#include "ExerciseControlResponse.h"
#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"

class DDSDomainParticipant;

class _EXERCISE_CONTROL_RESPONSE_TRANSMITTER_BUILD_MODE_ ExerciseControlResponseTransmitter: public ExerciseControlResponse
{
public:

	ExerciseControlResponseTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~ExerciseControlResponseTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

	//Exercise Control Response API
	void LoadExerciseResponse(const bool& success) override;
	void StartExerciseResponse(const bool& success) override;
	void PauseExerciseResponse(const bool& success) override;
	void ResumeExerciseResponse(const bool& success) override;
	void StopExerciseResponse(const bool& success) override;
	void LoadVoiceCommandResponse(const bool& success) override;
	void StartTrainingSessionResponse(const bool& success, const unsigned long messageLength, const std::string& message) override; //  override; need to redo .idl file
		
private:

	DDSDomainParticipant* transmitterParticipant;
};

