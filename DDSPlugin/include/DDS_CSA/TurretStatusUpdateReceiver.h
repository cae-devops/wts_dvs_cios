//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "TurretStatusUpdateReceiverExport.h"

class TurretStatusUpdate;

class _TURRET_STATUS_UPDATE_RECEIVER_BUILD_MODE_ TurretStatusUpdateReceiver
{
public:

	TurretStatusUpdateReceiver(TurretStatusUpdate* turretStatusUpdate);
	virtual ~TurretStatusUpdateReceiver(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

private:

	TurretStatusUpdate* turretStatusUpdate;
};

