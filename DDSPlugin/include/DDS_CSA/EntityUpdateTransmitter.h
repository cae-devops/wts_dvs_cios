//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "EntityUpdateTransmitterExport.h"
#include "EntityUpdate.h"

class DDSDomainParticipant;

class _ENTITY_UPDATE_TRANSMITTER_BUILD_MODE_ EntityUpdateTransmitter : public EntityUpdate
{
public:

	EntityUpdateTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~EntityUpdateTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

	void EntityStatus(const unsigned long& entityId, const unsigned short& statusId)override;
	void EntityBodyUpdate(const unsigned long& entityId, const double& speed, const S_ENU_POSITION& position, const S_ENU_ORIENTATION& orientation) override;

private:

	DDSDomainParticipant* transmitterParticipant;
};

