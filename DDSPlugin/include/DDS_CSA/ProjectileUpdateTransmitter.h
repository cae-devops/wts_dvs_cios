//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ProjectileUpdateTransmitterExport.h"
#include "ProjectileUpdate.h"
#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"

class DDSDomainParticipant;

class _PROJECTILE_UPDATE_TRANSMITTER_BUILD_MODE_ ProjectileUpdateTransmitter : public ProjectileUpdate
{
public:

	ProjectileUpdateTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~ProjectileUpdateTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Projectile Update API
	void FallOfShotEvent(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& projectileId, const S_ENU_POSITION& detonationPosition) override;
	void ProjectileTrajectory(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& projectileId, const S_ENU_POSITION& startPosition, const S_ENU_POSITION& endPosition) override;


	
private:

	DDSDomainParticipant* transmitterParticipant;
};

