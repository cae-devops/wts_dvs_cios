//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ExerciseGenerationReceiverExport.h"

class ExerciseGeneration;
class DDSDomainParticipant;

class _EXERCISE_GENERATION_RECEIVER_BUILD_MODE_ ExerciseGenerationReceiver
{
public:

	ExerciseGenerationReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~ExerciseGenerationReceiver(void);

	void Initialise(ExerciseGeneration* exerciseGeneration);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

