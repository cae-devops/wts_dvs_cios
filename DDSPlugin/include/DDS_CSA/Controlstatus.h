/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "ControlStatusExport.h"

#define TASK_CONTROL_STATUS "ControlStatus"


class ControlStatus
{

public:


	enum Switches
	{
		COMMANDERS_BGTI_POWER = 1,						
		COMMANDERS_SIGHT_WIPER = 2,						
		SMOKE_DISCHARGER_ARM = 3,						
		COMMANDERS_SIGHT_CIRCUIT_BREAKER = 4,						
		ARMAMENT_CIRCUIT_BREAKER = 5,						
		POWER_TRAVERSE_CIRCUIT_BREAKER = 6,						
		CAPU_TI_SHEU_CIRCUIT_BREAKER = 7,						
		ARMAMENT_ARM = 8,						 
		POWER_TRAVERSE_ARM = 9,					
		GUNNERS_BGTI_POWER = 10,
		GUNNERS_SIGHT_WIPER = 11,
		ARMAMENT_SELECTOR = 12,					
		GUNNERS_FOOT_LASER = 13,					
		COMMANDERS_POWER_TRAVERSE_ENGAGE = 14,
		GUNNERS_POWER_TRAVERSE_ENGAGE = 15,
		ARMAMENT_FIRE = 16,
		RARDEN_STATE = 17,
		RARDEN_FIRE = 18,
		RARDEN_AMMO_TYPE = 19,
		RARDEN_LOADED = 20,
		RARDEN_FEED_COVER_OPEN = 21,
		RARDEN_LOAD_HANDLE_POSITION = 22,
		RARDEN_READY_TO_FIRE = 23,
		TURRET_LOCK = 24,
		MANUAL_TRAVERSE_SPEED = 25
	};


	enum Buttons
	{
		COMMANDERS_SMOKE_DISCHARGER = 1,
		GUNNERS_SMOKE_DISCHARGER = 2,
		GUNNERS_REMOTE_BGTI_AMMO = 3,
		GUNNERS_REMOTE_BGTI_ENTER = 4,
	};


	enum Controls
	{
		COMMANDERS_POWER_TRAVERSE = 1,
		GUNNERS_POWER_TRAVERSE = 2,
		GUNNERS_MANUAL_TRAVERSE = 3,
		GUNNERS_ELEVATION = 4,
		TURRET_POSITION_INDICATOR = 5
	};


	virtual void ButtonStatus(const unsigned short& buttonId, const bool& buttonPressed, const double& buttonPressTime) = 0;
	virtual void SwitchStatus(const unsigned short& switchId, const unsigned short& switchPosition) = 0;
	virtual void AnalogControlStatus(const unsigned short& controlId, const double& controlValue) = 0;
};