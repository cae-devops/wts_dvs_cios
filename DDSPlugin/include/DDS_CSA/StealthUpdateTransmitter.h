//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "StealthUpdateTransmitterExport.h"
#include "StealthUpdate.h"
#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"

class DDSDomainParticipant;

class _STEALTH_UPDATE_TRANSMITTER_BUILD_MODE_ StealthUpdateTransmitter : public StealthUpdate
{
public:

	StealthUpdateTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~StealthUpdateTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Stealth Update API
	void StealthBodyUpdate(const S_ECEF_POSITION& position, const double& heading, const double& pitch) override;
	void StealthCameraHeadingUpdate(const double& heading) override ;



private:

	DDSDomainParticipant* transmitterParticipant;
};

