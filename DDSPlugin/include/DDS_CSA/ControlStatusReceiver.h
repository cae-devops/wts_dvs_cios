//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ControlStatusReceiverExport.h"

class ControlStatus;
class DDSDomainParticipant;

class _CONTROL_STATUS_RECEIVER_BUILD_MODE_ ControlStatusReceiver
{
public:

	ControlStatusReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~ControlStatusReceiver(void);

	void Initialise(ControlStatus* controlStatus);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

