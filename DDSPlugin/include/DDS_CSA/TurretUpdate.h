/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include <set>
#include "TurretUpdateExport.h"


#define TASK_TURRET_UPDATE "TurretUpdate"


class TurretUpdate
{

public:

	virtual void CurrentGunDirector(const unsigned long& entityId, const unsigned long& gunDirectorId) = 0;	
	virtual void TurretHeading(const unsigned long& entityId, const double& heading) = 0;	
	virtual void AmmoStockLevel(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& stockLevel) = 0;
};