//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "WeaponControlTransmitterExport.h"
#include "WeaponControl.h"


class DDSDomainParticipant;

class _WEAPON_CONTROL_TRANSMITTER_BUILD_MODE_ WeaponControlTransmitter : public WeaponControl
{
public:

	WeaponControlTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~WeaponControlTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Weapon Control API
	void CreateWeapon(const unsigned short& requestId, const unsigned short& weaponTypeId) override;
	void SelectAmmunition(const unsigned short& weaponId, const unsigned short& ammoTypeId) override;
	void FireWeapon(const unsigned short& weaponId, const S_POSITION_AND_ORIENTATION& barrelBase, const double& chassisHeading) override;
	void SetBoreSightErrors(const unsigned short& weaponId, const double& elevationErrorMagnitude, const double& lineErrorMagnitude) override;

private:

	DDSDomainParticipant* transmitterParticipant;
};

