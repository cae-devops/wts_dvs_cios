
#include "DDSReceiverInterfaceExport.h"

class ActiveEntityStatusUpdate;
class ControlStatus;
class EntityControl;
class EntityUpdate;
class ExerciseControl;
class ExerciseControlResponse;
class ExerciseGeneration;
class ExerciseGenerationResponse;
class StealthControl;
class StealthUpdate;
class TurretUpdate;
class TurretControl;
class WeaponControl;
class WeaponControlResponse;
class WeaponUpdate;
class AssetControl;
class ProjectileUpdate;
class MissionFunctionRequest;
class MissionFunctionResponse;
class RemoteExerciseControl;
class EngagementEvent;
class SightUpdate;
class DDSDomainParticipant;

class ActiveEntityStatusUpdateReceiver;
class ControlStatusReceiver;
class EntityControlReceiver;
class EntityUpdateReceiver;
class ExerciseControlReceiver;
class ExerciseControlResponseReceiver;
class ExerciseGenerationReceiver;
class ExerciseGenerationResponseReceiver;
class StealthControlReceiver;
class StealthUpdateReceiver;
class TurretUpdateReceiver;
class TurretControlReceiver;
class WeaponControlReceiver;
class WeaponControlResponseReceiver;
class WeaponUpdateReceiver;
class AssetControlReceiver;
class ProjectileUpdateReceiver;
class MissionFunctionRequestReceiver;
class MissionFunctionResponseReceiver;
class RemoteExerciseControlReceiver;
class EngagementEventReceiver;
class SightUpdateReceiver;

class _DDS_DOMAIN_PART_RECV_BUILD_MODE_ DDSReceiverInterface
{

public:

	DDSReceiverInterface();

	  ~DDSReceiverInterface(void) {};
	  	  	 
	  // Message Group Receiver access
	  ActiveEntityStatusUpdateReceiver*	  GetActiveEntityStatusUpdateReceiver();
	  ControlStatusReceiver*              GetControlStatusReceiver();
	  EntityControlReceiver*              GetEntityControlReceiver();
	  EntityUpdateReceiver*               GetEntityUpdateReceiver();
	  ExerciseControlReceiver*            GetExerciseControlReceiver();
	  ExerciseControlResponseReceiver*    GetExerciseControlResponseReceiver();
	  ExerciseGenerationReceiver*         GetExerciseGenerationReceiver();
	  ExerciseGenerationResponseReceiver* GetExerciseGenerationResponseReceiver();
	  StealthControlReceiver*			  GetStealthControlReceiver();
	  StealthUpdateReceiver*			  GetStealthUpdateReceiver();
	  TurretUpdateReceiver*				  GetTurretUpdateReceiver();
	  TurretControlReceiver*			  GetTurretControlReceiver();
	  WeaponControlReceiver*			  GetWeaponControlReceiver();
	  WeaponControlResponseReceiver*	  GetWeaponControlResponseReceiver();
	  WeaponUpdateReceiver*				  GetWeaponUpdateReceiver();
	  AssetControlReceiver*				  GetAssetControlReceiver();
	  ProjectileUpdateReceiver*			  GetProjectileUpdateReceiver();
	  MissionFunctionRequestReceiver*	  GetMissionFunctionRequestReceiver();
	  MissionFunctionResponseReceiver*	  GetMissionFunctionResponseReceiver();
	  RemoteExerciseControlReceiver*	  GetRemoteExerciseControlReceiver();
	  EngagementEventReceiver*			  GetEngagementEventReceiver();
	  SightUpdateReceiver*				  GetSightUpdateReceiver();

	  int ReceiverParticipantShutdown(void);
	
private:

	unsigned long domainId;   //value must match the transmitters number

	DDSDomainParticipant* receiverParticipant;  // The one and only -- for receivers

// Message Group 
	ActiveEntityStatusUpdate*   activeEntityStatusUpdate;
	ControlStatus*              controlStatus;
	EntityControl*              entityControl;
	EntityUpdate*               entityUpdate;
	ExerciseControl*            exerciseControl;
	ExerciseControlResponse*    exerciseControlResponse;
	ExerciseGeneration*         exerciseGeneration;
	ExerciseGenerationResponse* exerciseGenerationResponse;
	StealthControl*             stealthControl;
	StealthUpdate*              stealthUpdate;
	TurretUpdate*               turretUpdate;
	TurretControl*              turretControl;
	WeaponControl*              weaponControl;
	WeaponControlResponse*      weaponControlResponse;
	WeaponUpdate*               weaponUpdate;
	AssetControl*				assetControl;
	ProjectileUpdate*			projectileUpdate;
	MissionFunctionRequest*		missionFunctionRequest;
	MissionFunctionResponse*	missionFunctionResponse;
	RemoteExerciseControl*		remoteExerciseControl;
	EngagementEvent*			engagementEvent;
	SightUpdate*				sightUpdate;

	// Message Group Receiver
	ActiveEntityStatusUpdateReceiver*	activeEntityStatusUpdateReceiver;
	ControlStatusReceiver*              controlStatusReceiver;
	EntityControlReceiver*              entityControlReceiver;
	EntityUpdateReceiver*               entityUpdateReceiver;
	ExerciseControlReceiver*            exerciseControlReceiver;
	ExerciseControlResponseReceiver*    exerciseControlResponseReceiver;
	ExerciseGenerationReceiver*         exerciseGenerationReceiver;
	ExerciseGenerationResponseReceiver* exerciseGenerationResponseReceiver;
	StealthControlReceiver*             stealthControlReceiver;
	StealthUpdateReceiver*              stealthUpdateReceiver;
	TurretUpdateReceiver*               turretUpdateReceiver;
	TurretControlReceiver*              turretControlReceiver;
	WeaponControlReceiver*              weaponControlReceiver;
	WeaponControlResponseReceiver*      weaponControlResponseReceiver;
	WeaponUpdateReceiver*               weaponUpdateReceiver;
	AssetControlReceiver*				assetControlReceiver;
	ProjectileUpdateReceiver*			projectileUpdateReceiver;
	MissionFunctionRequestReceiver*		missionFunctionRequestReceiver;
	MissionFunctionResponseReceiver*	missionFunctionResponseReceiver;
	RemoteExerciseControlReceiver*		remoteExerciseControlReceiver;
	EngagementEventReceiver*			engagementEventReceiver;
	SightUpdateReceiver*				sightUpdateReceiver;
};


