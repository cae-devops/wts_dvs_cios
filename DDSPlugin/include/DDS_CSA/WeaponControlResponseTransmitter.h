//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "WeaponControlResponseTransmitterExport.h"
#include "WeaponControlResponse.h"
#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"

class DDSDomainParticipant;

class _WEAPON_CONTROL_RESPONSE_TRANSMITTER_BUILD_MODE_ WeaponControlResponseTransmitter : public WeaponControlResponse
{
public:

	WeaponControlResponseTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~WeaponControlResponseTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

	//Weapon Control Response API
	void CreateWeaponResponse(const unsigned long& requestId, const bool& success, const unsigned long& weaponId) override;


private:

	DDSDomainParticipant* transmitterParticipant;
};

