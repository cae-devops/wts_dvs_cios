/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "ProjectileUpdateExport.h"

#define TASK_PROJECTILE_UPDATE "ProjectileUpdate"


class ProjectileUpdate
{
public:

	typedef struct
	{
		double ENU_X;
		double ENU_Y;
		double ENU_Z;
	} S_ENU_POSITION;

	virtual void FallOfShotEvent(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& projectileId, const S_ENU_POSITION& detonationPosition) = 0;
	virtual void ProjectileTrajectory(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& projectileId, const S_ENU_POSITION& startPosition, const S_ENU_POSITION& endPosition) = 0;
};