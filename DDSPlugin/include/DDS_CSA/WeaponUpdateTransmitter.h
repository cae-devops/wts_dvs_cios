//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "WeaponUpdateTransmitterExport.h"
#include "WeaponUpdate.h"


class DDSDomainParticipant;

class _WEAPON_UPDATE_TRANSMITTER_BUILD_MODE_ WeaponUpdateTransmitter : public WeaponUpdate
{
public:

	WeaponUpdateTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~WeaponUpdateTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Turret Update API
	void WeaponFire(const unsigned long& entityId, const unsigned long& weaponTypeId, const unsigned long& ammoTypeId, const unsigned long& projectileId) override;
	void WeaponSelection(const unsigned long& entityId, const unsigned long& weaponTypeId, const unsigned long& ammoTypeId) override;
	void BurstLength(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& numOfProjctileIds, const unsigned long *projectileIds) override;
	
private:

	DDSDomainParticipant* transmitterParticipant;
};

