/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "WeaponControlExport.h"


#define TASK_WEAPON_CONTROL "WeaponControl"

class WeaponControl
{
public:

	typedef struct
	{
		double barrelBaseHeading;
		double barrelBasePitch;
		double barrelBaseRoll;
		double barrelBaseX;
		double barrelBaseY;
		double barrelBaseZ;
	} S_POSITION_AND_ORIENTATION;

	typedef struct
	{


	} S_BORESIGHT_ERRORS;

	virtual void CreateWeapon(const unsigned short& requestId, const unsigned short& weaponTypeId) = 0;
	virtual void SelectAmmunition(const unsigned short& weaponId, const unsigned short& ammoTypeId) = 0;
	virtual void FireWeapon(const unsigned short& weaponId, const S_POSITION_AND_ORIENTATION& barrelBase, const double& chassisHeading) = 0;
	virtual void SetBoreSightErrors(const unsigned short& weaponId, const double& elevationErrorMagnitude, const double& lineErrorMagnitude) = 0;
};