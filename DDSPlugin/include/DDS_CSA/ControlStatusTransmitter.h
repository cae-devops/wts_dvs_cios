//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ControlStatusTransmitterExport.h"
#include "ControlStatus.h"


class DDSDomainParticipant;


class _CONTROL_STATUS_TRANSMITTER_BUILD_MODE_ ControlStatusTransmitter : public ControlStatus
{
public:

	ControlStatusTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~ControlStatusTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Control Status API
	void ButtonStatus(const unsigned short& buttonId, const bool& buttonPressed, const double& buttonPressTime) override;
	void SwitchStatus(const unsigned short& switchId, const unsigned short& switchPosition) override;
	void AnalogControlStatus(const unsigned short& controlId, const double& controlValue) override;

private:

	DDSDomainParticipant* transmitterParticipant;
};

