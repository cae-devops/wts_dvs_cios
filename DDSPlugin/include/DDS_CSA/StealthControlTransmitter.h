//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "StealthControlTransmitterExport.h"
#include "StealthControl.h"

class DDSDomainParticipant;

class _STEALTH_CONTROL_TRANSMITTER_BUILD_MODE_ StealthControlTransmitter : public StealthControl
{
public:

	StealthControlTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~StealthControlTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Stealth Control API
	void AlignStealthCameraToHeading(const double& heading) override;
	void AlignStealthCameraToLocation(const S_ECEF_POSITION& position) override;
	void AttachViewToCommandersSightOnEntity(const unsigned short& entityId) override;
	void AttachViewToGunnersSightOnEntity(const unsigned short& entityId) override;
	void AttachViewToStealthEntity(void) override;
	void DisableTerrainFollowingMode(void) override;
	void EnableTerrainFollowingMode(void) override;
	void RepositionStealthBody(const S_ECEF_POSITION& position, const double& heading) override;
	void UpdateStealthBodyControlDemands(const double& requiredSpeed, const double& headingRate, const double& pitchRate) override ;
	void UpdateStealthCameraControlDemands(const double& headingRate, const double& pitchRate) override ;
	void UpdateStealthCameraElevationOffset(const double& heightOffset) override ;
	void UpdateStealthCameraZoom(const double& zoomLevel) override ;

private:

	DDSDomainParticipant* transmitterParticipant;

};

