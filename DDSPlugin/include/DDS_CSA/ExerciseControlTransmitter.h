//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ExerciseControlTransmitterExport.h"
#include "ExerciseControl.h"

class DDSDomainParticipant;

class _EXERCISE_CONTROL_TRANSMITTER_BUILD_MODE_ ExerciseControlTransmitter : public ExerciseControl
{
public:

	ExerciseControlTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~ExerciseControlTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Exercise Control API
	void LoadExercise(const std::string& exerciseName, const std::string& exerciseFile) override ;
	void StartExercise(void) override ;
	void StopExercise(void) override ;
	void PauseExercise(void) override ;
	void ResumeExercise(void) override ;
	void LoadVoiceCommand(const std::string& voiceCommandName, const std::string& voiceCommandFile) override ;
	void StartExerciseGeneration(void) override;
	void LeaveExerciseGeneration(void) override;
	void StartTrainingSession(void) override ;
	void LeaveTrainingSession(void) override ;
	void CourseComplete(void) override;
	void PhaseComplete(void) override;
	void DisplayMessage(const std::string& message) override;
	void CGFOwnership(const unsigned long& ovEntityId, const unsigned long& numOfEntityIDs, const unsigned long *entityId) override;


private:

	DDSDomainParticipant* transmitterParticipant;
};

