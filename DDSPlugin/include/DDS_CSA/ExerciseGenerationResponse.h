/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "ExerciseGenerationResponseExport.h"

#define TASK_EXERCISE_GENERATION_RESPONSE "ExerciseGenerationResponse"


class ExerciseGenerationResponse
{

public:

	virtual void LoadTerrainDatabaseResponse(const bool& success) = 0;
	virtual void LoadTrackResponse(const bool& success) = 0; 
	virtual void PauseTrackResponse(const bool& success) = 0;
	virtual void RemoveVisualObjectsResponse(const bool& success) = 0;
	virtual void ResumeTrackResponse(const bool& success) = 0;
	virtual void StartTrackResponse(const bool& success) = 0;
	virtual void StopTrackResponse(const bool& success) = 0;
};