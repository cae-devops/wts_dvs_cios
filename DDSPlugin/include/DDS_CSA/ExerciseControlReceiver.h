//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ExerciseControlReceiverExport.h"

class ExerciseControl;
class DDSDomainParticipant;

class _EXERCISE_CONTROL_RECEIVER_BUILD_MODE_ ExerciseControlReceiver
{
public:

	ExerciseControlReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~ExerciseControlReceiver(void);

	void Initialise(ExerciseControl* exerciseControl);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

