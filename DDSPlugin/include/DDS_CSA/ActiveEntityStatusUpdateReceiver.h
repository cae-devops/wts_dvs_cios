//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ActiveEntityStatusUpdateReceiverExport.h"

class ActiveEntityStatusUpdate;
class DDSDomainParticipant;

class _ACTIVE_ENTITY_STATUS_UPDATE_RECEIVER_BUILD_MODE_ ActiveEntityStatusUpdateReceiver
{
public:

	ActiveEntityStatusUpdateReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~ActiveEntityStatusUpdateReceiver(void);

	void Initialise(ActiveEntityStatusUpdate* activeEntityStatusUpdate);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

