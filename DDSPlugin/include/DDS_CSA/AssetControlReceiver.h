//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "AssetControlReceiverExport.h"

class AssetControl;
class DDSDomainParticipant;

class _ASSET_CONTROL_RECEIVER_BUILD_MODE_ AssetControlReceiver
{
public:

	AssetControlReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~AssetControlReceiver(void);

	void Initialise(AssetControl* assetControl);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

