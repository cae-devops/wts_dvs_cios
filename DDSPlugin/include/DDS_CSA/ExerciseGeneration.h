/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include <string>
#include <vector>
#include "ExerciseGenerationExport.h"


#define TASK_EXERCISE_GENERATION "ExerciseGeneration"


class ExerciseGeneration
{

public:

	typedef struct
	{
		unsigned long objectId;
		unsigned long objectTypeId;

	} S_UNIQUE_OBJECT_IDENTIFIER;


	typedef struct
	{
		double ECEF_X;
		double ECEF_Y;
		double ECEF_Z;
	} S_ECEF_POSITION;


	typedef struct
	{
		S_ECEF_POSITION position;
		double heading;

	} S_OBJECT_LOCATION_AND_HEADING;


	//Depending upon how this is handled at the VBS end this could be simplifed so that the Id is unique for all CIOS objects
	virtual void LoadTerrainDatabase(const unsigned long& terrainId, const std::string& terrainName) = 0;

	virtual void LoadTrack(const std::string& trackName, const std::string& trackFile) = 0;
	virtual void PauseTrack(void) = 0;
	virtual void ResumeTrack(void) = 0;
	virtual void StartTrack(void) = 0;
	virtual void StopTrack(void) = 0;

	virtual void DisplayVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier, const long& objectSubTypeId, const S_OBJECT_LOCATION_AND_HEADING& locationAndheading) = 0;
	virtual void SelectVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) = 0;
	virtual void DeselectVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) = 0;
	virtual void UpdateVisualObjectPosition(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier, const S_OBJECT_LOCATION_AND_HEADING& locationAndheading) = 0;
	virtual void RemoveVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) = 0;
	virtual void RemoveVisualObjects(const unsigned long& noOfObjects, const S_UNIQUE_OBJECT_IDENTIFIER *objectIdentifiers) = 0;
};












