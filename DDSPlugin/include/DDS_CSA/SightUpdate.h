/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "SightUpdateExport.h"



#define TASK_SIGHT_UPDATE "SightUpdate"


class  SightUpdate
{

public:

	typedef struct
	{
		double ECEF_X;
		double ECEF_Y;
		double ECEF_Z;
	} S_SIGHT_POSITION;

	typedef struct
	{
		double psi;
		double theta;
		double phi;
	} S_SIGHT_ORIENTATION;
	
	typedef struct
	{
		double horizontal;
		double vertical;
	} S_SIGHT_FOV;
	
	typedef struct
	{
		double azimuth;
		double elevation;
	} S_ANGULAR_OFFSET_FROM_FOV_CENTRE;



	virtual void SelectedSight(	const unsigned long& entityId, 
								const unsigned long& sightId, 
								const unsigned long& crewRoleId, 
								S_SIGHT_FOV fieldOfView, 
								const unsigned long& gratId, 
								S_ANGULAR_OFFSET_FROM_FOV_CENTRE lamOffset, 
								S_ANGULAR_OFFSET_FROM_FOV_CENTRE bamOffset, 
								const double& bamWidth, 
								const bool& isActive) = 0;

	virtual void SightPositionAndOrientation(	const unsigned long& entityId, 
												const unsigned long& sightId, 
												S_SIGHT_POSITION position, 
												S_SIGHT_ORIENTATION orientation) = 0;

};