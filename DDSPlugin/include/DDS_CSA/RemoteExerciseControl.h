/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include <string>
#include <vector>
#include "RemoteExerciseControlExport.h"


#define TASK_REMOTE_EXERCISE_CONTROL "RemoteExerciseControl"



class RemoteExerciseControl
{

public:

	virtual void LoadExerciseRequest(void) = 0;
	virtual void StartExerciseRequest(void) = 0;
	virtual void StopExerciseRequest(void) = 0;
};