//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ActiveEntityStatusUpdateTransmitterExport.h"
#include "ActiveEntityStatusUpdate.h"


class DDSDomainParticipant;

class _ACTIVE_ENTITY_STATUS_UPDATE_TRANSMITTER_BUILD_MODE_ ActiveEntityStatusUpdateTransmitter : public ActiveEntityStatusUpdate
{
public:

	ActiveEntityStatusUpdateTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~ActiveEntityStatusUpdateTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

	//Active Entity Status Update API
	void EntityDamageStatus(const unsigned long& entityId, const unsigned short& damageStatusId) override;
	void EntityEngagementStatus(const unsigned long& entityId, const unsigned short& engagementStatusId) override;


private:

	DDSDomainParticipant* transmitterParticipant;
};

