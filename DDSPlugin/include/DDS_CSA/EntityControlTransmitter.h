//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "EntityControlTransmitterExport.h"
#include "EntityControl.h"



class DDSDomainParticipant;

class _ENTITY_CONTROL_TRANSMITTER_BUILD_MODE_ EntityControlTransmitter : public EntityControl
{
public:

	EntityControlTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~EntityControlTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Entity Control API
	
	void CreateEntity(const unsigned long& entityId, const unsigned long& entityTypeId, const S_OBJECT_LOCATION_AND_HEADING& locationAndheading) override;
	void RemoveEntity(const unsigned long& entityId) override;
	void GoToLocation(const unsigned long& entityId, const S_ECEF_POSITION& location, const double& speed, const double& dwellTime ) override;
	void StopEntityMovement(const unsigned long& entityId) override;
	void MakeInvulnerableEvent(const unsigned long& entityId) override;
	void MakeVulnerableEvent(const unsigned long& entityId) override;
	void KillEntity(const unsigned long& entityId) override;

private:

	DDSDomainParticipant* transmitterParticipant;
};

