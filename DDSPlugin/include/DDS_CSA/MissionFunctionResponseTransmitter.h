//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "MissionFunctionResponseTransmitterExport.h"
#include "MissionFunctionResponse.h"


class DDSDomainParticipant;


class _MISSION_FUNCTION_RESPONSE_TRANSMITTER_BUILD_MODE_ MissionFunctionResponseTransmitter : public MissionFunctionResponse
{
public:

	MissionFunctionResponseTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~MissionFunctionResponseTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

	void LineOfSightResponse(const unsigned long& requestId, const unsigned long& intersectTypeId);
	void MultiLineOfSightResponse(const unsigned long& endElementId, const unsigned long& numOfResponses, const INTERSECT_RESPONSE *responses);

private:

	DDSDomainParticipant* transmitterParticipant;
};

