//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "TurretUpdateReceiverExport.h"

class TurretUpdate;
class DDSDomainParticipant;

class _TURRET_UPDATE_RECEIVER_BUILD_MODE_ TurretUpdateReceiver
{
public:

	TurretUpdateReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~TurretUpdateReceiver(void);

	void Initialise(TurretUpdate* turretUpdate);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

