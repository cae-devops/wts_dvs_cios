/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include "MissionFunctionResponseExport.h"
#include <vector>
#define TASK_MISSION_FUNCTION_RESPONSE "MissionFunctionResponse"


class MissionFunctionResponse
{
public:

	typedef struct
	{
		unsigned long lineOfSightRequestId;
		unsigned long intersectTypeId;
	} INTERSECT_RESPONSE;

	//Not sure we need the ElementIds? how are these used? Need to check with Jem/Nicole
	virtual void LineOfSightResponse(const unsigned long& requestId, const unsigned long& intersectTypeId) = 0;
	virtual void MultiLineOfSightResponse(const unsigned long& endElementId, const unsigned long& numOfResponses, const INTERSECT_RESPONSE *responses) = 0;

};