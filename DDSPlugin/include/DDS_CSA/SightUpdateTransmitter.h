//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "SightUpdateTransmitterExport.h"
#include "SightUpdate.h"


class DDSDomainParticipant;

class _SIGHT_UPDATE_TRANSMITTER_BUILD_MODE_ SightUpdateTransmitter : public SightUpdate
{
public:

	SightUpdateTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~SightUpdateTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

	void SelectedSight(	const unsigned long& entityId, 
						const unsigned long& sightId, 
						const unsigned long& crewRoleId, 
						S_SIGHT_FOV fieldOfView, 
						const unsigned long& gratId, 
						S_ANGULAR_OFFSET_FROM_FOV_CENTRE lamOffset,
						S_ANGULAR_OFFSET_FROM_FOV_CENTRE bamOffset, 
						const double& bamWidth, const bool& isActive) override;

	void SightPositionAndOrientation(	const unsigned long& entityId, 
										const unsigned long& sightId, 
										S_SIGHT_POSITION position, 
										S_SIGHT_ORIENTATION orientation) override;

private:

	DDSDomainParticipant* transmitterParticipant;
};

