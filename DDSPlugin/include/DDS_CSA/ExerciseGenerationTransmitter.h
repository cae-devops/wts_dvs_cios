//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ExerciseGenerationTransmitterExport.h"
#include "ExerciseGeneration.h"

class DDSDomainParticipant;

class _EXERCISE_GENERATION_TRANSMITTER_BUILD_MODE_ ExerciseGenerationTransmitter : public ExerciseGeneration
{
public:

	ExerciseGenerationTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~ExerciseGenerationTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Exercise Generation Control API
	void DisplayVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier, 
							 const long& objectSubTypeId, 
							 const S_OBJECT_LOCATION_AND_HEADING& locationAndheading) override;

	void DeselectVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) override ;
	
	void LoadTerrainDatabase(const unsigned long& terrainId, 
						     const std::string& terrainName) override ;

	void LoadTrack(const std::string& trackName, const std::string& trackFile);

	void PauseTrack(void);
	
	void RemoveVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) override ;
	
	void RemoveVisualObjects(const unsigned long& numOfObjects, const S_UNIQUE_OBJECT_IDENTIFIER *objectIdentifiers) override ;
	
	void ResumeTrack(void);
	
	void SelectVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) override ;
	
	void StartTrack(void);
	
	void StopTrack(void);
	
	void UpdateVisualObjectPosition(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier, 
									const S_OBJECT_LOCATION_AND_HEADING& locationAndheading) override;

private:

	DDSDomainParticipant* transmitterParticipant;
};

