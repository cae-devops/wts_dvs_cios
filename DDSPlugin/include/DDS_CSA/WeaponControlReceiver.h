//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "WeaponControlReceiverExport.h"

class WeaponControl;
class DDSDomainParticipant;

class _WEAPON_CONTROL_RECEIVER_BUILD_MODE_ WeaponControlReceiver
{
public:

	WeaponControlReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~WeaponControlReceiver(void);

	void Initialise(WeaponControl* weaponControl);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

