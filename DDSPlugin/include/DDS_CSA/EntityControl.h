/******************************************************************************
*
* The contents of this file are the copyright (c) of CAE(UK) plc, 2020.
* This software is provided for use with the associated hardware only.
* The software, and copies thereof, may not be supplied to a third party.
*
*******************************************************************************/
#pragma once

#include <string>
#include "EntityControlExport.h"


#define TASK_ENTITY_CONTROL "EntityControl"


// These locations are all defined by an X/Y and height above terrain in the exercise.
// In order to generate an ECEF position we need to obtain a the terrain height at the X/Y location
// It may be better to use Lat/Long and let VBS perform the translation??


class EntityControl
{
public:

	typedef struct
	{
		double ECEF_X;
		double ECEF_Y;
		double ECEF_Z;
	} S_ECEF_POSITION;

	typedef struct
	{
		S_ECEF_POSITION position;
		double heading;

	} S_OBJECT_LOCATION_AND_HEADING;

	virtual void CreateEntity(const unsigned long& entityId, const unsigned long& entityTypeId, const S_OBJECT_LOCATION_AND_HEADING& locationAndheading) = 0;
	virtual void RemoveEntity(const unsigned long& entityId) = 0;
	virtual void GoToLocation(const unsigned long& entityId, const S_ECEF_POSITION& location, const double& speed, const double& dwellTime) = 0;
	virtual void StopEntityMovement(const unsigned long& entityId) = 0;
	virtual void MakeInvulnerableEvent(const unsigned long& entityId) = 0;
	virtual void MakeVulnerableEvent(const unsigned long& entityId) = 0;
	virtual void KillEntity(const unsigned long& entityId) = 0;
};