//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "TurretControlTransmitterExport.h"
#include "TurretControl.h"

class DDSDomainParticipant;

class _TURRET_CONTROL_TRANSMITTER_BUILD_MODE_ TurretControlTransmitter : public TurretControl
{
public:

	TurretControlTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~TurretControlTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);


	//Turret Control API
	void UpdateTurretHeading(const unsigned long& entityId, const double& heading) override;
	void FireLaser(const unsigned long& entityId) override;
	void UpdateWeaponElevation(const unsigned long& entityId, const unsigned long& weaponId, const double& elevation) override;
	void RestockAmmunition(const unsigned long& ammoTypeId) override;
	void SetFixedGunError(/*const unsigned long& entityId,*/ const double& azimuthError, const double& targetRange, const double& requiredRange) override;


private:

	DDSDomainParticipant* transmitterParticipant;
};

