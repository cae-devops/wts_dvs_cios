//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ExerciseGenerationResponseReceiverExport.h"

class ExerciseGenerationResponse;
class DDSDomainParticipant;

class _EXERCISE_GENERATION_RESPONSE_RECEIVER_BUILD_MODE_ ExerciseGenerationResponseReceiver
{
public:

	ExerciseGenerationResponseReceiver(DDSDomainParticipant* receiverParticipant);
	virtual ~ExerciseGenerationResponseReceiver(void);

	void Initialise(ExerciseGenerationResponse* exerciseGenerationResponse);
	void Update(void);
	void Exit(void);

private:

	DDSDomainParticipant* receiverParticipant;
};

