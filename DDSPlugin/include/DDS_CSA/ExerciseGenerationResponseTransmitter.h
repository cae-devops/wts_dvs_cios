//*****************************************************************************
// The contents of this file are the Copyright (c) of CAE UK Plc, 2020
// This software is provided for use with the associated hardware only.
// The software, and copies thereof, may not be supplied to a third party.
//******************************************************************************
#pragma once

#include "ExerciseGenerationResponseTransmitterExport.h"
#include "ExerciseGenerationResponse.h"
#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"

class DDSDomainParticipant;

class _EXERCISE_GENERATION_RESPONSE_TRANSMITTER_BUILD_MODE_ ExerciseGenerationResponseTransmitter: public ExerciseGenerationResponse
{
public:

	ExerciseGenerationResponseTransmitter(DDSDomainParticipant* transmitterParticipant);
	virtual ~ExerciseGenerationResponseTransmitter(void);

	void Initialise(void);
	void Update(void);
	void Exit(void);

	//Exercise Generation Response API
	void LoadTerrainDatabaseResponse(const bool& success) override;
	void LoadTrackResponse(const bool& success) override;
	void PauseTrackResponse(const bool& success) override;
	void RemoveVisualObjectsResponse(const bool& success) override;
	void ResumeTrackResponse(const bool& success) override;
	void StartTrackResponse(const bool& success) override;
	void StopTrackResponse(const bool& success) override;

private:

	DDSDomainParticipant* transmitterParticipant;

};

