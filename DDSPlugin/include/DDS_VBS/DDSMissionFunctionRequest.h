#pragma once

#include "MissionFunctionRequest.h"


class DDSMissionFunctionRequest : public MissionFunctionRequest
{

	void LineOfSightRequest(const unsigned long& requestId, 
		const unsigned long& startElementId, 
		const S_ENU_POSITION& startPosition, 
		const unsigned long& endElementId, 
		const S_ENU_POSITION& endPosition) override;

	void MultiLineOfSightRequest(const unsigned long& lineOfSightId, 
		const unsigned long& startElementId, 
		const S_ENU_POSITION& startPosition, 
		const unsigned long& endElementId,
		const unsigned long& numOfLOSReqIds,
		const REQUEST *requests) override;

	void CancelLineOfSightRequest(const unsigned long& numOfRequests, unsigned long *requestIds) override;
	
};
