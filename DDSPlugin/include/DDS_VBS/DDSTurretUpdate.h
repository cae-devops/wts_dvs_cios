#pragma once

#include "TurretUpdate.h"


class DDSTurretUpdate : public TurretUpdate
{

	void CurrentGunDirector(const unsigned long& entityId, const unsigned long& gunDirectorId) override;
	void TurretHeading(const unsigned long& entityId, const double& heading) override;
	void AmmoStockLevel(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& stockLevel) override;
	
};
