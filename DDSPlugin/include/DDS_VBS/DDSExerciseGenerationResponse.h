#pragma once

#include "ExerciseGenerationResponse.h"


class DDSExerciseGenerationResponse : public ExerciseGenerationResponse
{
	void LoadTerrainDatabaseResponse(const bool& success) override;
	void LoadTrackResponse(const bool& success) override;
	void PauseTrackResponse(const bool& success) override;
	void RemoveVisualObjectsResponse(const bool& success) override;
	void ResumeTrackResponse(const bool& success) override;
	void StartTrackResponse(const bool& success) override;
	void StopTrackResponse(const bool& success) override;
};
 
 
 
 
 
 
 
