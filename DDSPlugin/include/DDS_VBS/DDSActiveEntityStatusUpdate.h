#pragma once

#include "ActiveEntityStatusUpdate.h"


class DDSActiveEntityStatusUpdate : public ActiveEntityStatusUpdate
{
	virtual void EntityDamageStatus(const unsigned long& entityId, const unsigned short& damageStatusId) override;
	virtual void EntityEngagementStatus(const unsigned long& entityId, const unsigned short& engagementStatusId) override;
};
