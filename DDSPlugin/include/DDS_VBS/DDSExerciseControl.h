#pragma once

#include "ExerciseControl.h"


class DDSExerciseControl : public ExerciseControl
{
	void LoadExercise(const std::string& exerciseName, const std::string& exerciseFile) override;
	void StartExercise(void) override;
	void StopExercise(void) override;
	void PauseExercise(void) override;
	void ResumeExercise(void) override;
	void LoadVoiceCommand(const std::string& voiceCommandName, const std::string& voiceCommandFile) override;
	void StartExerciseGeneration(void) override;
	void LeaveExerciseGeneration(void) override;
	void StartTrainingSession(void) override;
	void LeaveTrainingSession(void) override;
	void CourseComplete(void) override;
	void PhaseComplete(void) override;
    void DisplayMessage(const std::string& message) override;
	void CGFOwnership(const unsigned long& ovEntityId, const unsigned long& numOfEntityIds, const unsigned long *entityId) override;
};
