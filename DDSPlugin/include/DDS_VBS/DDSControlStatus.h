#pragma once

#include "ControlStatus.h"


class DDSControlStatus : public ControlStatus
{
	void ButtonStatus(const unsigned short& buttonId, const bool& buttonPressed, const double& buttonPressTime) override;
	void SwitchStatus(const unsigned short& switchId, const unsigned short& switchPosition)override;
	void AnalogControlStatus(const unsigned short& controlId, const double& controlValue) override;
	
};
