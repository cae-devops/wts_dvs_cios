#pragma once

#include "RemoteExerciseControl.h"


class DDSRemoteExerciseControl : public RemoteExerciseControl
{
	void LoadExerciseRequest(void) override;
	void StartExerciseRequest(void) override;
	void StopExerciseRequest(void) override;
};
