#pragma once

#include "MissionFunctionResponse.h"


class DDSMissionFunctionResponse : public MissionFunctionResponse
{
	void LineOfSightResponse(const unsigned long& requestId, const unsigned long& intersectTypeId) override;
	void MultiLineOfSightResponse(const unsigned long& endElementId, const unsigned long& numOfResponses, const INTERSECT_RESPONSE *responses) override;
};
