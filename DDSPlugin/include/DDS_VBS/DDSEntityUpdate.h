#pragma once

#include "EntityUpdate.h"


class DDSEntityUpdate : public EntityUpdate
{
	void EntityStatus(const unsigned long& entityId, const unsigned short& statusId) override;
	void EntityBodyUpdate(const unsigned long& entityId, const double& speed, const S_ENU_POSITION& position, const S_ENU_ORIENTATION& orientation) override;
};
