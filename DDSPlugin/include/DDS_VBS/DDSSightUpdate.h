#pragma once

#include "SightUpdate.h"


class DDSSightUpdate : public SightUpdate
{


	void SelectedSight(const unsigned long& entityId,
		const unsigned long& sightId,
		const unsigned long& crewRoleId,
		S_SIGHT_FOV fieldOfView,
		const unsigned long& gratId,
		S_ANGULAR_OFFSET_FROM_FOV_CENTRE lamOffset,
		S_ANGULAR_OFFSET_FROM_FOV_CENTRE bamOffset,
		const double& bamWidth,
		const bool& isActive)  override;

	void SightPositionAndOrientation(const unsigned long& entityId,
		const unsigned long& sightId,
		S_SIGHT_POSITION position,
		S_SIGHT_ORIENTATION orientation) override;

	
	
};
