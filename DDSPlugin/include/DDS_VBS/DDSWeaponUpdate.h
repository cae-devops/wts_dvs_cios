#pragma once

#include "WeaponUpdate.h"


class DDSWeaponUpdate : public WeaponUpdate
{
	void WeaponSelection(const unsigned long& entityId, const unsigned long& weaponTypeId, const unsigned long& ammoTypeId) override;
	void WeaponFire(const unsigned long& entityId, const unsigned long& weaponTypeId, const unsigned long& ammoTypeId, const unsigned long& projectileId) override;
	void BurstLength(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& numOfProjctileIds, const unsigned long *projectileIds) override;
};
