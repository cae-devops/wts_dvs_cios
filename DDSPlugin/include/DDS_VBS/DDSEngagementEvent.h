#pragma once

#include "EngagementEvent.h"


class DDSEngagementEvent : public EngagementEvent
{
	void CentralHitAchievedOnEntity(const unsigned long& entityId) override;
	void AimingMarkAlignedWithEntity(const unsigned long& entityId) override;
	void BurstFiredAtEntity(const unsigned long& entityId) override;
	void BurstsFiredAtEntity(const unsigned long& entityId, const unsigned short& burstCount) override;
	void LaserFiredAtEntity(const unsigned long& entityId) override;
	void LasesFiredAtEntity(const unsigned long& entityId, const unsigned short& laseCount) override;
	
};
