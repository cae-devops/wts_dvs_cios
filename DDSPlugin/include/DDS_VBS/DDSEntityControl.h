#pragma once

#include "EntityControl.h"


class DDSEntityControl : public EntityControl
{
	void CreateEntity(const unsigned long& entityId, const unsigned long& entityTypeId, const S_OBJECT_LOCATION_AND_HEADING& locationAndheading) override;
	void RemoveEntity(const unsigned long& entityId) override;
	void GoToLocation(const unsigned long& entityId, const S_ECEF_POSITION& location, const double& speed, const double& dwellTime) override;
	void StopEntityMovement(const unsigned long& entityId) override;
	void MakeInvulnerableEvent(const unsigned long& entityId) override;
	void MakeVulnerableEvent(const unsigned long& entityId) override;
	void KillEntity(const unsigned long& entityId) override;
	
};
