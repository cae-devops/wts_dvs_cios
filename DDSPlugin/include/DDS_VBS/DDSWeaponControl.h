#pragma once

#include "WeaponControl.h"


class DDSWeaponControl : public WeaponControl
{
	void CreateWeapon(const unsigned short& requestId, const unsigned short& weaponTypeId) override;
	void SelectAmmunition(const unsigned short& weaponId, const unsigned short& ammoTypeId) override;
	void FireWeapon(const unsigned short& weaponId, const S_POSITION_AND_ORIENTATION& barrelBase, const double& chassisHeading) override;
	void SetBoreSightErrors(const unsigned short& weaponId, const double& elevationErrorMagnitude, const double& lineErrorMagnitude) override;
};
