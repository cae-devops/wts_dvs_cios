#pragma once

#include "StealthControl.h"


class DDSStealthControl : public StealthControl
{
	void AlignStealthCameraToHeading(const double& heading) override;
	void AlignStealthCameraToLocation(const S_ECEF_POSITION& position) override;
	void AttachViewToCommandersSightOnEntity(const unsigned short& entityId) override;
	void AttachViewToGunnersSightOnEntity(const unsigned short& entityId) override;
	void AttachViewToStealthEntity(void) override;
	void DisableTerrainFollowingMode(void) override;
	void EnableTerrainFollowingMode(void) override;
	void RepositionStealthBody(const S_ECEF_POSITION& position, const double& heading) override;
	void UpdateStealthBodyControlDemands(const double& requiredSpeed, const double& headingRate, const double& pitchRate) override;
	void UpdateStealthCameraControlDemands(const double& headingRate, const double& pitchRate) override;
	void UpdateStealthCameraElevationOffset(const double& heightOffset) override;  
	void UpdateStealthCameraZoom(const double& zoomLevel) override; 

private:

	double Interpolate(const double& requiredRangeMax, const double& requiredRangeMin, const double& inputValue,
						const double& inputRangeMax, const double& inputRangeMin) const;

	double SmoothInput(const double& inputValue) const;

};
