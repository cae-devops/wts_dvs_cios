#pragma once

#include "ProjectileUpdate.h"


class DDSProjectileUpdate : public ProjectileUpdate
{
	void FallOfShotEvent(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& projectileId, const S_ENU_POSITION& detonationPosition) override;
	void ProjectileTrajectory(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& projectileId, const S_ENU_POSITION& startPosition, const S_ENU_POSITION& endPosition) override;
};
