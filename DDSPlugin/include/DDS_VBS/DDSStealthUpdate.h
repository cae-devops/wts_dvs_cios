#pragma once

#include "StealthUpdate.h"


class DDSStealthUpdate : public StealthUpdate
{
	void StealthBodyUpdate(const S_ECEF_POSITION& position, const double& heading, const double& pitch) override;
	void StealthCameraHeadingUpdate(const double& heading) override;
};
