#pragma once

#include "TurretControl.h"


class DDSTurretControl : public TurretControl
{
	void UpdateTurretHeading(const unsigned long& entityId, const double& heading)override;
	void FireLaser(const unsigned long& entityId)override;
	void UpdateWeaponElevation(const unsigned long& entityId, const unsigned long& weaponId, const double& elevation) override;
	void RestockAmmunition(const unsigned long& ammoTypeId) override;
	void SetFixedGunError(/*const unsigned long& entityId,*/ const double& azimuthError, const double& targetRange, const double& requiredRange) override;
};
