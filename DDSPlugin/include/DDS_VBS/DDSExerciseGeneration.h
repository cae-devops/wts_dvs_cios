#pragma once

#include "ExerciseGeneration.h"

class DDSExerciseGeneration : public ExerciseGeneration
{

	void DisplayVisualObject(const ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier, 
		                     const long& objectSubTypeId, 
		                     const ExerciseGeneration::S_OBJECT_LOCATION_AND_HEADING& locationAndheading) override;
	void DeselectVisualObject(const ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) override;
	void LoadTerrainDatabase(const unsigned long& terrainId, const std::string& terrainName) override;
	void LoadTrack(const std::string& TrackName, const std::string& trackFile) override;
	void PauseTrack(void) override;
	void RemoveVisualObject(const ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) override ;
	void RemoveVisualObjects(const unsigned long& numOfObjects, const S_UNIQUE_OBJECT_IDENTIFIER *objectIdentifiers) override;
	void ResumeTrack(void) override;
	void SelectVisualObject(const ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier) override;
	void StartTrack(void) override;
	void StopTrack(void) override;
	void UpdateVisualObjectPosition(const ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier, 
		                            const ExerciseGeneration::S_OBJECT_LOCATION_AND_HEADING& locationAndheading) override;
};
