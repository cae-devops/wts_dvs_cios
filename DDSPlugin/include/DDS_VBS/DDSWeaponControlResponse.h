#pragma once

#include "WeaponControlResponse.h"


class DDSWeaponControlResponse : public WeaponControlResponse
{
	void CreateWeaponResponse(const unsigned long& requestId, const bool& success, const unsigned long& weaponId) override;
};
