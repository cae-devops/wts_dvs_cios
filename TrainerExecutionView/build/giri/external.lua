-- ///////////////////////////////////
-- ///////////////////////////////////
-- ///////////////////////////////////
-- WARNING
-- This file has been auto-generated.
-- Do NOT make modifications directly to it as they will be overwritten!
-- ///////////////////////////////////
-- ///////////////////////////////////
-- ///////////////////////////////////

-- Project names:
local base_name = 'TrainerExecutionView'
local component_project_name = base_name
local library_project_name = base_name .. 'Library'
local unittest_project_name = base_name .. 'UnitTest'

-- Projects:
if _ACTION == 'vs2010' then
externalproject( component_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '23cd391a-b5ad-4979-806c-d6b477833fa6' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '0b1d0443-e44a-4abb-b5a3-653f7bad9031' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '03b6042e-0419-4c6c-b557-0e8c66c34cbd' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2012' then
externalproject( component_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '23cd391a-b5ad-4979-806c-d6b477833fa6' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '0b1d0443-e44a-4abb-b5a3-653f7bad9031' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '03b6042e-0419-4c6c-b557-0e8c66c34cbd' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2013' then
externalproject( component_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '23cd391a-b5ad-4979-806c-d6b477833fa6' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '0b1d0443-e44a-4abb-b5a3-653f7bad9031' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '03b6042e-0419-4c6c-b557-0e8c66c34cbd' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2015' then
externalproject( component_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '23cd391a-b5ad-4979-806c-d6b477833fa6' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '0b1d0443-e44a-4abb-b5a3-653f7bad9031' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '03b6042e-0419-4c6c-b557-0e8c66c34cbd' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2017' then
externalproject( component_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '23cd391a-b5ad-4979-806c-d6b477833fa6' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '0b1d0443-e44a-4abb-b5a3-653f7bad9031' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '03b6042e-0419-4c6c-b557-0e8c66c34cbd' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2019' then
externalproject( component_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '23cd391a-b5ad-4979-806c-d6b477833fa6' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '0b1d0443-e44a-4abb-b5a3-653f7bad9031' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '03b6042e-0419-4c6c-b557-0e8c66c34cbd' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

