#include "DDSControlStatus.h"

void DDSControlStatus::AnalogControlStatus(const unsigned short& controlId, const double& controlValue)
{
	//std::cout << " *** DDSControlStatus::AnalogControlStatus ***" << std::endl;
	//std::cout << "     Control ID " << controlId << std::endl;
	//std::cout << "     Control Value " << controlValue << std::endl;
	
}

void DDSControlStatus::ButtonStatus(const unsigned short& buttonId, const bool& buttonPressed, const double& buttonPressTime)
{
	//std::cout << " *** DDSControlStatus::ButtonStatus ***" << std::endl;
	//std::cout << "     Button ID " << buttonId << std::endl;
	//std::cout << "     Button Pressed " << buttonPressed << std::endl;
	//std::cout << "     Press Time " << buttonPressTime << std::endl;
}

void DDSControlStatus::SwitchStatus(const unsigned short& switchId, const unsigned short& switchPosition)
{
	//std::cout << " *** DDSControlStatus::SwitchStatus ***  " << std::endl;
	//std::cout << "     Switch ID " << switchId << std::endl;
	//std::cout << "     Switch Position " << switchPosition << std::endl;
	
}
