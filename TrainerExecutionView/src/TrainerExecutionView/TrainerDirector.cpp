/*
*
* Copyright(c) 2018 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/

#include "TrainerMessageData.h"
#include "Gears.h"
#include <string>

#include "TrainerDirector.h"
#include "TrainerView.h"

#define LISTENER_PRIORITY 5000

APIResult TrainerDirector::Initialize(APIManager_v6* api_manager, NativeModuleHandle proxy_handle)
{
	bool allFound = true;

	// Start service listener
	if (APIRESULT_FAIL(Gears::API::OSServiceAPIv6()->GetMainWindow(&_main_window, kTimeoutInfinite)) 
		|| APIRESULT_FAIL(_main_window->AddListener(_main_window, Gears::MyAPI::OSWindowListenerAPIv3(), LISTENER_PRIORITY)))
	{
		allFound = false;
	}

	// Allocate APIs
	auto log_api = Gears::API::LogAPIv1();

	log_api->LogInfo(log_api, "Exercise View loading.");

	if (allFound)
	{
		log_api->LogInfo(log_api, "OK.");
	}
	else
	{
		log_api->LogInfo(log_api, "Error occurred.");
	}

	_api_manager = api_manager;
	_proxy_handle = proxy_handle;

	_trainer_view = new TrainerView(_main_window);

	return allFound ? kAPIResult_GeneralSuccess : kAPIResult_GeneralError;
}

void TrainerDirector::OnBeforeSimulation(float delta)
{
	if (_trainer_view != nullptr)
	{
		_trainer_view->OnBeforeSimulation(delta);
	}
}

void TrainerDirector::OnAfterSimulation(float delta)
{
	if (_trainer_view != nullptr)
	{
		_trainer_view->OnAfterSimulation(delta);
	}
}

void TrainerDirector::OnMissionStart()
{
	if (_trainer_view != nullptr)
	{
		_trainer_view->OnMissionStart();
	}
}

void TrainerDirector::OnMissionEnd()
{
	if (_trainer_view != nullptr)
	{
		_trainer_view->OnMissionEnd();
	}
}

void TrainerDirector::OnMissionLoad(const char* mission_name)
{
	if (_trainer_view != nullptr)
	{
		_trainer_view->OnMissionLoad(mission_name);
	}
}

void TrainerDirector::OnFire(ObjectHandle_v3 shooter, ObjectHandle_v3 shot)
{
  if (_trainer_view) _trainer_view->OnFire(shooter, shot);
}

void TrainerDirector::OnAmmoHit(const HitEvent_v2* hit_info)
{
  if (_trainer_view) _trainer_view->OnAmmoHit(hit_info);
}

void TrainerDirector::OnDamageChange(const ObjectHandle_v3 object, const ObjectHandle_v3 source, const float32_t damage)
{
	if (_trainer_view) _trainer_view->OnDamageChange(object,source,damage);
}

void TrainerDirector::OnDestruction(const ObjectHandle_v3 object, const ObjectHandle_v3 source)
{
	if (_trainer_view) _trainer_view->OnDestruction(object, source);
}

void TrainerDirector::OnObjectHit(const HitEvent_v2* hit_info)
{
	if (_trainer_view) _trainer_view->OnObjectHit(hit_info);
}

void TrainerDirector::OnRenderMainWindow()
{
	Vbs3ApplicationState_v1 state;

	SDKCheck(Gears::API::VBS3ApplicationAPIv1()->GetCurrentState(&state));

	if (state == kVbs3ApplicationState_unknown || state == kVbs3ApplicationState_shutdown)
	{
		return;
	}

	SetStyles();

	if (_trainer_view != nullptr)
	{
		_trainer_view->OnRenderMainWindow();
	}

	// Set size for buttons we will use
	ImVec2_v1 size;
	size._x = 180;
	size._y = 25;

	// Show mouse cursor if some gui is drawn
	SDKCheck(Gears::API::VBS3GUIAPIv2()->SetCursorType(kVBS3CursorType_arrow));
}

// Sets DebugUI style
void TrainerDirector::SetStyles()
{
	ImGuiStyle_v1* pStyle;

	SDKCheck(Gears::API::IMGuiAPIv1()->GetStyle(&pStyle));

	pStyle->_WindowRounding = 0;
	pStyle->_ChildWindowRounding = 0;
	pStyle->_ScrollbarRounding = 0;
	pStyle->_Alpha = 0.9f;
	pStyle->_AntiAliasedLines = TRUE;
	pStyle->_AntiAliasedShapes = TRUE;
	pStyle->_ScrollbarSize = 14;

	const ImVec4_v1 main_color = { 0.937F, // _x
								   0.521F, // _y
								   0.129F, // _z
								   1.0F }; // _w

	pStyle->_Colors[kImGuiCol_CheckMark] = main_color;
	pStyle->_Colors[kImGuiCol_FrameBgHovered] = main_color;
	pStyle->_Colors[kImGuiCol_ButtonHovered] = main_color;

	const ImVec4_v1 scrollbar_color = { 0.15F,  // _x
										0.15F,  // _y
										0.15F,  // _z
										1.0F }; // _w

	pStyle->_Colors[kImGuiCol_ScrollbarGrabActive] = scrollbar_color;
	pStyle->_Colors[kImGuiCol_ScrollbarGrabHovered] = scrollbar_color;

	const ImVec4_v1 button_color = { 0.5F,   // _x
									 0.5F,   // _z
									 0.5F,   // _y
									 1.0F }; // _w

	pStyle->_Colors[kImGuiCol_Button] = button_color;
}

bool32_t TrainerDirector::OnSize(const int32_t client_width, const int32_t client_height)
{
	return _trainer_view != nullptr && _trainer_view->OnSize(client_width, client_height) ? TRUE : FALSE;
}

bool32_t TrainerDirector::OnMouseButtonPressed(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
	return _trainer_view != nullptr && _trainer_view->OnMouseButtonPressed(button_id, client_x, client_y) ? TRUE : FALSE;
}

bool32_t TrainerDirector::OnMouseButtonReleased(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
	return false;  // _trainer_view != nullptr && _trainer_view->OnMouseButtonReleased(button_id, client_x, client_y) ? TRUE : FALSE;
}

bool32_t TrainerDirector::OnMouseWheel(WheelID_v1 wheel_id, int32_t wheel_delta)
{
	return false;  //  _trainer_view != nullptr && _trainer_view->OnMouseWheel(wheel_id, wheel_delta) ? TRUE : FALSE;
}

bool32_t TrainerDirector::OnKeyPressed(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags)
{
	return _trainer_view != nullptr && _trainer_view->OnKeyPressed(key_id, system_key, scan_code, scan_flags) ? TRUE : FALSE;
}

bool32_t TrainerDirector::OnKeyReleased(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags)
{
	return false;  //  _trainer_view != nullptr && _trainer_view->OnKeyReleased(key_id, system_key, scan_code, scan_flags) ? TRUE : FALSE;
}

bool32_t TrainerDirector::OnMouseMove(int32_t client_x, int32_t client_y)
{
	if (_trainer_view == nullptr)
	{
		return FALSE;
	}
	else
	{
		const ScreenPosition_v3 client_xy = { static_cast<float32_t>(client_x),   // x
											  static_cast<float32_t>(client_y) }; // y

		return false;  //  _trainer_view->OnMouseMove(client_xy) ? TRUE : FALSE;
	}
}

bool32_t TrainerDirector::OnMouseDoubleClick(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
	return false;  //  _trainer_view != nullptr && _trainer_view->OnMouseDoubleClick(button_id, client_x, client_y) ? TRUE : FALSE;
}

void TrainerDirector::OnObjectCollision(ObjectHandle_v3 object, ObjectHandle_v3 trigger_object, CollisionEventType_v1 type, CollisionParameters_v1 params, CollisionParameters_v1 trigger_params)
{
	return;  // if (_trainer_view) _trainer_view->OnObjectCollision(object, trigger_object, type, params, trigger_params);
}

void TrainerDirector::OnObjectCreation(ObjectHandle_v3 object)
{
	return;  // 	if (_trainer_view) _trainer_view->OnObjectCreation(object);
}

void TrainerDirector::OnObjectDeletion(ObjectHandle_v3 object)
{
	return;  // 	if (_trainer_view) _trainer_view->OnObjectDeletion(object);
}
