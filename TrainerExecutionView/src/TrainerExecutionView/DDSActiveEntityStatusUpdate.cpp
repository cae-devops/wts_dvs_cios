#include "DDSActiveEntityStatusUpdate.h"
#include "TrainerMessageData.h"

void DDSActiveEntityStatusUpdate::EntityDamageStatus(const unsigned long& entityId, const unsigned short& damageStatusId)
{
	//std::cout << std::endl << " *** DDSActiveEntityStatusUpdate::EntityDamageStatus ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	//std::cout << "     Damage Status ID " << damageStatusId << std::endl;
	exconEntityDamageStatusID = damageStatusId;
	entityDamageInput.insert(std::map<unsigned long, unsigned short>::value_type(entityId, damageStatusId));
}


void DDSActiveEntityStatusUpdate::EntityEngagementStatus(const unsigned long& entityId, const unsigned short& engagementStatusId)
{
	//std::cout << std::endl << " *** DDSActiveEntityStatusUpdate::EntityEngagementStatus ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	//std::cout << "     Engagement Status ID " << engagementStatusId << std::endl;
}

