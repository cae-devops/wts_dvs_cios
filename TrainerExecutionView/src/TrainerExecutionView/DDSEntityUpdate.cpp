#include "DDSEntityUpdate.h"

void DDSEntityUpdate::EntityStatus(const unsigned long& entityId, const unsigned short& statusId)
{
	//std::cout << " *** DDSEntityControl::EntityStatus ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	//std::cout << "     Status ID " << statusId << std::endl;
}

void DDSEntityUpdate::EntityBodyUpdate(const unsigned long& entityId, const double& speed, const S_ENU_POSITION& position, const S_ENU_ORIENTATION& orientation)
{
	//std::cout << " *** DDSEntityControl::EntityBodyUpdate ***  " << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl; 
	//std::cout << "     Speed" << speed << std::endl;
	//std::cout << "     Position X " << position.ENU_X << std::endl;
	//std::cout << "     Position Y " << position.ENU_Y << std::endl;
	//std::cout << "     Position Z " << position.ENU_Z << std::endl;
}
