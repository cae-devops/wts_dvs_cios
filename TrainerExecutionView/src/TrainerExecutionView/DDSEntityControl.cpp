#include "DDSEntityControl.h"
#include "TrainerMessageData.h"
#include "ScopeLock.h"

void DDSEntityControl::CreateEntity(const unsigned long& entityId, const unsigned long& entityTypeId, const S_OBJECT_LOCATION_AND_HEADING& locationAndheading)
{
	ScopeLock lock(MessageLock);

	//std::cout << " *** DDSEntityControl::CreateEntity ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	//std::cout << "     Entity Type ID " << entityTypeId << std::endl;
	//std::cout << "     Heading " << locationAndheading.heading << std::endl;
	//std::cout << "     Position X " << locationAndheading.position.ECEF_X << std::endl;
	//std::cout << "     Position Y " << locationAndheading.position.ECEF_Y << std::endl;
	//std::cout << "     Position Z " << locationAndheading.position.ECEF_Z << std::endl;

	entityAddObjectIDs.push_back(entityId);
	entityAddEntityTypeId.push_back(entityTypeId);
	entityAddLocationAndHeading.push_back(locationAndheading);
	addEntities = true;
}

void DDSEntityControl::RemoveEntity(const unsigned long& entityId)
{
	ScopeLock lock(MessageLock);

	//std::cout << " *** DDSEntityControl::RemoveEntity ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;

	entityRemoveObjectIdentifier = entityId;
}

void DDSEntityControl::GoToLocation(const unsigned long& entityId, const S_ECEF_POSITION& location, const double& speed, const double& dwellTime)
{
	ScopeLock lock(MessageLock);

	//std::cout << " *** DDSEntityControl::GoToLocation ***  " << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl; 
	//std::cout << "     Position X " << location.ECEF_X << std::endl;
	//std::cout << "     Position Y " << location.ECEF_Y << std::endl;
	//std::cout << "     Position Z " << location.ECEF_Z << std::endl;
	//std::cout << "     Speed "<< speed <<std::endl;
	//std::cout << "     Dwell Time" << dwellTime << std::endl;

	entityGoToObjectIdentifier.push_back(entityId);
	entityGoToLocation.push_back(location);
	entityGoToSpeed.push_back(speed);
	entityGoToDwellTime.push_back(dwellTime);
}

void DDSEntityControl::StopEntityMovement(const unsigned long& entityId) 
{
	ScopeLock lock(MessageLock);

	//std::cout << std::endl << " *** DDSEntityControl::StopEntityMovement ***  " << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	exconStopEntityMovementID = entityId;
}

void DDSEntityControl::MakeInvulnerableEvent(const unsigned long& entityId) 
{
	ScopeLock lock(MessageLock);

	//std::cout << std::endl << " *** DDSEntityControl::MakeInvulnerableEvent ***  " << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	exconMakeInvulnerableEventID = entityId;
}

void DDSEntityControl::MakeVulnerableEvent(const unsigned long& entityId)
{
	ScopeLock lock(MessageLock);

	//std::cout << std::endl << " *** DDSEntityControl::MakeVulnerableEvent ***  " << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	exconMakeVulnerableEventID = entityId;
}

void DDSEntityControl::KillEntity(const unsigned long& entityId) 
{
	ScopeLock lock(MessageLock);

	//std::cout << std::endl << " *** DDSEntityControl::KillEntity ***  " << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	exconKillEntityID = entityId;
}
