#include "DDSTurretUpdate.h"

void DDSTurretUpdate::AmmoStockLevel(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& stockLevel)
{
	// << " *** DDSTurretUpdate::AmmoStockLevel ***" << std::endl;
	// << "     Entity ID: " << entityId << std::endl;
	// << "     Ammo Type ID: " << ammoTypeId << std::endl;
	// << "    Stock Level: " << stockLevel << std::endl;
}

void DDSTurretUpdate::TurretHeading(const unsigned long& entityId, const double& heading)
{
	// << " *** DDSTurretUpdate::TurretHeading ***" << std::endl;
	// << "     Entity ID: " << entityId << std::endl;
	// << "     Heading: " << heading << std::endl;
}

void DDSTurretUpdate::CurrentGunDirector(const unsigned long& entityId, const unsigned long& gunDirectorId)
{
	// << " *** DDSTurretUpdate::CurrentGunDirector ***  " << std::endl;
	// << "     Entity ID: " << entityId << std::endl;
	// << "     Gun Director ID: " << gunDirectorId << std::endl;
}