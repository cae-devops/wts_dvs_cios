#include "DDSMissionFunctionRequest.h"
#include "TrainerMessageData.h"

void DDSMissionFunctionRequest::LineOfSightRequest(const unsigned long& requestId,
	const unsigned long& startElementId,
	const S_ENU_POSITION& startPosition,
	const unsigned long& endElementId,
	const S_ENU_POSITION& endPosition)
{
	// *** DDSMissionFunctionRequest::LineOfSightRequest ***" 
	//     Start Element ID " << startElementId 
	//     Start Position X " << startPosition.ENU_X 
	//                  " Y " << startPosition.ENU_Y 
	//	                " Z " << startPosition.ENU_Z 
	//     End Element ID "   << endElementId <<  
	//     End Position " X " << endPosition.ENU_X
	//	                " Y " << endPosition.ENU_Y
	//	                " Z " << endPosition.ENU_Z  
	//
	missfuncLOSRequestID.push_back(requestId);
	missfuncLOSRequestStartID.push_back(startElementId);
	missfuncLOSRequestStartpos.push_back(startPosition);
	missfuncLOSRequestEndID.push_back(endElementId);
	missfuncLOSRequestEndpos.push_back(endPosition);
}

void DDSMissionFunctionRequest::MultiLineOfSightRequest(
	const unsigned long& lineOfSightId,
	const unsigned long& startElementId,
	const S_ENU_POSITION& startPosition,
	const unsigned long& endElementId,
	const unsigned long& numOfLOSReqIds,
	const REQUEST *requests)
{
	//" *** DDSMissionFunctionRequest::MultiLineOfSightRequest ***" 
	//     Line Of Sight ID " << lineOfSightId << 
	//     Start Element ID " << startElementId << 
	//     Start Position X " << startPosition.ENU_X
	//                  " Y " << startPosition.ENU_Y
	//	                " Z " << startPosition.ENU_Z 
	//     End Element ID     << endElementId << 
	//     Request            << request id >>
	//                        << ENU Pos    >>

	missfuncnumOfLOSReqIds.push_back(numOfLOSReqIds);
	missfuncLOSRequestID.push_back(lineOfSightId);
	missfuncLOSRequestStartID.push_back(startElementId);
	missfuncLOSRequestStartpos.push_back(startPosition);
	missfuncLOSRequestEndID.push_back(endElementId);

	for (unsigned long i = 0; i < numOfLOSReqIds; i++)
	{
		// Request ID and ENU End Position
		missfuncLOSRequestEndposIDs.push_back(requests[i].requestId);
		missfuncLOSRequestEndpos.push_back(requests[i].endPosition);
	}
}

void DDSMissionFunctionRequest::CancelLineOfSightRequest(const unsigned long& numOfRequests, unsigned long *requestIds)
{
	for (unsigned long i = 0; i < numOfRequests; i++)
	{
		missfuncCancelLOSRequestID.push_back(requestIds[i]) ;
	}

}
