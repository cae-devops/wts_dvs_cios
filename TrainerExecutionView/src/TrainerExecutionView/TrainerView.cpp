#include "TrainerView.h"

#include <IMGuiAPI.h>
#include <ModelAspectAPI.h>
#include <OSServiceListenerAPI.h>
#include <SimulationAspectAPI.h>
#include <TransformationAspectAPI.h>
#include <VBS3ApplicationAPI.h>
#include <VBS3GUIAPI.h>
#include <WorldAPI.h>
#include "Gears.h"

#include "TrainerDirector.h"
#include "TrainerMessageData.h"

#include "DDSTransmitterInterface.h"
#include "EntityUpdateTransmitter.h"
#include "TurretUpdateTransmitter.h"
#include "WeaponUpdateTransmitter.h"
#include "ProjectileUpdateTransmitter.h"
#include "MissionFunctionResponseTransmitter.h"
#include "EngagementEventTransmitter.h"
#include "SightUpdateTransmitter.h"
#include "TurretControlTransmitter.h"

#include "ExerciseControlReceiver.h"
#include "EntityControlReceiver.h"
#include "TurretControlReceiver.h"
#include "WeaponControlReceiver.h"
#include "AssetControlReceiver.h"
#include "MissionFunctionRequestReceiver.h"
#include "RemoteExerciseControlReceiver.h"
#include "EngagementEventReceiver.h"
#include "ActiveEntityStatusUpdateReceiver.h"
#include "SightUpdateReceiver.h"

#include "DDSTurretControl.h"
#include "DDSEntityControl.h"
#include "DDSExerciseControl.h"
#include "DDSWeaponControl.h"
#include "DDSReceiverInterface.h"
#include "DDSAssetControl.h"
#include "DDSMissionFunctionRequest.h"
#include "DDSMissionFunctionResponse.h"
#include "DDSRemoteExerciseControl.h"
#include "DDSEngagementEvent.h"
#include "DDSSightUpdate.h"
#include "DDSActiveEntityStatusUpdate.h"

#include <cmath>
#include <string>
#include <utility>
#include <stdlib.h>

#include "ScopeLock.h"
SharedPtr<LockObject> MessageLock;

//
// ****** Entity Control *******
//
bool addEntities = false; 
std::vector<long> entityAddEntityTypeId = { 0 };
std::vector<long> entityAddObjectIDs;
std::vector <EntityControl::S_OBJECT_LOCATION_AND_HEADING> entityAddLocationAndHeading = { 0,{0.0,0.0,0.0} };
long entityRemoveObjectIdentifier = 0;
std::vector<long> entityGoToObjectIdentifier;
std::vector<EntityControl::S_ECEF_POSITION> entityGoToLocation;
std::vector<double> entityGoToSpeed;
std::vector<double> entityGoToDwellTime;
unsigned long exconStopEntityMovementID = 0;
unsigned long exconMakeInvulnerableEventID = 0;
unsigned long exconMakeVulnerableEventID = 0;
unsigned long exconKillEntityID = 0;

EntityUpdateTransmitter*   entityUpdateTransmitterCopy;

unsigned short exconEntityDamageStatusID = 0;
std::map<unsigned long, unsigned short> entityDamageInput;

//
// ****** Exercise Control *******
//
bool exgenStart = false; //????????????????
bool exgenLeave = false;  //???????????????????
std::string exconExerciseName = "LDFSH1T";
std::string exconExerciseFile = "LDFBLLCKS";
bool exconLoadExercise = false;
bool exconStartExercise = false; 
bool exconStopExercise = false;
bool exconResumeExercise = false;
bool exconPauseExercise = false;
bool exconStartTrainingSession = false;
bool exconLeaveTrainingSession = false;
bool exconCourseComplete = false;
bool exconPhaseComplete = false;
std::string exconDisplayMessage = "HELP ME";
std::vector<long> exconEntityIDs = { 0 };
unsigned long exconOVEntityID = 0;

//
// ****** Turret Control ****** 
//
unsigned long exconTurretRestockAmmoID = 0;

std::map<unsigned long, WeaponElevation> weaponElevationUpdateRequests;
bool weaponElevationUpdateRequestsPending;

std::map<unsigned long, double> turretHeadingUpdateRequests;
bool turretHeadingUpdateRequestsPending;

//This is a set so that only one request can be set per vehicle until it is processed
std::set<unsigned long> laserFireRequests;
bool laserFireRequestsPending = false;

TurretUpdateTransmitter* turretUpdateTransmitterCopy;
TurretControlTransmitter* turretControlTransmitterCopy;

//Sight Update
long exconSelectedSightEntityID;
long exconSelectedSightID;
unsigned long exconSightPosEntityId;
unsigned long exconSightPosSightId;
double exconBoreSightLineError = 0;
double exconBoreSightElevationError = 0;

SightUpdateTransmitter*				   sightUpdateTransmitterCopy;

//
// ****** Weapon Control ****** 
//
unsigned short exconWeaponControlWeaponID = 0;
unsigned short exconWeaponControlFireID = 0;
bool exconWeaponAmmoChange = false; 
WeaponControl::S_POSITION_AND_ORIENTATION exconWeaponControlFireBarrelbase = { 0,0,0 };
double exconWeaponControlFireChassisHeading = 0;
unsigned short exconWeaponControlSelectWeaponID = 0;
unsigned short exconWeaponControlSelectAmmoTypeID = 0;

WeaponUpdateTransmitter*               weaponUpdateTransmitterCopy;
ProjectileUpdateTransmitter*		   projectileUpdateTransmitterCopy;
EngagementEventTransmitter*	           engagementEventTransmitterCopy;

//
//  ****** Mission Function ******
//
// Requests
std::vector<unsigned long> missfuncnumOfLOSReqIds;
std::vector<unsigned long> missfuncLOSRequestID;
std::vector<unsigned long> missfuncLOSRequestStartID;
std::vector<MissionFunctionRequest::S_ENU_POSITION> missfuncLOSRequestStartpos;
std::vector<unsigned long> missfuncLOSRequestEndID;

std::vector<unsigned long> missfuncLOSRequestEndposIDs;
std::vector<MissionFunctionRequest::S_ENU_POSITION> missfuncLOSRequestEndpos;

std::vector<unsigned long> missfuncCancelLOSRequestID;

//Responses
unsigned long numOfResponses = 0;
MissionFunctionResponseTransmitter*    missionFunctionResponseTransmitterCopy;

//
//  ****** Conversion values ******
//
double radsToDegs = 57.295779513;
double degsToRads = 0.01745329252;
double KMHToMPS = 0.277777778;
double MPSToKMH = 3.6;

const float64_t ORIENTATION_DELTA = 0.0001;
const float64_t POSITION_DELTA = 0.0001;
const float64_t VELOCITY_DELTA = 0.0001;

//Origin for map - needs to be configurable
//SHREWTON map
//Vector3f64_v3 mapOrigin{ 34400.02, 0.0, 19399.98 };  // note Y an Z are transposed

//SALS map
Vector3f64_v3 mapOrigin{ 24400.0, 0.0, 19400.0 };  // note Y an Z are transposed

bool enable_log_output = true;

void ShowAndLog(const char* str)
{
	if (enable_log_output)
	{
		SDKCheck(Gears::API::GUIAPIv1()->DisplayMessage(str));
		auto log_api = Gears::API::LogAPIv1();
		log_api->LogInfo(log_api, str);
	}
}

bool TrainerView::Init()
{
	SDKCheck(_main_window->GetClientWidth(_main_window, &_client_width));
	SDKCheck(_main_window->GetClientHeight(_main_window, &_client_height));

	SDKCheck(Gears::API::VBS3ApplicationAPIv1()->GetCurrentState(&state));
	_in_mission = state == kVbs3ApplicationState_mission;

	//Clear mission request/response data
	ClearMissionData();
	
	if (!_in_mission)
	{
		// Start empty mission
		return APIRESULT_SUCCESS(Gears::API::MissionAPIv3()->StartEmptyMission("tt_spta_dvs", TRUE));
	}

	return true;
}

void TrainerView::OnMissionStart()
{
	// Set up environment
	float32_t overcast_value = 0.0;
	SDKCheck(Gears::API::EnvironmentAPIv1()->SetOvercast(overcast_value, 0.0));
	SDKCheck(Gears::API::EnvironmentAPIv1()->EnableAmbientLife(disable));

	_mission_started = true;

	const GeoPosition_v5 position = { 51.19193185, -1.9026947, 160.0 };

	// Create empty vehicle and snap it to the ground
	SDKCheck(Gears::API::WorldAPIv5()->CreateObject("cae_fv510_wdl_x", position, &_dead_player));
	SDKCheck(Gears::API::EnvironmentAPIv1()->AlignToTerrain(_dead_player));
	// Set camera behind vehicle
	SQFSwitchCamera(_dead_player, kCameraLinkType_third_person);
}

void TrainerView::OnBeforeSimulation(float delta)
{
  SDKCheck(Gears::API::VBS3ApplicationAPIv1()->GetCurrentState(&state));
  _in_mission = state == kVbs3ApplicationState_mission;

  if (!_mission_started || !_in_mission)
  {
    return;
  }

  //Initialise DDS transmitters and receivers
  if (!initialiseDDS)
  {
	 SharedPtr<LockObject> messageLock(new LockObject);
	 MessageLock = messageLock;

    InitialiseDDSData();
	initialiseDDS = true;
  }

  if (exconStartExercise)
  {
	  if (entityAddObjectIDs.size() > 0 && !initialiseDatabase)
	{
		// Load database so heights can be found for new objects
		GeoPosition_v5 position = { 51.19193185, -1.9026947, 160.0 };

		// OV object first in array
		ConvertENUToLLA(entityAddLocationAndHeading[0].position.ECEF_X,
			            entityAddLocationAndHeading[0].position.ECEF_Y,
			            entityAddLocationAndHeading[0].position.ECEF_Z);

		SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs2_landscaping_mudpatch", latLongPosition, &_ownship));

		SQFSwitchCamera(_ownship, kCameraLinkType_third_person);

		SQFDisableActionMenu();
		SQFDisableUserInput();

		initialiseDatabase = true;
		ShowAndLog("initialiseDatabase");
	}

	ControlEntities(delta);

	if (ValidateHandle(_ownship))
	{
		ControlTurrets();
		ControlSights(delta);
		ControlWeapons(delta);
		MissionFunctionLOS(delta);
		EntityUpdatesForCIOS();
	}
  }

  // Exercise Control messages
  ControlExercise(delta);
 
}

void TrainerView::OnRenderMainWindow()
{
	const char text[] =
		"Exercise View baby"
		;
	bool32_t opened = TRUE;

	SDKCheck(Gears::API::IMGuiAPIv1()->Begin("Exercise View information", kImGuiWindowFlags_AlwaysAutoResize, &opened, &opened));
	SDKCheck(Gears::API::IMGuiAPIv1()->Text(text));
	SDKCheck(Gears::API::IMGuiAPIv1()->End());
}

void TrainerView::InitialiseDDSData()
{
	//****************************
	//       TRANSMITTER
	//****************************
	DDSTransmitterInterface* ddsTransmitterInterface = new DDSTransmitterInterface;

	// ****** Entity Update *******
	EntityUpdateTransmitter*            entityUpdateTransmitter = ddsTransmitterInterface->GetEntityUpdateTransmitter();
	entityUpdateTransmitter->Initialise();

	entityUpdateTransmitterCopy = entityUpdateTransmitter;

	// ****** Turret Update *******
	TurretUpdateTransmitter* turretUpdateTransmitter = ddsTransmitterInterface->GetTurretUpdateTransmitter();
	turretUpdateTransmitter->Initialise();

	turretUpdateTransmitterCopy = turretUpdateTransmitter;

	// ****** Weapon Update *******
	WeaponUpdateTransmitter* weaponUpdateTransmitter = ddsTransmitterInterface->GetWeaponUpdateTransmitter();
	weaponUpdateTransmitter->Initialise();

	weaponUpdateTransmitterCopy = weaponUpdateTransmitter;

	// ****** Projectile Update *******
	ProjectileUpdateTransmitter*  projectileUpdateTransmitter = ddsTransmitterInterface->GetProjectileUpdateTransmitter();
	projectileUpdateTransmitter->Initialise();

	projectileUpdateTransmitterCopy = projectileUpdateTransmitter;

	// ******Mission Function Response *******
	MissionFunctionResponseTransmitter*  missionFunctionResponseTransmitter = ddsTransmitterInterface->GetMissionFunctionResponseTransmitter();
	missionFunctionResponseTransmitter->Initialise();

	missionFunctionResponseTransmitterCopy = missionFunctionResponseTransmitter;

	// ****** Engagement Event *******
	EngagementEventTransmitter*	  engagementEventTransmitter = ddsTransmitterInterface->GetEngagementEventTransmitter();
	engagementEventTransmitter->Initialise();

	engagementEventTransmitterCopy = engagementEventTransmitter;

	// ****** Sight  Update Transmit*******
	SightUpdateTransmitter*	sightUpdateTransmitter = ddsTransmitterInterface->GetSightUpdateTransmitter();
	sightUpdateTransmitter->Initialise();

	sightUpdateTransmitterCopy = sightUpdateTransmitter;

	// ****** Turret Control Transmit - only used for keyboard input *******
	TurretControlTransmitter* turretControlTransmitter = ddsTransmitterInterface->GetTurretControlTransmitter();
	turretControlTransmitter->Initialise();

	turretControlTransmitterCopy = turretControlTransmitter;

	//****************************
	//          RECEIVER
	//****************************

	DDSReceiverInterface* ddsReceiverInterface = new DDSReceiverInterface();

	// ****** Exercise Control *******
	DDSExerciseControl* ddsExerciseControl = new DDSExerciseControl;
	ExerciseControlReceiver* exerciseControlReceiver = ddsReceiverInterface->GetExerciseControlReceiver();
	exerciseControlReceiver->Initialise(ddsExerciseControl);

	// ****** Entity Control *******
	DDSEntityControl*   ddsEntityControl = new DDSEntityControl;
	EntityControlReceiver* entityControlReceiver = ddsReceiverInterface->GetEntityControlReceiver();
	entityControlReceiver->Initialise(ddsEntityControl);

	// ****** Active Entity Update *******
	DDSActiveEntityStatusUpdate*   ddsActiveEntityStatusUpdate = new DDSActiveEntityStatusUpdate;
	ActiveEntityStatusUpdateReceiver* activeEntityStatusUpdateReceiver = ddsReceiverInterface->GetActiveEntityStatusUpdateReceiver();
	activeEntityStatusUpdateReceiver->Initialise(ddsActiveEntityStatusUpdate);

	// ****** Turret Control *******
	DDSTurretControl*   ddsTurretControl = new DDSTurretControl;
	TurretControlReceiver*  turretControlReceiver = ddsReceiverInterface->GetTurretControlReceiver();
	turretControlReceiver->Initialise(ddsTurretControl);

	// ****** Weapon Control *******
	DDSWeaponControl*   ddsWeaponControl = new DDSWeaponControl;
	WeaponControlReceiver* weaponControlReceiver = ddsReceiverInterface->GetWeaponControlReceiver();
	weaponControlReceiver->Initialise(ddsWeaponControl);

	// ****** Asset Control *******
	DDSAssetControl*	           ddsAssetControl = new DDSAssetControl;
	AssetControlReceiver*	assetControlReceiver = ddsReceiverInterface->GetAssetControlReceiver();
	assetControlReceiver->Initialise(ddsAssetControl);

	// ****** Mission Function Request *******
	DDSMissionFunctionRequest*     ddsMissionFunctionRequest = new DDSMissionFunctionRequest;
	MissionFunctionRequestReceiver*	missionFunctionRequestReceiver = ddsReceiverInterface->GetMissionFunctionRequestReceiver();
	missionFunctionRequestReceiver->Initialise(ddsMissionFunctionRequest);

	// ****** Sight  Update Receive*******
	DDSSightUpdate*      ddsSightUpdate = new DDSSightUpdate;
	SightUpdateReceiver* sightUpdateReceiver = ddsReceiverInterface->GetSightUpdateReceiver();
	sightUpdateReceiver->Initialise(ddsSightUpdate);

	// ****** Remote Exercise Control *******
	DDSRemoteExerciseControl*      ddsRemoteExerciseControl = new DDSRemoteExerciseControl;
	RemoteExerciseControlReceiver* remoteExerciseControlReceiver = ddsReceiverInterface->GetRemoteExerciseControlReceiver();
	remoteExerciseControlReceiver->Initialise(ddsRemoteExerciseControl);

	// ****** Engagement Event *******
	DDSEngagementEvent*            ddsEngagementEvent = new DDSEngagementEvent;
	EngagementEventReceiver*       engagementEventReceiver = ddsReceiverInterface->GetEngagementEventReceiver();
	engagementEventReceiver->Initialise(ddsEngagementEvent);
}

void TrainerView::ControlExercise(float delta)
{
	if (exconStartExercise)
	{
		exconLoadExercise = false;
	}

	if (exconLoadExercise)
	{
		DeleteAllObjects();
		exconLoadExercise = false;
		ShowAndLog("load exercise");
	}

	if (exconStopExercise)
	{
        // clear all objects and flags
		DeleteAllObjects();
		exconStartTrainingSession = false;
		exconLoadExercise = false;
		exconStartExercise = false;
		exconStopExercise = false;
	}

}

void TrainerView::ControlEntities(float delta)
{
	ScopeLock lock(MessageLock);

	// Create new entity in visual scene
	if (addEntities)
	{
		for (int i = 0; i < entityAddObjectIDs.size(); i++)
		{
			if (entityAddObjectIDs[i] > 0)
			{
				ConvertENUToLLA(entityAddLocationAndHeading[i].position.ECEF_X,
					            entityAddLocationAndHeading[i].position.ECEF_Y,
					            entityAddLocationAndHeading[i].position.ECEF_Z);

				CreateObject(latLongPosition.latitude, latLongPosition.longitude, entityAddObjectIDs[i], entityAddEntityTypeId[i],
					                   (entityAddLocationAndHeading[i].heading));
			}
			entityAddObjectIDs[i] = 0;
		}

		addEntities = false;
		entityAddObjectIDs.clear();
		entityAddEntityTypeId.clear();
		entityAddLocationAndHeading.clear();
	}

	// Remove entity in visual scene
	if (entityRemoveObjectIdentifier > 0 || exconKillEntityID > 0 )
	{
		long deleteobj = ObjectAlreadyExists(entityRemoveObjectIdentifier);

		// If no remove object ID get kill ID
		if (deleteobj == 0)
		{
			deleteobj = ObjectAlreadyExists(exconKillEntityID);
		}

		// Only delete object if present
		if (deleteobj > 0)
		{
			DeleteObject(deleteobj);

			entityRemoveObjectIdentifier = 0;
			exconKillEntityID = 0;
		}
	}

	// Update entity position
	else if (entityGoToObjectIdentifier.size() > 0 && _csa_object_identifier.size() > 0)
	{

		for (int i = 0; i < entityGoToObjectIdentifier.size(); i++)
		{
			long updateobj = ObjectAlreadyExists(entityGoToObjectIdentifier[i]);

			if (updateobj)
			{
				ControlObjectPosition(updateobj, i);
				entityGoToObjectIdentifier.erase(entityGoToObjectIdentifier.begin() + i);
				entityGoToLocation.erase(entityGoToLocation.begin() + i);
				entityGoToSpeed.erase(entityGoToSpeed.begin() + i);
				entityGoToDwellTime.erase(entityGoToDwellTime.begin() + i);
			}
		}
	}

	else if (entityDamageInput.size() > 0)
	{
		ControlDamage();
		entityDamageInput.clear();
	}
}

void TrainerView::ControlTurrets(void)
{
	ScopeLock lock(MessageLock);

	//  Turret Heading / Gun Elevation values receivd are in signed radians.
	//	Assume + / - PI where + ve value is a traverse to the right or elevation up.
	//	The Turret switches from + ve to �ve as it passes through the point where it faces directly back.
	//

	// Fire laser only in sight optics view.
	// The laser cannot be fired when the Unity window is in use.
	if (laserFireRequestsPending && SQFSwitchCameraView_mode == kOpticsViewMode && exconSelectedSightID == sightID_x8)
	{
		auto iterator = laserFireRequests.begin();
		while (iterator != laserFireRequests.end())
		{
			// Only fire the OV laser for now
			if (*iterator == exconOVEntityID)
				FireOVLaser();
			iterator++;
		}
		laserFireRequests.clear();
		laserFireRequestsPending = false;
	}

	if (turretHeadingUpdateRequestsPending)
	{
		auto iterator = turretHeadingUpdateRequests.begin();
		while (iterator != turretHeadingUpdateRequests.end())
		{
			// Only update the OV turret heading for now
			if (iterator->first == exconOVEntityID)
			{
				UpdateOVTurretHeading(iterator->first, iterator->second);
				ovTurretHeadingUpdated = true;
			}
			iterator++;
		}
		turretHeadingUpdateRequestsPending = false;
		turretHeadingUpdateRequests.clear();
	}

	if (weaponElevationUpdateRequestsPending)
	{
		auto iterator = weaponElevationUpdateRequests.begin();
		while (iterator != weaponElevationUpdateRequests.end())
		{
			// Only update the OV weapon elevation for now
			if (iterator->first == exconOVEntityID)
				UpdateOVWeaponElevation(iterator->first, iterator->second.weaponElevation);
			iterator++;
		}
		weaponElevationUpdateRequestsPending = false;
		weaponElevationUpdateRequests.clear();
	}

	TurretUpdatesForCIOS(exconOVEntityID);
}

void TrainerView::ControlSights(float delta)
{
    //Note:  Sight Name & SightId :
    //
    //	Stealth = 12
    //	Warrior Gun Unity = 15
    //	Warrior Gunner X8 Optical = 16

	if (ValidateHandle(_ownship) && !initialiseSightToUnity)
	{
		SQFSwitchCamera(_ownship, kCameraLinkType_optics);
		optics_enabled = true;
		ShowAndLog("KOptics view");

		exconSelectedSightID = sightID_unity;
		initialiseSightToUnity = sightID_unity;
	}

	if (!(exconSelectedSightID == last_selected_sight) && SQFSwitchCameraView_mode == kOpticsViewMode)
	{
		if (exconSelectedSightID == sightID_unity)
		{
			sight_device_index = kSightDeviceType_unity;
		}
		else if (exconSelectedSightID == sightID_x8)
		{
			sight_device_index = kSightDeviceType_x8;
		}

		SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->SetActiveWeaponOptics(_ownship_turret, &sight_device_index, &sight_mode_index));

		last_selected_sight = exconSelectedSightID;
		exconSelectedSightEntityID = 0;
	}
	
	if (ValidateHandle(_ownship_turret))
	{
		SightUpdatesForCIOS(exconOVEntityID, _ownship);
	}
}

void TrainerView::ControlWeapons(float delta)
{
	// Find the turret object
	_vehicle_turret = _ownship_turret; // set to OV at present 

	if (exconWeaponControlSelectWeaponID && exconWeaponAmmoChange)
	{
		if (exconWeaponControlSelectWeaponID == weaponID_rarden)
		{
			SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->SetActiveWeapon(_vehicle_turret, _rarden, kWeaponAspectAutoSelectValue, kWeaponAspectAutoSelectValue, kWeaponAspectAutoSelectValue));

			// SetActiveWeapon(object, weapon_index, magazine_index, muzzle_index, mode_index)
			//
			if (exconWeaponControlSelectAmmoTypeID == ammotype_APDS)
			{
				SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->SetActiveWeapon(_vehicle_turret, _rarden, cae_mag_90rnd_30mm_APDS_rarden, kWeaponAspectAutoSelectValue, kWeaponAspectAutoSelectValue));
			}
			else if (exconWeaponControlSelectAmmoTypeID == ammotype_HEI)
			{
				SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->SetActiveWeapon(_vehicle_turret, _rarden, cae_mag_90rnd_30mm_HEI_rarden, kWeaponAspectAutoSelectValue, kWeaponAspectAutoSelectValue));
			}
		}
		else if (exconWeaponControlSelectWeaponID == weaponID_chaingun)
		{
			SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->SetActiveWeapon(_vehicle_turret, _chaingun, kWeaponAspectAutoSelectValue, kWeaponAspectAutoSelectValue, kWeaponAspectAutoSelectValue));
		}

		exconWeaponAmmoChange = false;

		// Take safety OFF
		SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->EnableActiveWeaponSafety(_vehicle_turret, FALSE));
	}

	if (exconWeaponControlFireID)
	{
		// Get the vehicle associated with the turret
		ObjectHandle_v3 platform;
		ObjectHandle_v3* ptr_platform = &platform;
		int32_t* crewindex = 0;
		SDKCheck(Gears::API::TurretAspectAPIv1()->GetPlatformOn(_vehicle_turret, ptr_platform, crewindex));

		// vehicle_turret overriden at top of function to be ownship turret
		if (_vehicle_turret != kNullObjectHandle)
		{
			SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->FireActiveWeapon(_vehicle_turret, "Boom"));
		}

		// Updates to CIOS
		// ONLY ownship at the moment
		WeaponUpdatesForCIOS(exconOVEntityID);

		exconWeaponControlFireID = 0;

		ShowAndLog("Boom");
	}

	if (shot_detonated || shot_fired)
	{
		// Send updates to Sim
		ProjectileUpdatesForCIOS(exconOVEntityID);
	}
}

void TrainerView::MissionFunctionLOS(float delta)
{
	ScopeLock lock(MessageLock);

	if (missfuncnumOfLOSReqIds.size() > 0)
	{
		for (int i = 0; i < missfuncLOSRequestID.size(); i++)
		{
			// get number of request Ids per message block
			if (i > 0)
			{
				EndposOffset += missfuncnumOfLOSReqIds[i - 1];
			}

			LOSRequestID = missfuncLOSRequestID[i];
			LOSRequestStartID = missfuncLOSRequestStartID[i];
			ENURequestStartpos = missfuncLOSRequestStartpos[i];
			LOSRequestEndID = missfuncLOSRequestEndID[i];

			numOfLOSReqIds[i] = missfuncnumOfLOSReqIds[i];

			for (unsigned long j = 0; j < missfuncnumOfLOSReqIds[i]; j++)
			{
				// Request ID and ENU End Position
				LOSRequestEndposIDs.push_back(missfuncLOSRequestEndposIDs[j + EndposOffset]);
				ENURequestEndpos.push_back(missfuncLOSRequestEndpos[j + EndposOffset]);
			}

			MissionFunctionENUConvertLLA(delta, numOfLOSReqIds[i]);
		}
	}

	ClearMissionData();
}

void TrainerView::MissionFunctionENUConvertLLA(float delta,unsigned long numOfReqIds)
{
	GeoPosition_v5 Endpos = { 0,0,0 };
	
	ConvertENUToLLA(ENURequestStartpos.ENU_X, ENURequestStartpos.ENU_Y, ENURequestStartpos.ENU_Z);
	LLARequestStartpos.altitude = latLongPosition.altitude;
	LLARequestStartpos.latitude = latLongPosition.latitude;
	LLARequestStartpos.longitude = latLongPosition.longitude;
	
	LLARequestEndpos.resize(numOfReqIds);

	for (unsigned long j = 0; j < numOfReqIds; j++)
	{
		// Request ID and ENU End Position
		ConvertENUToLLA(ENURequestEndpos[j].ENU_X, ENURequestEndpos[j].ENU_Y, ENURequestEndpos[j].ENU_Z);
		Endpos.altitude = latLongPosition.altitude;
		Endpos.latitude = latLongPosition.latitude;
		Endpos.longitude = latLongPosition.longitude;

		LLARequestEndpos.at(j) = latLongPosition;
	}

	MissionFunctionIntersectionChecks(delta);
}

void TrainerView::MissionFunctionIntersectionChecks(float delta)
{
	// Set up intersection input parameters
	intersectionInput.begin_point = LLARequestStartpos;
	intersectionInput.geometry = kGeometryType_collision;
	intersectionInput.intersection_type = kIntersectionType_v4_All;
	intersectionInput.minimum_detail = 0;
	intersectionInput.maximum_detail = 0;
	intersectionInput.ignored_objects = 0;
	intersectionInput.ignored_objects_count = 0;
	intersectionInput.altitude_reference = kAltitudeReference_geoid;

	int index_vbs_obj = FindVBSIDfromcsaID(LOSRequestEndID);
	if (index_vbs_obj != -1)
	{

		for (int j = 0; j < LLARequestEndpos.size(); j++)
		{
			intersectionInput.end_point = LLARequestEndpos[j];
			// Set to 1 every time because of API problem - To Be Fixed by Bohemia
			noOfIntersections = 1;

			SDKCheck(Gears::API::WorldAPIv5()->CalculateIntersection(&intersectionInput, &intersectionOutput, &noOfIntersections));

			if (noOfIntersections > 0 && (_vbs_object_identifier.at(index_vbs_obj) != intersectionOutput.object))
			{
				intersectionObject[j] = intersectionOutput.object;
				intersectionLocal_position[j] = intersectionOutput.local_position;
				intersectionDistance[j] = intersectionOutput.distance;
				intersectionType[j] = intersectionOutput.type;
				intersectionPosition[j] = intersectionOutput.position;
			}

		}

		MissionResponseForCIOS(delta);
	}

}

void TrainerView::EntityUpdatesForCIOS()
{
	bool publishEntityUpdate = false;
	unsigned long entityID = 0;
	GeoPosition_v5 entity_position = { 0, 0, 0 }; 
	EntityUpdate::S_ENU_ORIENTATION orient = { 0.0, 0.0, 0.0 };
	Vector3f32_v3 velocity = { 0, 0, 0 };
	RotationalAngles_v3 objOrientation = { 0, 0, 0 };

	for (int i = 0; i < _csa_object_identifier.size(); i++)
	{
		ObjectHandle_v3 obj = _vbs_object_identifier.at(i);
		if (ValidateHandle(obj))
		{
			auto iterator = _csa_object_data.find(_csa_object_identifier.at(i));
			if (iterator != _csa_object_data.end())
			{
				// Get VBS object and its position

				SDKCheck(Gears::API::TransformationAspectAPIv4()->GetPosition(obj, &entity_position));

				if (abs(entity_position.latitude - iterator->second.position.latitude) > POSITION_DELTA ||
					abs(entity_position.longitude - iterator->second.position.longitude) > POSITION_DELTA ||
					abs(entity_position.altitude - iterator->second.position.altitude) > POSITION_DELTA)
				{
					publishEntityUpdate = true;
					iterator->second.position.latitude = entity_position.latitude;
					iterator->second.position.longitude = entity_position.longitude;
					iterator->second.position.altitude = entity_position.altitude;
				}

		        // model velocity:	Object velocity in model space (right, up, forward) in meters per second
				SDKCheck(Gears::API::TransformationAspectAPIv4()->GetModelVelocity(obj, &velocity));
				if (abs(velocity.x - iterator->second.velocity.x) > VELOCITY_DELTA ||
					abs(velocity.y - iterator->second.velocity.y) > VELOCITY_DELTA ||
					abs(velocity.z - iterator->second.velocity.z) > VELOCITY_DELTA)
				{
					publishEntityUpdate = true;
					iterator->second.velocity.x = velocity.x;
					iterator->second.velocity.y = velocity.y;
					iterator->second.velocity.z = velocity.z;
				}

				// ORIENT (yaw,pitch,roll -> heading,pitch,roll)
				SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(obj, &objOrientation));
				if (abs(objOrientation.pitch - iterator->second.orientation.pitch) > ORIENTATION_DELTA ||
					abs(objOrientation.roll - iterator->second.orientation.roll) > ORIENTATION_DELTA ||
					abs(objOrientation.yaw - iterator->second.orientation.yaw) > ORIENTATION_DELTA)
				{
					publishEntityUpdate = true;
					iterator->second.orientation.pitch = objOrientation.pitch;
					iterator->second.orientation.roll = objOrientation.roll;
					iterator->second.orientation.yaw = objOrientation.yaw;
				}

				if (publishEntityUpdate)
				{
					// ENTITYID ON CIOS
					entityID = _csa_object_identifier[i];
					//  Converts LLA to ENU and writes into entityUpdatePosition
					SDKCheck(Gears::API::TransformationAspectAPIv4()->GetPosition(obj, &latLongPosition));
					ConvertLLAToENU();

					// model velocity:	Object velocity in model space (x:right,y:up,z:forward) in meters per second
					double speed = velocity.z * MPSToKMH;

					orient.heading = degsToRads * objOrientation.yaw;
					orient.pitch = degsToRads * objOrientation.pitch;
					orient.roll = degsToRads * -1 * objOrientation.roll;

					// Send updates to CIOS
					if (publishEntityUpdate)
						entityUpdateTransmitterCopy->EntityBodyUpdate(entityID, speed, entityUpdatePosition, orient);
				}
			}
		}
		else
		{
			// VBS object deleted remove related csa ID
			DeleteObject(_csa_object_identifier.at(i));
		}
	}
}

void TrainerView::TurretUpdatesForCIOS(unsigned long csaObjId)
{
	// Get tank heading
	RotationalAngles_v3 tank_orientation = {0,0,0};
	int index_vbs_obj = FindVBSIDfromcsaID(csaObjId);
	if (index_vbs_obj != -1)
	{
		if (ovTurretHeadingUpdated)
		{
			SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(_vbs_object_identifier.at(index_vbs_obj), &tank_orientation));

			RotationalAngles_v3 turret_orientation;

			ObjectHandle_v3 turretObject = FindTurret(csaObjId);
			if (!(turretObject == kNullObjectHandle))
			{
				SDKCheck(Gears::API::TurretAspectAPIv1()->GetOrientation(turretObject, &turret_orientation));
				// CIOS heading is relative to tank
				turret_orientation.yaw = turret_orientation.yaw - tank_orientation.yaw;

				double turrethead = degsToRads * turret_orientation.yaw;
				turretUpdateTransmitterCopy->TurretHeading(csaObjId, turrethead);
			}
			ovTurretHeadingUpdated = false;
		}
	

		//Send out a FireLaser message
		//This would normally be generated externally when the footswitch is used
		if (laserFiredfromKeyboard)
		{
			turretControlTransmitterCopy->FireLaser(csaObjId);
			laserFiredfromKeyboard = false;
		}
	}
}

void TrainerView::WeaponUpdatesForCIOS(unsigned long csaObjId)
{
	weaponUpdateTransmitterCopy->WeaponFire(csaObjId, exconWeaponControlSelectWeaponID, exconWeaponControlSelectAmmoTypeID, exconWeaponUpdateFireBurstProjID);
	exconWeaponControlSelectWeaponID = 0;

	if (exconWeaponUpdateFireBurstProjID > 0 )
	{
		//numOfProjectiles = (unsigned long)exconWeaponUpdateFireBurstProjID.size();
		numOfProjectiles = 1;
		unsigned long* ptr_projectiles = &exconWeaponUpdateFireBurstProjID;
		weaponUpdateTransmitterCopy->BurstLength(csaObjId, exconWeaponControlSelectAmmoTypeID, numOfProjectiles, ptr_projectiles);
	}
}

void TrainerView::ProjectileUpdatesForCIOS(unsigned long csaObjId)
{
	if (shot_detonated)
	{
		//  Converts LLA to ENU and writes into entityUpdatePosition
		latLongPosition = detonation_position;
		ConvertLLAToENU();

		ProjectileUpdate::S_ENU_POSITION detonatePosition;
		detonatePosition.ENU_X = entityUpdatePosition.ENU_X;
		detonatePosition.ENU_Y = entityUpdatePosition.ENU_Y;
		detonatePosition.ENU_Z = entityUpdatePosition.ENU_Z;

		projectileUpdateTransmitterCopy->FallOfShotEvent(csaObjId, exconWeaponControlSelectAmmoTypeID, exconWeaponUpdateFireBurstProjID, detonatePosition);

		target_endpos = false;
		shot_detonated = false;
		shot_fired = false;
		numOfProjectiles = 0;
		exconWeaponUpdateFireBurstProjID = 0;
		Lastime_shot_Flying_Position = { 0,0,0 };
		//exconWeaponUpdateFireBurstProjID.erase(exconWeaponUpdateFireBurstProjID.begin());
	}
	
	if (shot_fired )
	{
		shot_Flying_Position = GetPosition(shot_objectID);

		// Offset shot position if sight error set
		if (exconBoreSightLineError || exconBoreSightElevationError)
		{
			shot_Flying_Position.latitude = shot_Flying_Position.latitude + exconBoreSightLineError;
			shot_Flying_Position.longitude = shot_Flying_Position.longitude + exconBoreSightLineError;
			shot_Flying_Position.altitude = shot_Flying_Position.altitude + exconBoreSightElevationError;
			SDKCheck(Gears::API::TransformationAspectAPIv4()->SetPosition(shot_objectID, shot_Flying_Position));
		}

		if (Lastime_shot_Flying_Position.latitude > 0)
		{
			latLongPosition = Lastime_shot_Flying_Position;
			ConvertLLAToENU();

			ProjectileUpdate::S_ENU_POSITION startPosition;
			startPosition.ENU_X = entityUpdatePosition.ENU_X;
			startPosition.ENU_Y = entityUpdatePosition.ENU_Y;
			startPosition.ENU_Z = entityUpdatePosition.ENU_Z;

			latLongPosition = shot_Flying_Position;
			ConvertLLAToENU();
			ProjectileUpdate::S_ENU_POSITION endPosition;
			endPosition.ENU_X = entityUpdatePosition.ENU_X;
			endPosition.ENU_Y = entityUpdatePosition.ENU_Y;
			endPosition.ENU_Z = entityUpdatePosition.ENU_Z;

			projectileUpdateTransmitterCopy->ProjectileTrajectory(csaObjId, exconWeaponControlSelectAmmoTypeID, exconWeaponUpdateFireBurstProjID, startPosition, endPosition);
		}

		Lastime_shot_Flying_Position = shot_Flying_Position;
	}
}

void TrainerView::ControlDamage()
{
	auto iterator = entityDamageInput.begin();
	while (iterator != entityDamageInput.end())
	{
		int index_vbs_obj = FindVBSIDfromcsaID(iterator->first);
		if (index_vbs_obj != -1)
		{
			// CIOS sends 0:good, 1:partly damaged, 2:dead
			// VBS takes damage in the range 0-1
			double damage = iterator->second * 0.5;
			SDKCheck(Gears::API::DamageAspectAPIv2()->SetDamage(_vbs_object_identifier.at(index_vbs_obj), (float32_t)damage));
		}
		iterator++;
	}
}

void TrainerView::MissionResponseForCIOS(float delta)
{
	// Responses to CIOS called after a mission request
	numOfResponses = unsigned long (LOSRequestEndposIDs.size());
	MissionFunctionResponse::INTERSECT_RESPONSE* responses = new  MissionFunctionResponse::INTERSECT_RESPONSE[numOfResponses];

	unsigned long CIOStype = 0;

	for (unsigned long i = 0; i < numOfResponses; i++)
	{
		if (intersectionType[i] == kIntersectionType_v4_Object ||
			intersectionType[i] == kIntersectionType_v4_Building ||
			intersectionType[i] == kIntersectionType_v4_Ground ||
			intersectionType[i] == kIntersectionType_v4_Water ||
			intersectionType[i] == kIntersectionType_v4_Road ||
			intersectionType[i] == kIntersectionType_v4_Roadway ||
			intersectionType[i] == kIntersectionType_v4_Tree ||
			intersectionType[i] == kIntersectionType_v4_Bush)
		{
			CIOStype = 1;
		}
		responses[i].intersectTypeId = CIOStype;
		responses[i].lineOfSightRequestId = LOSRequestEndposIDs[i];

		CIOStype = 0;
	}
	// Send updates to CIOS
	missionFunctionResponseTransmitterCopy->MultiLineOfSightResponse(LOSRequestEndID, numOfResponses, responses);

	// Clear local copy of mission data
	LOSRequestID = 0;
	LOSRequestStartID = 0;
	ENURequestStartpos = { 0,0,0 };
	LOSRequestEndID = 0;
	LOSRequestEndposIDs.clear();
	ENURequestEndpos.clear();
	LLARequestEndpos.clear();
	LLARequestStartpos = { 0,0,0 };
	ClearArrays();
}

void TrainerView::SightUpdatesForCIOS(long selectedSightEntityID,ObjectHandle_v3 obj)
{
	bool publishSightData = false;
	long selected_sightID = sightID_x8;

	// Sight RECEIVE which sight selected
	if (sight_device_index == kSightDeviceType_unity)
	{
		selected_sightID = sightID_unity;
	}
	else
	{
		selected_sightID = sightID_x8;
	}

	RotationalAngles_v3 own_orientation;
	SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(_ownship, &own_orientation));

	RotationalAngles_v3 turret_orientation;
	SDKCheck(Gears::API::TurretAspectAPIv1()->GetOrientation(_ownship_turret, &turret_orientation));


	SDKCheck(Gears::API::TransformationAspectAPIv4()->GetPosition(sight_invis_object, &latLongPosition));
	if (abs(publishedSightPosition.latitude - latLongPosition.latitude) > POSITION_DELTA ||
		abs(publishedSightPosition.longitude - latLongPosition.longitude) > POSITION_DELTA ||
		abs(publishedSightPosition.altitude - latLongPosition.altitude) > POSITION_DELTA)
	{
		publishSightData = true;
	}

	RotationalAngles_v3 sight_orientation;
	SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(sight_invis_object, &sight_orientation));

	if (abs(publishedSightOrientation.pitch - sight_orientation.pitch) > ORIENTATION_DELTA ||
		abs(publishedSightOrientation.roll - sight_orientation.roll) > ORIENTATION_DELTA ||
		abs(publishedSightOrientation.yaw - sight_orientation.yaw) > ORIENTATION_DELTA)
		
	{
		publishSightData = true;
	}

	if(publishSightData)
	{
		SightUpdate::S_SIGHT_POSITION sight_position = { 0,0,0 };
		SightUpdate::S_SIGHT_ORIENTATION orientation = { 0,0,0 };

		//  Converts LLA to ENU and writes into entityUpdatePosition
		ConvertLLAToENU();

		sight_position.ECEF_X = entityUpdatePosition.ENU_X;
		sight_position.ECEF_Y = entityUpdatePosition.ENU_Y;
		sight_position.ECEF_Z = entityUpdatePosition.ENU_Z;

		orientation.theta = degsToRads * sight_orientation.pitch;
		orientation.phi = degsToRads * sight_orientation.roll;
		orientation.psi = degsToRads * sight_orientation.yaw;

		sightUpdateTransmitterCopy->SightPositionAndOrientation(selectedSightEntityID, selected_sightID, sight_position, orientation);

		publishedSightPosition.latitude = latLongPosition.latitude;
		publishedSightPosition.longitude = latLongPosition.longitude;
		publishedSightPosition.altitude = latLongPosition.altitude;
		publishedSightOrientation.pitch = sight_orientation.pitch;
		publishedSightOrientation.roll = sight_orientation.roll;
		publishedSightOrientation.yaw = sight_orientation.yaw;
	}
}

int TrainerView::FindVBSIDfromcsaID(unsigned long csaObjId)
{
	int index = -1;

	auto foundObjId = std::find(_csa_object_identifier.begin(), _csa_object_identifier.end(), (signed long)csaObjId);
	if (foundObjId == _csa_object_identifier.end())
	{
		ShowAndLog("csaID not found");
	}
	else
	{
		index = (int(std::distance(_csa_object_identifier.begin(), foundObjId)));
	}

	return index;
}

int TrainerView::FindcsaIDfromVBSID(ObjectHandle_v3 vbsObjID)
{
	int index = 0;

	auto foundObjId = std::find(_vbs_object_identifier.begin(), _vbs_object_identifier.end(), vbsObjID);
	if (foundObjId == _vbs_object_identifier.end())
	{
		ShowAndLog("VBS ID not found");
	}
	else
	{
		index = (int(std::distance(_vbs_object_identifier.begin(), foundObjId)));
	}

	return index;
}

long TrainerView::ObjectAlreadyExists(long csaObjId)
{
	long csaObjectID = 0;
	for (int i = 0; i < _csa_object_identifier.size(); i++)
	{
		if (_csa_object_identifier.at(i) == csaObjId)
		{
			csaObjectID = csaObjId;
		}
	}
	return csaObjectID;
}

bool TrainerView::CreateObject(float64_t ipLat, float64_t ipLon, unsigned long csaObjID, long objectType, double heading)
{
	std::pair<bool, GeoPosition_v5> entity_position = NewPosition(ipLat, ipLon);
	RotationalAngles_v3 orientation;
	ObjectHandle_v3 _new_obj = { 0,0 };
	bool tank_object = false;

	if (entity_position.first)
	{
		if (exconOVEntityID == csaObjID)
		{
			ownship_position = entity_position.second;
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("cae_fv510_wdl_x", ownship_position, &_ownship));

			//Heading
			SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(_ownship, &orientation));
			orientation.yaw = heading * radsToDegs;
			SDKCheck(Gears::API::TransformationAspectAPIv4()->SetOrientation(_ownship, orientation));

			SDKCheck(Gears::API::EnvironmentAPIv1()->AlignToTerrain(_ownship));

			CreateTankCrewAndTurret(_ownship, ownship_position, csaObjID);
			SDKCheck(Gears::API::MissionAPIv3()->SetPlayer(_object_gunner[0]));

			// Attach camera to OV
			SQFSwitchCamera(_ownship, kCameraLinkType_third_person);

			AddNewObject(_ownship, (csaObjID));

			// Set turret to OV
			_ownship_turret = _turret_objects[0];

			// object for getting sight position/orientation to CIOS
			sight_invis_object = AttachToGunnerOptics(_ownship, "mainturret_elev", "gunnerview");

		}
		else
		{
			if (objectType == (long)1.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Army_T80U_W_X", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)6.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_ARMY_BRDM_W_X", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)13.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Army_bmp1_D_X", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)19.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs_dvs_op_army_hip_k_wdl_x", entity_position.second, &_new_obj));
			}
			else if (objectType == (long)45.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_ZZ_Army_T62A_W", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)52.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs_opfor_2s19_grn_x", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)56.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Army_ZSU_23_4_W_X", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)57.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Ural_4320_W_Ammo_X", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)59.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("SquadleaderE", entity_position.second, &_new_obj));
			}
			else if (objectType == (long)62.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("cae_fv510_wdl_x", entity_position.second, &_new_obj));
				_ownship = _new_obj;
				tank_object = true;
			}
			else if (objectType == (long)65.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_CZ_Army_T72_D", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)67.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Army_BMP3_W_X", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)70.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_AU_Army_ARH_AntiTank_W_X", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)74.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_GB_RAF_CH47_G_M134_X", entity_position.second, &_new_obj));
				tank_object = true;
			}
			else if (objectType == (long)104.0 || objectType == (long)230.0 || objectType == (long)231.0 || objectType == (long)108.0 || objectType == (long)109.0) {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs2_target_gb_bs3_bmp_bs3_popup", entity_position.second, &_new_obj));
			}
			else {
				SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs2_visual_arrow_red", entity_position.second, &_new_obj));
			}

			//Heading
			SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(_new_obj, &orientation));
			orientation.yaw = heading * radsToDegs;
			SDKCheck(Gears::API::TransformationAspectAPIv4()->SetOrientation(_new_obj, orientation));

			SDKCheck(Gears::API::EnvironmentAPIv1()->AlignToTerrain(_new_obj));

			// Turn AI aspects off models
			//Careless: Units try to stay on roads, not caring about any cover.
			//SDKCheck(Gears::API::VBS3AIAspectAPIv2()->SetBehavior(_new_obj, kBehaviorMode_careless));
			//Interop: Object is not simulated by the engine, can be changed / moved by the API, changes are sent over the network.
			//SDKCheck(Gears::API::SimulationAspectAPIv1()->SetSimulationMode(_new_obj, kSimulationState_interop));

			if (tank_object)
			{
				CreateTankCrewAndTurret(_new_obj, entity_position.second, csaObjID);
			}
			
			// Detect shots hitting the vehicle
			SDKCheck(Gears::API::DamageAspectAPIv2()->RegisterEvent(_new_obj, kDamageEventType_hit_v2, "TrainerView"));
			SDKCheck(Gears::API::DamageAspectAPIv2()->RegisterEvent(_new_obj, kDamageEventType_damage_change_v2, "TrainerView"));
			AddNewObject(_new_obj, (csaObjID));

			// Disable damage for all objects
			SDKCheck(Gears::API::DamageAspectAPIv2()->EnableDamage(_new_obj, disable));
		}
	}
	return entity_position.first;
}

ObjectHandle_v3 TrainerView::AttachToGunnerOptics(const ObjectHandle_v3& vehicle, const char* optics_bone_name, const char* optics_memory_point_name)
{
	// Create an invisible object to attach to the memory point
	ObjectHandle_v3 invis_object = kNullObjectHandle;
	SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_invis_turret", {}, &invis_object));

	// Attach the invisible object to the vehicle first
	SDKCheck(Gears::API::TransformationAspectAPIv4()->SetAttachedTo(invis_object, vehicle));

	// Attach the invisible object to the memory point of the gunner's optic
	SDKCheck(Gears::API::TransformationAspectAPIv4()->SetAttachedToBone(invis_object, optics_bone_name));
	SDKCheck(Gears::API::TransformationAspectAPIv4()->SetAttachedToMemoryPoint(invis_object, optics_memory_point_name));

	// Clear out the current offsets (Attaching to an object keeps the original offset at the time of attaching)
	SDKCheck(Gears::API::TransformationAspectAPIv4()->SetLocalPosition(invis_object, { 0.0, 0.0, 0.0 }));
	SDKCheck(Gears::API::TransformationAspectAPIv4()->SetLocalOrientation(invis_object, { 0.0, 0.0, 0.0 }));

	return invis_object;
}

void TrainerView::CreateTankCrewAndTurret(ObjectHandle_v3 vehicle, GeoPosition_v5 position, unsigned long csaObjID)
{
	SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_GB_Army_AFVCrew_M_L22A2_None_CrewHelmet_None", position, &_gunner));
	SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_GB_Army_AFVCrew_M_L22A2_None_CrewHelmet_None", position, &_driver));
	SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_GB_Army_AFVCrew_M_L22A2_None_CrewHelmet_None", position, &_commander));

	//Interop: Object is not simulated by the engine, can be changed / moved by the API, changes are sent over the network.
	//SDKCheck(Gears::API::SimulationAspectAPIv1()->SetSimulationMode(_driver, kSimulationState_interop));

	_object_gunner.push_back(_gunner);
	_object_driver.push_back(_driver);
	_object_commander.push_back(_commander);

	// Get vehicle crew positions
	int num_positions = 0;

	SDKCheck(Gears::API::CrewAspectAPIv1()->GetCrewPositions(vehicle, nullptr, &num_positions));

	std::vector<CrewPosition_v1> positions(num_positions);

	SDKCheck(Gears::API::CrewAspectAPIv1()->GetCrewPositions(vehicle, positions.data(), &num_positions));

	// Assign gunner,driver,commander into the vehicle
	for (int i = 0; i < num_positions; i++)
	{
		if (positions[i].type == kCrewPositionType_driver)
		{
		    SDKCheck(Gears::API::CrewAspectAPIv1()->SetToPosition(vehicle, i, _driver));
		}
		else if (positions[i].type == kCrewPositionType_turret)
		{
			SDKCheck(Gears::API::CrewAspectAPIv1()->SetToPosition(vehicle, i, _gunner));
		}
		else if (positions[i].type == (kCrewPositionType_turret | kCrewPositionType_commander))
		{
			SDKCheck(Gears::API::CrewAspectAPIv1()->SetToPosition(vehicle, i, _commander));
		}
	}

	// Create group for moving tank object
	CreateTankGroup(_no_of_tanks);
	_no_of_tanks += 1;

	// Get number of vehicle turrets
	int num_turrets = 0;

	SDKCheck(Gears::API::TurretAspectAPIv1()->GetChildrenWithAspect(vehicle, nullptr, &num_turrets));

	// Get turrets and save first turret handle for shooting and other turret operations
	std::vector<ObjectHandle_v3> turrets(num_turrets);

	SDKCheck(Gears::API::TurretAspectAPIv1()->GetChildrenWithAspect(vehicle, turrets.data(), &num_turrets));

	if (num_turrets > 0)
	{
		_vehicle_turret = turrets.front();
		// Save turret
		_turret_objects.push_back(_vehicle_turret);
        // Save corresponding csa object IDs
		_object_with_turret.push_back( csaObjID);

		SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->RegisterEvent(_vehicle_turret, kMunitionEventType_fire, "TrainerView"));
	}

	// Get weapons for Ownship only
	if (exconOVEntityID == csaObjID)
	{
		// Get all turret weapons
		int num_weapons = 0;
		SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->GetWeaponCount(_vehicle_turret, &num_weapons));

		for (int iWeapon = 0; iWeapon < num_weapons; ++iWeapon)
		{
			char weaponName[256];
			int buffSize = 256;

			SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->GetWeaponName(_vehicle_turret, iWeapon, weaponName, &buffSize));

			if (_rarden < 0 && !_stricmp(weaponName, "cae_l21a1_rarden")) { _rarden = iWeapon; }
			if (_chaingun < 0 && !_stricmp(weaponName, "cae_l94a1")) { _chaingun = iWeapon; }
		}
	}
}

void TrainerView::CreateTankGroup(int32_t no_of_tanks)
{
	ObjectHandle_v3 group = { 0,0 };

	if (no_of_tanks == 0)
	{
		// ownship
		SDKCheck(Gears::API::WorldAPIv5()->CreateGroup(kFaction_blufor, &group));
	}
	else
	{
		SDKCheck(Gears::API::WorldAPIv5()->CreateGroup(kFaction_opfor, &group));
	}

	_tank_group.push_back(group);
	SDKCheck(Gears::API::ORBATAPIv2()->SetSuperiorGroup(_object_driver[no_of_tanks], _tank_group[no_of_tanks]));
	SDKCheck(Gears::API::ORBATAPIv2()->SetSuperiorGroup(_object_gunner[no_of_tanks], _tank_group[no_of_tanks]));
	SDKCheck(Gears::API::ORBATAPIv2()->SetSuperiorGroup(_object_commander[no_of_tanks], _tank_group[no_of_tanks]));
}

std::pair<bool, GeoPosition_v5> TrainerView::NewPosition(const float64_t& ipLat, const float64_t& ipLon)
{
	bool success = false;
	GeoPosition_v5 position;
	float64_t Alt = 0.0;
	float64_t* ipAlt = &Alt;

	position.latitude = ipLat;
	position.longitude = ipLon;

	APIResult result = Gears::API::EnvironmentAPIv1()->GetTerrainElevation(ipLat, ipLon, ipAlt);
	position.altitude = Alt;

	if (result == kAPIResult_GeneralSuccess)
	{
		success = true;
		position.altitude = Alt;
	}
	ShowAndLog("Exercise new position");

	return std::make_pair(success, position);
}

void TrainerView::AddNewObject(ObjectHandle_v3 obj, long csaObjectId)
{
	auto found = std::find( _vbs_object_identifier.begin(),  _vbs_object_identifier.end(), obj);
	if (found ==  _vbs_object_identifier.end())
	{
		_vbs_object_identifier.push_back(obj);
		_csa_object_identifier.push_back(csaObjectId);
		ObjectData objectData;
		_csa_object_data.insert(std::map<long, ObjectData>::value_type(csaObjectId, objectData));
		ShowAndLog("Exercise visual object added");
	}
	else
	{
		ShowAndLog("Exercise visual object already exists");
	}
}

void TrainerView::DeleteAllObjects()
{
	ScopeLock lock(MessageLock);

	// Create invisible object for player. VBS always needs a player.
	if (ValidateHandle(_ownship))
	{
		GeoPosition_v5 invisible_position = GetPosition(_ownship);
		SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs2_invisible_man_west", invisible_position, &_dead_player));
		SDKCheck(Gears::API::MissionAPIv3()->SetPlayer(_dead_player));
	}

	// Delete VBS models
	for (int i = 0; i < _object_gunner.size(); i++)
	{
		ObjectHandle_v3 obj = _object_gunner.at(i);
		SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(obj));
		obj = _object_driver.at(i);
		SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(obj));
		obj = _object_commander.at(i);
		SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(obj));
	}

	if (ValidateHandle(_gunner))
	{
		SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(_gunner));
		_gunner = kNullObjectHandle;
	}
	if (ValidateHandle(_driver))
	{
		SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(_driver));
		_driver = kNullObjectHandle;
	}
	if (ValidateHandle(_commander))
	{
		SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(_commander));
		_commander = kNullObjectHandle;
	}

	for (int i = 0; i < _vbs_object_identifier.size(); i++)
	{
		ObjectHandle_v3 obj = _vbs_object_identifier.at(i);
		SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(obj));
	}

	_ownship = kNullObjectHandle;
	_ownship_turret = kNullObjectHandle;
	_vehicle_turret = kNullObjectHandle;

	//Clear out vector arrays
	ClearVectors();
		// Empty mission function arrays
		ClearArrays();
	// Tank groups
	_no_of_tanks = 0;
	// Sight initialisation flags
	initialiseSightToUnity = 0;
	last_selected_sight = 0;
	exconBoreSightLineError = 0.0;
	exconBoreSightElevationError = 0.0;
	// Weapon flags
	shot_detonated = false;
	shot_fired = false;
	target_hit_ID = kNullObjectHandle;
	Lastime_shot_Flying_Position = { 0,0,0 };
	if (draw_hit_lines || draw_laser_lines)
	{
		ClearLinesOfShot();
	}

	ShowAndLog("ALL objects deleted");
}

void TrainerView::DeleteObject(long csaObjId)
{
		auto foundObjId = std::find(_csa_object_identifier.begin(), _csa_object_identifier.end(), (signed long)csaObjId);
		if (foundObjId == _csa_object_identifier.end())
		{
			ShowAndLog("Exercise delete visual object not found");
		}
		else
		{
			int index = (int(std::distance(_csa_object_identifier.begin(), foundObjId)));

			//remove from published data map
			_csa_object_data.erase((signed long)csaObjId);

			// remove from CSA object IDs vector
			_csa_object_identifier.erase(foundObjId);

			// Get related VBS object and remove
			ObjectHandle_v3 obj = _vbs_object_identifier.at(index);
			if (ValidateHandle(obj))
			{
				SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(obj));
			}

			// remove from VBS objects vector
			_vbs_object_identifier.erase(_vbs_object_identifier.begin() + index);

			ShowAndLog("Exercise object deleted");
		}
}

void TrainerView::ControlObjectPosition(long csaObjId,int entityGoToID)
{
	GeoPosition_v5 goto_position = { 0,0,0 };
	//Vector3f32_v3 velocity = { 0, 0, 0 }; 
	int index = 0;

	auto foundObjId = std::find(_csa_object_identifier.begin(), _csa_object_identifier.end(), (signed long)csaObjId);
	if (foundObjId == _csa_object_identifier.end())
	{
		ShowAndLog("Entity update visual object not found");
	}
	else
	{
		// Get object number to access correct tank group
		index = (int(std::distance(_csa_object_identifier.begin(), foundObjId)));

		ConvertENUToLLA(entityGoToLocation[entityGoToID].ECEF_X, entityGoToLocation[entityGoToID].ECEF_Y, entityGoToLocation[entityGoToID].ECEF_Z);
		goto_position = (NewPosition(latLongPosition.latitude,latLongPosition.longitude)).second;

		ObjectHandle_v3 waypoint;
		SDKCheck(Gears::API::WorldAPIv5()->CreateWaypoint(&waypoint));
		SDKCheck(Gears::API::WaypointAspectAPIv2()->SetType(waypoint, kWaypointType_move));
		SDKCheck(Gears::API::WaypointAspectAPIv2()->SetAssignedTo(waypoint, _tank_group[index], -1));

		SDKCheck(Gears::API::TransformationAspectAPIv4()->SetPosition(waypoint, goto_position));

		// Received entity speed in Kilometres/hour
		// model velocity:	Object velocity in model space (right, up, forward) in meters per second
		//SDKCheck(Gears::API::TransformationAspectAPIv4()->GetModelVelocity(_vbs_object_identifier[index], &velocity));
		//float32_t speed_mps = (float32_t)(entityGoToSpeed[entityGoToID] * KMHToMPS);
		//velocity.z = speed_mps;
		//SDKCheck(Gears::API::TransformationAspectAPIv4()->SetModelVelocity(_vbs_object_identifier[index], velocity));

		ShowAndLog("Exercise object position updated");
	}
}

ObjectHandle_v3 TrainerView::FindTurret(unsigned long csaObjId)
{
	int IDindex = 0;
	// Find the turret that is assigned to the CSA entity
	auto foundObjID = std::find(_object_with_turret.begin(), _object_with_turret.end(), (signed long)csaObjId);
	if (foundObjID == _object_with_turret.end())
	{
		ShowAndLog("Update turret ID not found");
		return kNullObjectHandle;
	}
	IDindex = (int(std::distance(_object_with_turret.begin(), foundObjID)));

	return _turret_objects.at(IDindex);

}


void TrainerView::UpdateOVTurretHeading(const unsigned long ovCSAObjId, const double& relativeTurretHeading)
{
	ObjectHandle_v3 turretObject = FindTurret(ovCSAObjId);

	if (ValidateHandle(turretObject) && ValidateHandle(_ownship))
	{
		// Get orientation of turret
		RotationalAngles_v3 turret_orientation;
		SDKCheck(Gears::API::TurretAspectAPIv1()->GetOrientation(turretObject, &turret_orientation));
		RotationalAngles_v3 OV_orientation;
		SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(_ownship, &OV_orientation));

		UpdateEntityTurretHeading(turretObject, OV_orientation, turret_orientation, relativeTurretHeading);
	}
}


void TrainerView::UpdateOVWeaponElevation(const unsigned long ovCSAObjId, const double& relativeWeaponElevation)
{
	ObjectHandle_v3 turretObject = FindTurret(ovCSAObjId);

	if (ValidateHandle(turretObject) && ValidateHandle(_ownship))
	{
		RotationalAngles_v3 turret_orientation;
		SDKCheck(Gears::API::TurretAspectAPIv1()->GetOrientation(turretObject, &turret_orientation));
		RotationalAngles_v3 OV_orientation;
		SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(_ownship, &OV_orientation));

		UpdateEntityWeaponElevation(turretObject, OV_orientation, turret_orientation, relativeWeaponElevation);
	}
}


void TrainerView::UpdateEntityTurretHeading(const ObjectHandle_v3& turretObject, const RotationalAngles_v3& currentEntityOrientation, RotationalAngles_v3& currentTurretOrientation, const double& relativeTurretHeading)
{
	float64_t entityHeading = currentEntityOrientation.yaw * degsToRads;
	currentTurretOrientation.yaw = radsToDegs * (entityHeading + relativeTurretHeading);
	SDKCheck(Gears::API::TurretAspectAPIv1()->SetOrientation(turretObject, currentTurretOrientation));
	ShowAndLog("Turret Heading Updated");
}


void TrainerView::UpdateEntityWeaponElevation(const ObjectHandle_v3& turretObject, const RotationalAngles_v3& currentEntityOrientation, RotationalAngles_v3& currentTurretOrientation, const double& relativeWeaponElevation)
{
	float64_t turretelev = currentEntityOrientation.pitch * degsToRads;
	currentTurretOrientation.pitch = radsToDegs * (turretelev + relativeWeaponElevation);
	SDKCheck(Gears::API::TurretAspectAPIv1()->SetOrientation(turretObject, currentTurretOrientation));
	ShowAndLog("Weapon Elevation Updated");
}


void TrainerView::ConvertLLAToENU()
{
	Vector3f64_v3 mapPosition{ 0.0, 0.0, 0.0 };
	SDKCheck(Gears::API::WorldAPIv5()->GeoPositionToMapPosition(latLongPosition, &mapPosition));

	entityUpdatePosition.ENU_X = mapPosition.x - mapOrigin.x;
	entityUpdatePosition.ENU_Y = mapPosition.z - mapOrigin.z;
	entityUpdatePosition.ENU_Z = mapPosition.y;

	//ShowAndLog("DDS LLA To ENU");
}

void TrainerView::ConvertENUToLLA(const double& x, const double& y, const double& z)
{
	Vector3f64_v3 mapPosition{ x + mapOrigin.x, z, y + mapOrigin.z };
	SDKCheck(Gears::API::WorldAPIv5()->MapPositionToGeoPosition(mapPosition, &latLongPosition));

	//ShowAndLog("DDS ENU To LLA");
}

void TrainerView::ClearMissionData()
{
	// Clear out all Line Of Sight data
	EndposOffset = 0;
	missfuncLOSRequestID.clear();
	missfuncLOSRequestStartID.clear();
	missfuncLOSRequestStartpos.clear();
	missfuncLOSRequestEndID.clear();
	missfuncLOSRequestEndposIDs.clear();
	missfuncLOSRequestEndpos.clear();
	missfuncnumOfLOSReqIds.clear();
	missfuncCancelLOSRequestID.clear();
	ClearArrays();
}

void TrainerView::ClearArrays()
{
	//Empty mission response arrays
	memset(intersectionPosition, 0, sizeof(intersectionPosition));
	memset(intersectionObject, 0, sizeof(intersectionObject));
	memset(intersectionLocal_position, 0, sizeof(intersectionLocal_position));
	memset(intersectionDistance, 0, sizeof(intersectionDistance));
	memset(intersectionType, 0, sizeof(intersectionType));
	memset(numOfLOSReqIds, 0, sizeof(numOfLOSReqIds));
	
}

void TrainerView::ClearVectors()
{
	entityGoToObjectIdentifier.clear();
	entityGoToLocation.clear();
	entityGoToSpeed.clear();
	entityDamageInput.clear();
	entityGoToDwellTime.clear();
	_csa_object_data.clear();
	_csa_object_identifier.clear();
	_vbs_object_identifier.clear();
	_object_with_turret.clear();
	_turret_objects.clear();
	entityAddEntityTypeId.clear();
	entityAddObjectIDs.clear();
	entityAddLocationAndHeading.clear();
	exconEntityIDs.clear();
	_object_gunner.clear();
	_object_driver.clear();
	_object_commander.clear();
	_tank_group.clear();
	//exconWeaponUpdateFireBurstProjID.clear();
	exconWeaponUpdateFireBurstProjID = 0;
}

void TrainerView::ClearLinesOfShot()
{
	_num_shots_fired = _num_object_hits = _num_ricochets = 0;

	for (auto primitive : _primitives)
	{
			SDKCheck(Gears::API::WorldDrawAPIv3()->DeletePrimitive(primitive));
	}
	_primitives.clear();

}

void TrainerView::Cleanup()
{
  // clear message flags
  exconExerciseName = "";
  exconExerciseFile = "";
  exconStartExercise = false;
  exconStopExercise = false;
  exconResumeExercise = false;
  exconPauseExercise = false;
  exconStartTrainingSession = false;
  exconLeaveTrainingSession = false;
  exconCourseComplete = false;
  exconPhaseComplete = false;
  exconDisplayMessage = "";
  _mission_started = false;
  _in_mission = false;
  SetSelected(kNullObjectHandle);

  ClearVectors();
  ClearMissionData();
  
   SDKCheck(Gears::API::MissionAPIv3()->EndMission());

  auto log_api = Gears::API::LogAPIv1();
  log_api->LogInfo(log_api, "Unloaded Exercise View.");

}

bool TrainerView::OnSize(const int32_t client_width, const int32_t client_height)
{
  _client_width = client_width;
  _client_height = client_height;
  return false;
}

void TrainerView::SetSelected(ObjectHandle_v3 object)
{
  if (_selected == object)
  {
    return;
  }

  if (_selected != kNullObjectHandle)
  {
    const Vector3f32_v3 vzero = { 0.0F, 0.0F, 0.0F };

    SDKCheck(Gears::API::TransformationAspectAPIv4()->SetModelVelocity(_selected, vzero));
  }
  _selected = object;
  UpdateMarker();

  //const auto msg = object != kNullObjectHandle ? "Object selected." : "Object deselected.";

  //ShowAndLog(msg);
}

void TrainerView::UpdateMarker()
{ 
  if (_selected != kNullObjectHandle)
  {
    const auto pos = GetPosition(_selected);
    Vector3f32_v3 scale = { 1.0F,   // x
                            1.0F,   // y
                            1.0F }; // z

    SDKCheck(Gears::API::TransformationAspectAPIv4()->GetScale(_selected, &scale));

    const GeoPosition_v3 start = { pos.latitude,                     // latitude
                                   pos.longitude,                    // longitude
                                   pos.altitude + (4.0 * scale.y) }; // altitude

    if (_selectedMarker < 0)
    {
      const GeoPosition_v3 end = { pos.latitude,                     // latitude
                                   pos.longitude,                    // longitude
                                   pos.altitude + (6.0 * scale.y) }; // altitude
      const Color_v3 color = { 1.0F,   // r
                               0.0F,   // g
                               0.0F,   // b
                               0.8F }; // a

      SDKCheck(Gears::API::WorldDrawAPIv3()->CreateArrow(start, end, 2.0F, 2.0F, 2.0F, color, &_selectedMarker));
    }
    else
    {
      SDKCheck(Gears::API::WorldDrawAPIv3()->SetPosition(_selectedMarker, start));
    }
  }
  else if (_selectedMarker >= 0)
  {
    SDKCheck(Gears::API::WorldDrawAPIv3()->DeletePrimitive(_selectedMarker));

    _selectedMarker = -1;
  }
}

bool TrainerView::OnMouseButtonPressed(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
	// Allow shooting only if vehicle and turret exists
	if (!ValidateHandle(_ownship) || !ValidateHandle(_ownship_turret))
	{
		return false;
	}

	// Set safety off
	SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->EnableActiveWeaponSafety(_ownship_turret, FALSE));

	return false;
}

bool TrainerView::OnKeyPressed(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags)
{
	// Check that there is some selected object 
	if (_ownship == kNullObjectHandle)
	{
		return false;
	}

		ObjectHandle_v3 turretObject = FindTurret(exconOVEntityID);
		// Get orientation of turret
		RotationalAngles_v3 turret_orientation;
		SDKCheck(Gears::API::TurretAspectAPIv1()->GetOrientation(turretObject, &turret_orientation));

		if (key_id == 'J' || key_id == 'L') 
		{
			if (key_id == 'J')
			{
				turret_orientation.yaw += -0.1;
				ShowAndLog("Heading LEFT");
			}
			else
			{
				turret_orientation.yaw += 0.1;
				ShowAndLog("Heading RIGHT");
			}
			SDKCheck(Gears::API::TurretAspectAPIv1()->SetOrientation(turretObject, turret_orientation));
		}
		else if (key_id == 'I' || key_id == 'K') 
		{
			if (key_id == 'I')
			{
				turret_orientation.pitch += 0.1;
				ShowAndLog("Elevation UP");
			}
			else
			{
				turret_orientation.pitch += -0.1;
				ShowAndLog("Elevation DOWN");
			}
			SDKCheck(Gears::API::TurretAspectAPIv1()->SetOrientation(turretObject, turret_orientation));
		}
		else if (key_id == 'E' || key_id == 'R')
		{
			if (key_id == 'E')
			{
				exconBoreSightLineError = 0.0000028;
				ShowAndLog("Sight error LEFT");
			}
			else
			{
				exconBoreSightLineError = -0.0000028;
				ShowAndLog("Sight error RIGHT");
			}
		}
		else if (key_id == 'U' || key_id == 'Y')
		{
			if (key_id == 'U')
			{
				exconBoreSightElevationError = 0.1;
				ShowAndLog("Sight elevation error UP");
			}
			else
			{
				exconBoreSightElevationError = -0.1;
				ShowAndLog("Sight elevation error DOWN");
			}
		}
		else if (key_id == 'S')
		{
			if (SQFSwitchCameraView_mode == kOpticsViewMode)
			{
				if (sight_device_index == kSightDeviceType_unity)
				{
					exconSelectedSightID = 16;
				}
				else
				{
					exconSelectedSightID = 15;
				}
			}
			ShowAndLog("Sight change");
		}
		else if (key_id == 'W')
		{
			if (exconWeaponControlSelectWeaponID == weaponID_rarden)
			{
				exconWeaponControlSelectWeaponID = weaponID_chaingun;
			}
			else
			{
				exconWeaponControlSelectWeaponID = weaponID_rarden;
			}
			exconWeaponAmmoChange = true;
			ShowAndLog("Weapon change");
		}
		else if (key_id == 'A')
		{
			exconWeaponControlSelectWeaponID = weaponID_rarden;

			if (exconWeaponControlSelectAmmoTypeID == ammotype_APDS)
			{
				exconWeaponControlSelectAmmoTypeID = ammotype_HEI;
			}
			else
			{
				exconWeaponControlSelectAmmoTypeID = ammotype_APDS;
			}
			exconWeaponAmmoChange = true;
			ShowAndLog("Ammo change");
		}
		else if (key_id == 'B')
		{
			exconWeaponControlFireID = 1;
			ShowAndLog("BOOOOOOOM");
		}
		else if (key_id == 'P')
		{
			if (SQFSwitchCameraView_mode == kOpticsViewMode)
			{
				FireOVLaser();
				ShowAndLog("Fire Laser");
				laserFiredfromKeyboard = true;
			}
		}
		else if (key_id == 'O')
		{
			if (optics_enabled == FALSE && ValidateHandle(_ownship))
			{
				SQFSwitchCamera(_ownship, kCameraLinkType_optics);
				optics_enabled = true;
				ShowAndLog("KOptics view");

			}
			else if (ValidateHandle(_ownship))
			{
				SQFSwitchCamera(_ownship, kCameraLinkType_third_person);
				optics_enabled = false;

				ShowAndLog("Tank third person view");
			}
		}
		else if (key_id == 'U')
		{
			if (!enable_user_input)
			{
				SQFEnableUserInput();
				enable_user_input = true;
				ShowAndLog("Enable user input");
			}
			else
			{
				SQFDisableUserInput();
				enable_user_input = false;
				ShowAndLog("Disable user input");
			}
		}
		else if (key_id == 'C')
		{
		if (draw_hit_lines || draw_laser_lines)
		{
			ClearLinesOfShot();
		}
		    exconBoreSightLineError = 0.0;
			exconBoreSightElevationError = 0.0;
			ShowAndLog("Clear lines of shot");
		}
		else if (key_id == 'H')
		{
			if (draw_hit_lines)
			{
				ClearLinesOfShot();
				draw_laser_lines = false;
				draw_hit_lines = false;
			}
			else
			{
				draw_laser_lines = true;
				draw_hit_lines = true;
			}
		}

		return false;
}

void TrainerView::FireOVLaser()
{
	SDKCheck(Gears::API::InputAPIv1()->SetInput("Lase", 1.0f));
	// Get laser return _lrf_range
	QueryLaserRangeFinder(_ownship);
	QueryLaserRangePosition(_ownship);
}

void TrainerView::OnFire(ObjectHandle_v3 shooter, ObjectHandle_v3 shot)
{
	shot_objectID = shot;
	shot_fired = true;

	if (draw_hit_lines)
	{
		if (_primitives.size() > 100)
		{
			for (const auto primitive : _primitives)
			{
				SDKCheck(Gears::API::WorldDrawAPIv3()->DeletePrimitive(primitive));
			}
			_primitives.clear();
		}
	}

	++_num_shots_fired;

 	shot_Start_Positionv5 = GetPosition(shot_objectID);
	//exconWeaponUpdateFireBurstProjID.push_back((unsigned long)shot.id);
	exconWeaponUpdateFireBurstProjID = (unsigned long)shot.id;

	//Sight Line Error
	if (exconBoreSightLineError)
	{
		shot_Start_Positionv5.longitude = shot_Start_Positionv5.longitude + exconBoreSightLineError;
	}

	if (exconBoreSightElevationError)
	{
		shot_Start_Positionv5.longitude = shot_Start_Positionv5.altitude + exconBoreSightElevationError;
	}

	shot_positionv3.altitude = shot_Start_Positionv5.altitude;
	shot_positionv3.latitude = shot_Start_Positionv5.latitude;
	shot_positionv3.longitude = shot_Start_Positionv5.longitude;

	// Remember the initial position for drawing the line
	_last_positions[shot.id] = shot_positionv3;

	// Detect the shot hit
	SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->RegisterEvent(shot, kMunitionEventType_ammo_hit, "TrainerView"));

	ShowAndLog("Shot fired.");
}

std::pair<int64_t, int64_t> DrawHitLine(const GeoPosition_v3& impact, const Color_v3& color, const GeoPosition_v3& start)
{
	// Draw a sphere representing the hit
	std::pair<int64_t, int64_t> result;

	SDKCheck(Gears::API::WorldDrawAPIv3()->CreateSphere(impact, 0.1F, color, 20, 20, &result.first));

	// Draw a line representing a trajectory
	SDKCheck(Gears::API::WorldDrawAPIv3()->CreateLine(start, impact, 0.01F, 2, color, &result.second));

	return result;
}

void TrainerView::OnAmmoHit(const HitEvent_v2* hit_info)
{
	if (draw_hit_lines)
	{
		_primitives.resize(_primitives.size() + 2U);

	     const auto it = std::next(_primitives.rbegin());

		if (hit_info->shot_exploded == FALSE)
		{
			const Color_v3 color = { 0.0F,   // r
									 1.0F,   // g
									 0.0F,   // b
									 1.0F }; // a

			std::tie(*it, _primitives.back()) = DrawHitLine(hit_info->impact, color, _last_positions[hit_info->shot.id]);
			++_num_ricochets;
		}
		else
		{
			const Color_v3 color = { 1.0F,   // r
									 0.0F,   // g
									 0.0F,   // b
									 1.0F }; // a

			std::tie(*it, _primitives.back()) = DrawHitLine(hit_info->impact, color, _last_positions[hit_info->shot.id]);

			SDKCheck(Gears::API::WeaponSystemAspectAPIv5()->UnregisterEvent(hit_info->shot, kMunitionEventType_ammo_hit, "TrainerView"));
		}
	}

	_last_positions[hit_info->shot.id] = hit_info->impact;

	detonation_position.altitude = hit_info->impact.altitude;
	detonation_position.latitude = hit_info->impact.latitude;
	detonation_position.longitude = hit_info->impact.longitude;
	
	shot_detonated = true;

}

void TrainerView::OnObjectHit(const HitEvent_v2* hit_info)
{
	target_hit_ID = hit_info->target;
}

void TrainerView::OnDamageChange(const ObjectHandle_v3 object, const ObjectHandle_v3 source, const float32_t damage)
{

}

void TrainerView::OnDestruction(const ObjectHandle_v3 object, const ObjectHandle_v3 source)
{

}

void TrainerView::QueryLaserRangeFinder(ObjectHandle_v3 _obj)
{
	// Get the ScriptID of the tank (Note: this can be done when the tank is created)
	char tank_script_id[32];
	int32_t script_id_size = 32;
	Gears::API::ScriptAPIv3()->GetScriptIdFromObjectHandle(_obj, tank_script_id, &script_id_size);

	// Format the `LaserRange` SQF command (https://sqf.bisimulations.com/display/SQF/LaserRange)
	// Note: We are using the `IdToObj` SQF command here to convert the Tank's ScriptID to the SQF object representing the tank
	// Note: We are using the `gunner` SQF command here to query for the gunner of the tank, but alternatively you could just provide the gunner object
	char sqf_command[128];
	sprintf(sqf_command, "(IdToObj %s) laserRange (gunner (IdToObj %s))", tank_script_id, tank_script_id);

	char result[512];
	int32_t result_size = 512;
	int32_t error_pos = -1;
	Gears::API::ScriptAPIv3()->ExecuteSQF(sqf_command, result, &result_size, &error_pos);
	if (error_pos >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing QueryLaserRange:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), result);
	}
	else
	{
		// Convert the string result from the command into an integer that represents that current range of the LRF
		_lrf_range = atoi(result);
	}
}

void TrainerView::QueryLaserRangePosition(ObjectHandle_v3 _obj)
{
	// Get the ScriptID of the tank (Note: this can be done when the tank is created)
	char tank_script_id[32];
	int32_t script_id_size = 32;
	Gears::API::ScriptAPIv3()->GetScriptIdFromObjectHandle(_obj, tank_script_id, &script_id_size);

	// Format the `LaserRange` SQF command (https://sqf.bisimulations.com/display/SQF/LaserRange)
	// Note: We are using the `IdToObj` SQF command here to convert the Tank's ScriptID to the SQF object representing the tank
	// Note: We are using the `gunner` SQF command here to query for the gunner of the tank, but alternatively you could just provide the gunner object
	char sqf_command[128];
	sprintf(sqf_command, "(IdToObj %s) laserRangePosition [0]", tank_script_id);

	char result[512];
	int32_t result_size = 512;
	int32_t error_pos = -1;
	Gears::API::ScriptAPIv3()->ExecuteSQF(sqf_command, result, &result_size, &error_pos);
	if (error_pos >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing QueryLaserRangePosition:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), result);
	}
	else
	{

		int posindex = 0;
		bool found_value = false;

		for (int j = 0; j <= sizeof(result); j++)
		{
			if (strncmp(&result[j], "," ,1) && !found_value)
			{
				laser_return_position[posindex] = atof(&result[j]);
				if (laser_return_position[posindex] > 0)
				{
					found_value = true;
				}
			}

			if (!strncmp(&result[j], ",", 1))
			{
				posindex += 1;
				found_value = false;
			}
		}

		map_position.x = laser_return_position[0];
		map_position.y = laser_return_position[2];
		map_position.z = laser_return_position[1];

		SDKCheck(Gears::API::WorldAPIv5()->MapPositionToGeoPosition(map_position, &laser_hit_position));

		if (draw_laser_lines)
		{
			OnLaserHit();
		}
	}
}

void TrainerView::OnLaserHit()
{
	std::pair<int64_t, int64_t> result;

	// Use ownship position for now
	GeoPosition_v5 ownpos = GetPosition(_ownship);
	GeoPosition_v3 startpos = { 0,0,0 };
	GeoPosition_v3 endpos = { 0,0,0 };

	startpos.altitude = ownpos.altitude + 2.5;
	startpos.latitude = ownpos.latitude;
	startpos.longitude = ownpos.longitude;

	endpos.altitude = laser_hit_position.altitude;
	endpos.latitude = laser_hit_position.latitude;
	endpos.longitude = laser_hit_position.longitude;

	if (draw_laser_lines)
	{
		if (_primitives.size() > 100)
		{
			for (const auto primitive : _primitives)
			{
				SDKCheck(Gears::API::WorldDrawAPIv3()->DeletePrimitive(primitive));
			}
			_primitives.clear();
		}

		_primitives.resize(_primitives.size() + 2U);

		const auto it = std::next(_primitives.rbegin());

		const Color_v3 color = { 0.0F,   // r
								0.0F,   // g
								1.0F,   // b
								1.0F }; // a

	    // Draw a line representing the laser
		std::tie(*it, _primitives.back()) = DrawHitLine(endpos, color, startpos);
	}
}

void TrainerView::SQFSwitchCamera(const ObjectHandle_v3& object, CameraLinkType_v1 mode)
{
	int32_t script_id_size = 0;
	SDKCheck(Gears::API::ScriptAPIv3()->GetScriptIdFromObjectHandle(object, nullptr, &script_id_size));
	std::string script_id(script_id_size, '\n');
	SDKCheck(Gears::API::ScriptAPIv3()->GetScriptIdFromObjectHandle(object, &script_id[0], &script_id_size));

	switch (mode)
	{
	case CameraLinkType_v1::kCameraLinkType_first_person:
	{
		SQFSwitchCameraView_mode = kInternalViewMode;
	}
	break;
	case CameraLinkType_v1::kCameraLinkType_third_person:
	{
		SQFSwitchCameraView_mode = kExternalViewMode;
	}
	break;
	case CameraLinkType_v1::kCameraLinkType_optics:
	{
		SQFSwitchCameraView_mode = kOpticsViewMode;
	}
	break;
	default:
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Invalid mode for `SQFSwitchCamera`!");
		return;
	}

	char sqf_command[256];
	sprintf(sqf_command, "IdToObj %s switchCamera %s", script_id.c_str(), SQFSwitchCameraView_mode);

	char command_result[512];
	int32_t command_result_length = 512;
	int32_t error_position = -1;
	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(sqf_command, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing SQFSwitchCamera:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}
}

void TrainerView::SQFDisableActionMenu()
{
	const char* bind_nextAction = "nextActionKeyBind = [\"NextAction\",false,0] bindKeyEx \"true\"";
	const char* bind_prevAction = "prevActionKeyBind = [\"PrevAction\",false,0] bindKeyEx \"true\"";
	const char* bind_defAction = "DeftActionKeyBind = [\"Action\",false,0] bindKeyEx \"true\";";

	char command_result[512];
	int32_t command_result_length = 512;
	int32_t error_position = -1;
	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(bind_nextAction, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing SQFDisableActionMenu:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}

	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(bind_prevAction, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing SQFDisableActionMenu:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}

	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(bind_defAction, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing SQFDisableActionMenu:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}
}

void TrainerView::SQFEnabledActionMenu()
{
	const char* unbind = "{unBindKey _x} forEach [nextActionKeyBind,prevActionKeyBind,DeftActionKeyBind  ];";

	char command_result[512];
	int32_t command_result_length = 512;
	int32_t error_position = -1;
	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(unbind, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing SQFEnabledActionMenu:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}
}

void TrainerView::SQFDisableUserInput()
{
	const char* disableUserInput = "disableUserInput true;";

	char command_result[512];
	int32_t command_result_length = 512;
	int32_t error_position = -1;
	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(disableUserInput, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing SQFDisableUserInput:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}
}

void TrainerView::SQFEnableUserInput()
{
	const char* disableUserInput = "disableUserInput false;";

	char command_result[512];
	int32_t command_result_length = 512;
	int32_t error_position = -1;
	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(disableUserInput, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing SQFDisableUserInput:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}
}

