#include "DDSEngagementEvent.h"

void DDSEngagementEvent::AimingMarkAlignedWithEntity(const unsigned long& entityId) 
{
	//std::cout<< std::endl << " *** DDSEngagementEvent::AimingMarkAlignedWithEntity ***" << std::endl;
	//std::cout<< "     Entity ID " << entityId << std::endl;
}

void  DDSEngagementEvent::CentralHitAchievedOnEntity(const unsigned long& entityId)
{
	//std::cout<< std::endl << " *** DDSEngagementEvent::CentralHitAchievedOnEntity ***  " << std::endl;
	//std::cout<< "     Entity ID " << entityId << std::endl;
}

void DDSEngagementEvent::BurstFiredAtEntity(const unsigned long& entityId)
{
	//std::cout<< std::endl << " *** DDSEngagementEvent::BurstFiredAtEntity ***" << std::endl;
	//std::cout<< "     Entity ID " << entityId << std::endl;
}

void DDSEngagementEvent::BurstsFiredAtEntity(const unsigned long& entityId, const unsigned short& burstCount)
{
	//std::cout<< std::endl << " *** DDSEngagementEvent::BurstsFiredAtEntity ***" << std::endl;
	//std::cout<< "     Entity ID " << entityId << std::endl;
	//std::cout<< "     Burst Count " << burstCount << std::endl;
}

void DDSEngagementEvent::LaserFiredAtEntity(const unsigned long& entityId)
{
	//std::cout<< std::endl << " *** DDSEngagementEvent::LasesFiredAtEntity ***  " << std::endl;
	//std::cout<< "     Entity ID " << entityId << std::endl;
}

void DDSEngagementEvent::LasesFiredAtEntity(const unsigned long& entityId, const unsigned short& laseCount)
{
	//std::cout<< std::endl << " *** DDSEngagementEvent::LasesFiredAtEntity ***  " << std::endl;
	//std::cout<< "     Entity ID " << entityId << std::endl;
	//std::cout<< "     Lase Count " << laseCount << std::endl;
}

