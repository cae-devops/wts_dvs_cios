#include "DDSTurretControl.h"
#include "TrainerMessageData.h"
#include "ScopeLock.h"

void DDSTurretControl::FireLaser(const unsigned long& entityId)
{
	ScopeLock lock(MessageLock);
	//// << " *** DDSTurretControl::FireLaser ***" << std::endl;
	//// << "     Entity ID: " << entityId << std::endl;
	laserFireRequests.insert(entityId);
	laserFireRequestsPending = true;
}

void DDSTurretControl::UpdateTurretHeading(const unsigned long& entityId, const double& heading)
{
	ScopeLock lock(MessageLock);
	//// << " *** DDSTurretControl::TurretHeading ***" << std::endl;
	//// << "     Entity ID: " << entityId << std::endl;
	//// << "     Heading: " << heading << std::endl;
	turretHeadingUpdateRequests.insert(std::map<unsigned long, double>::value_type(entityId, heading));
	turretHeadingUpdateRequestsPending = true;
}

void DDSTurretControl::UpdateWeaponElevation(const unsigned long& entityId, const unsigned long& weaponId, const double& elevation)
{
	ScopeLock lock(MessageLock);
	// << " *** DDSTurretControl::UpdateWeaponElevation ***  " << std::endl;
	// << "     Entity ID: " << entityId << std::endl;
	// << "     Weapon ID: " << weaponId << std::endl;
	// << "     Elevation: " << elevation << std::endl;
	weaponElevationUpdateRequests.insert(std::map<unsigned long, WeaponElevation>::value_type(entityId, { weaponId, elevation }));
	weaponElevationUpdateRequestsPending = true;
}

void DDSTurretControl::RestockAmmunition(const unsigned long& ammoTypeId)
{
	ScopeLock lock(MessageLock);
	// << " *** DDSTurretControl::RestockAmmunition ***  " << std::endl;
	// << "     Ammo Type ID: " << ammoTypeId << std::endl;
	exconTurretRestockAmmoID = ammoTypeId;
}

void DDSTurretControl::SetFixedGunError(const double& azimuthError, const double& targetRange, const double& requiredRange)
{
	// << " *** DDSTurretControl::SetFixedGunError ***  " << std::endl;
	// << "     Azimuth Error: " << azimuthError << std::endl;
	// << "     Target Range: " << targetRange << std::endl;
	// << "     Required Range: " << requiredRange << std::endl;
}