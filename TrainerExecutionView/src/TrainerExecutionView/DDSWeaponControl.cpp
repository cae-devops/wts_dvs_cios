#include "DDSWeaponControl.h"
#include "TrainerMessageData.h"

void DDSWeaponControl::CreateWeapon(const unsigned short& requestId, const unsigned short& weaponTypeId)
{
	// << " *** DDSWeaponControl::CreateWeapon ***" <<
	// << "     Request ID " << requestId << 
	// << "     Weapon Type ID " << weaponTypeId << 
	exconWeaponControlWeaponID = weaponTypeId;
}

void DDSWeaponControl::SelectAmmunition(const unsigned short& weaponId, const unsigned short& ammoTypeId)
{
	// << " *** DDSWeaponControl::SelectAmmunition ***" <<
	// << "     Weapon ID " << weaponId <<
	// << "     Ammo Type ID " << ammoTypeId <<
	exconWeaponControlSelectWeaponID = weaponId;
	exconWeaponControlSelectAmmoTypeID = ammoTypeId;
	exconWeaponAmmoChange = true;
}

void DDSWeaponControl::FireWeapon(const unsigned short& weaponId, const S_POSITION_AND_ORIENTATION& barrelBase, const double& chassisHeading)
{
	// << " *** DDSWeaponControl::FireWeapon ***  " << 
	// << "     Weapon ID " << weaponId <<
	// << "     Chassis Heading" << chassisHeading << 
	// << "     Pitch " << barrelBase.barrelBasePitch << 
	// << "     Roll " << barrelBase.barrelBaseRoll <<
	// << "     Heading " << barrelBase.barrelBaseHeading << 
	// << "     Position X " << barrelBase.barrelBaseX <<
	// << "     Position Y " << barrelBase.barrelBaseY <<
	// << "     Position Z " << barrelBase.barrelBaseZ <<
	exconWeaponControlFireID = weaponId;
	exconWeaponControlFireBarrelbase = barrelBase;
	exconWeaponControlFireChassisHeading = chassisHeading;
}
void DDSWeaponControl::SetBoreSightErrors(const unsigned short& weaponId, const double& elevationErrorMagnitude, const double& lineErrorMagnitude)
{
	//<< std::endl << " *** DDSWeaponControl::SetBoreSightErrors ***  " << 
	//<< "     Weapon ID " << weaponId <<
	//<< "     ElevationErrorMagnitude " << elevationErrorMagnitude << 
	//<< "     LineErrorMagnitude " << lineErrorMagnitude <<
	exconBoreSightLineError = elevationErrorMagnitude;
	exconBoreSightElevationError = lineErrorMagnitude;
	exconBoreSightLineError = 0.0000028;
	exconBoreSightElevationError = 0.1;
	if (lineErrorMagnitude < 0)
	{
	exconBoreSightLineError = exconBoreSightLineError * -1;
	}
	if (elevationErrorMagnitude < 0)
	{
		exconBoreSightElevationError = exconBoreSightElevationError * -1;
	}
}
