#include "DDSProjectileUpdate.h"

void DDSProjectileUpdate::FallOfShotEvent(const unsigned long& entityId, const unsigned long& ammoTypeId, 
	const unsigned long& projectileId, const S_ENU_POSITION& detonationPosition)
{
	//std::cout << std::endl << " *** DDSProjectileUpdate::FallOfShotEvent ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	//std::cout << "     Ammo Type ID " << ammoTypeId << std::endl;
	//std::cout << "     Projectile ID " << projectileId << std::endl;
	//std::cout << "     Detonation Position X " << detonationPosition.ENU_X << 
	//	                                  "Y " << detonationPosition.ENU_Y << 
	//	                                  "Z " << detonationPosition.ENU_Z << std::endl;
	
}

void DDSProjectileUpdate::ProjectileTrajectory(const unsigned long& entityId, const unsigned long& ammoTypeId, const unsigned long& projectileId,
	const S_ENU_POSITION& startPosition, const S_ENU_POSITION& endPosition)
{
	//std::cout << std::endl << " *** DDSProjectileUpdate::ProjectileTrajectory ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl; 
	//std::cout << "     Ammo Type ID " << ammoTypeId << std::endl;
	//std::cout << "     Projectile ID " << projectileId << std::endl;
	//std::cout << "     Start Position X " << startPosition.ENU_X <<
	//	                             "Y " << startPosition.ENU_Y <<
	//	                             "Z " << startPosition.ENU_Z << std::endl;
	//std::cout << "     End Position X " << endPosition.ENU_X <<
	//	                             "Y " << endPosition.ENU_Y <<
	//	                             "Z " << endPosition.ENU_Z << std::endl;

}

