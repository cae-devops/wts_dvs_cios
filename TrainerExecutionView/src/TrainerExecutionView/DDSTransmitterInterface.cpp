#define _DDS_DOMAIN_PART_INIT_BUILD_

#include "DDSTransmitterInterfaceExport.h"
#include "DDSTransmitterInterface.h"

// Transmitters
#include "ActiveEntityStatusUpdateTransmitter.h"
#include "EntityUpdateTransmitter.h"
#include "TurretUpdateTransmitter.h"
#include "WeaponUpdateTransmitter.h"
#include "ProjectileUpdateTransmitter.h"
#include "MissionFunctionResponseTransmitter.h"
#include "EngagementEventTransmitter.h"
#include "SightUpdateTransmitter.h"
#include "TurretControlTransmitter.h"

#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>


DDSTransmitterInterface::DDSTransmitterInterface(void) : domainId(1)
{ 
	/* create the participant - Transmitters */
	/* To customize participant QoS, use the configuration file USER_QOS_PROFILES.xml */

	transmitterParticipant = DDSTheParticipantFactory->create_participant(domainId, DDS_PARTICIPANT_QOS_DEFAULT,NULL, DDS_STATUS_MASK_NONE);

	activeEntityStatusUpdateTransmitter	  = new ActiveEntityStatusUpdateTransmitter(transmitterParticipant);
	entityUpdateTransmitter               = new EntityUpdateTransmitter(transmitterParticipant);
	turretUpdateTransmitter               = new TurretUpdateTransmitter(transmitterParticipant);
	weaponUpdateTransmitter               = new WeaponUpdateTransmitter(transmitterParticipant);
	projectileUpdateTransmitter           = new	ProjectileUpdateTransmitter(transmitterParticipant);
	missionFunctionResponseTransmitter    = new	MissionFunctionResponseTransmitter(transmitterParticipant);
	engagementEventTransmitter            = new	EngagementEventTransmitter(transmitterParticipant);
	sightUpdateTransmitter                = new	SightUpdateTransmitter(transmitterParticipant);
	turretControlTransmitter			  = new TurretControlTransmitter(transmitterParticipant);
}

ActiveEntityStatusUpdateTransmitter*   DDSTransmitterInterface::GetActiveEntityStatusUpdateTransmitter()
{
	return activeEntityStatusUpdateTransmitter;
};
EntityUpdateTransmitter*               DDSTransmitterInterface::GetEntityUpdateTransmitter()
{
	return entityUpdateTransmitter;
};

TurretUpdateTransmitter*               DDSTransmitterInterface::GetTurretUpdateTransmitter()
{
	return turretUpdateTransmitter;
};

WeaponUpdateTransmitter*               DDSTransmitterInterface::GetWeaponUpdateTransmitter()
{
	return weaponUpdateTransmitter;
};

ProjectileUpdateTransmitter*           DDSTransmitterInterface::GetProjectileUpdateTransmitter()
{
	return projectileUpdateTransmitter;
};

MissionFunctionResponseTransmitter*    DDSTransmitterInterface::GetMissionFunctionResponseTransmitter()
{
	return missionFunctionResponseTransmitter;
};

EngagementEventTransmitter*            DDSTransmitterInterface::GetEngagementEventTransmitter()
{
	return engagementEventTransmitter;
};

SightUpdateTransmitter*                DDSTransmitterInterface::GetSightUpdateTransmitter()
{
	return sightUpdateTransmitter;
};

TurretControlTransmitter*                DDSTransmitterInterface::GetTurretControlTransmitter()
{
	return turretControlTransmitter;
};




/* Shutdown and Delete Transmitter Participant and all its entities */
int DDSTransmitterInterface::TransmitterParticipantShutdown(void)
{
	DDS_ReturnCode_t retcode;
	int status = 0;

	std::cout << std::endl << "**** TransmitterParticipantShutdown ****" << std::endl;
	if (transmitterParticipant != NULL)
	{
		retcode = transmitterParticipant->delete_contained_entities();
		if (retcode != DDS_RETCODE_OK) 
		{
			std::cout << "TransmitterParticipantShutdown: delete_contained_entities() error" << retcode << std::endl;
			status = -1;
		}

		retcode = DDSTheParticipantFactory->delete_participant(transmitterParticipant);
		
		if (retcode != DDS_RETCODE_OK) 
		{
			std::cout << "TransmitterParticipantShutdown: delete_participant() error " << retcode << std::endl;
			status = -1;
		}
	}

	/* RTI Connext provides finalize_instance() method on
	domain participant factory for people who want to release memory used
	by the participant factory. Uncomment the following block of code for
	clean destruction of the singleton. */

	retcode = DDSDomainParticipantFactory::finalize_instance();
	
	if (retcode != DDS_RETCODE_OK) 
	{
		std::cout << "TransmitterParticipantShutdown: finalize_instance() error " << retcode << std::endl;
		status = -1;
	}
	return status;
}