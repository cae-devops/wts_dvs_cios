#define _DDS_DOMAIN_PART_INIT_BUILD_

#include "DDSReceiverInterfaceExport.h"
#include "DDSReceiverInterface.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"

#include "ActiveEntityStatusUpdateReceiver.h"
#include "EntityControlReceiver.h"
#include "ExerciseControlReceiver.h"
#include "TurretControlReceiver.h"
#include "WeaponControlReceiver.h"
#include "WeaponControlResponseReceiver.h"
#include "WeaponUpdateReceiver.h"
#include "AssetControlReceiver.h"
#include "MissionFunctionRequestReceiver.h"
#include "RemoteExerciseControlReceiver.h"
#include "EngagementEventReceiver.h"
#include "SightUpdateReceiver.h"

DDSReceiverInterface::DDSReceiverInterface() : domainId(1)
{ 
	/* create the participant - Receivers */
	/* To customize participant QoS, use the configuration file USER_QOS_PROFILES.xml */

	receiverParticipant = DDSTheParticipantFactory->create_participant(domainId, DDS_PARTICIPANT_QOS_DEFAULT, NULL, DDS_STATUS_MASK_NONE);

	activeEntityStatusUpdateReceiver   = new ActiveEntityStatusUpdateReceiver(receiverParticipant);
	entityControlReceiver              = new EntityControlReceiver(receiverParticipant);
	exerciseControlReceiver            = new ExerciseControlReceiver(receiverParticipant);
	turretControlReceiver              = new TurretControlReceiver(receiverParticipant);
	weaponControlReceiver              = new WeaponControlReceiver(receiverParticipant);
	weaponControlResponseReceiver      = new WeaponControlResponseReceiver(receiverParticipant);
	weaponUpdateReceiver               = new WeaponUpdateReceiver(receiverParticipant);
	assetControlReceiver               = new AssetControlReceiver(receiverParticipant);
	missionFunctionRequestReceiver     = new MissionFunctionRequestReceiver(receiverParticipant);
	remoteExerciseControlReceiver      = new RemoteExerciseControlReceiver(receiverParticipant);
	engagementEventReceiver            = new EngagementEventReceiver(receiverParticipant);
	sightUpdateReceiver                = new SightUpdateReceiver(receiverParticipant);
};
	


ActiveEntityStatusUpdateReceiver* DDSReceiverInterface::GetActiveEntityStatusUpdateReceiver()
{
	return activeEntityStatusUpdateReceiver;
}; 
EntityControlReceiver* DDSReceiverInterface::GetEntityControlReceiver()
{
	return entityControlReceiver;
};

ExerciseControlReceiver* DDSReceiverInterface::GetExerciseControlReceiver()
{
	return exerciseControlReceiver;
};

TurretControlReceiver* DDSReceiverInterface::GetTurretControlReceiver()
{
	return turretControlReceiver;
};

WeaponControlReceiver* DDSReceiverInterface::GetWeaponControlReceiver()
{
	return weaponControlReceiver;
};
AssetControlReceiver* DDSReceiverInterface::GetAssetControlReceiver() 
{
	return assetControlReceiver;
};
MissionFunctionRequestReceiver* DDSReceiverInterface::GetMissionFunctionRequestReceiver() 
{
	return missionFunctionRequestReceiver;
};
RemoteExerciseControlReceiver* DDSReceiverInterface::GetRemoteExerciseControlReceiver() 
{
	return remoteExerciseControlReceiver;
};
EngagementEventReceiver* DDSReceiverInterface::GetEngagementEventReceiver() 
{
	return engagementEventReceiver;
};
SightUpdateReceiver* DDSReceiverInterface::GetSightUpdateReceiver() 
{
	return sightUpdateReceiver;
};

//* Shutdown and Delete Receiver Participant and all its entities */
int DDSReceiverInterface::ReceiverParticipantShutdown(void)
{
	DDS_ReturnCode_t retcode;
	int status = 0;

	std::cout << std::endl << "**** ReceivererParticipantShutdown ****" << std::endl;
	if (receiverParticipant != NULL)
	{
		retcode = receiverParticipant->delete_contained_entities();
		if (retcode != DDS_RETCODE_OK) 
		{
			std::cout << "ReceivererParticipantShutdown: delete_contained_entities() error" << retcode << std::endl;
			status = -1;
		}

		retcode = DDSTheParticipantFactory->delete_participant(receiverParticipant);
		
		if (retcode != DDS_RETCODE_OK) 
		{
			std::cout << "ReceivererParticipantShutdown: delete_participant() error " << retcode << std::endl;
			status = -1;
		}
	}

	/* RTI Connext provides finalize_instance() method on
	domain participant factory for people who want to release memory used
	by the participant factory. Uncomment the following block of code for
	clean destruction of the singleton. */

	retcode = DDSDomainParticipantFactory::finalize_instance();

	if (retcode != DDS_RETCODE_OK) 
	{
		std::cout << "ReceivererParticipantShutdown: finalize_instance() error" << retcode << std::endl;
		status = -1;
	}

	return status;
}