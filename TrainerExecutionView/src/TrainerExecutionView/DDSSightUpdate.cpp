#include "TrainerMessageData.h"
#include "DDSSightUpdate.h"

void DDSSightUpdate::SelectedSight(
	const unsigned long& entityId, const unsigned long& sightId, const unsigned long& crewRoleId,
	S_SIGHT_FOV fieldOfView, const unsigned long& gratId,
	S_ANGULAR_OFFSET_FROM_FOV_CENTRE lamOffset, S_ANGULAR_OFFSET_FROM_FOV_CENTRE bamOffset,
	const double& bamWidth, const bool& isActive)
{
	//std::cout << std::endl << " *** DDSSightUpdate::SelectedSight ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	//std::cout << "     Sight ID " << sightId << std::endl;
	//std::cout << "     Crew Role ID " << crewRoleId << std::endl;
	//std::cout << "     Horizontal FOV " << fieldOfView.horizontal << std::endl;
	//std::cout << "     Vertical FOV " << fieldOfView.vertical << std::endl;
	//std::cout << "     Graticule ID " << gratId << std::endl;
	//std::cout << "     LAM Offset Angle (Az) " << lamOffset.azimuth << std::endl;
	//std::cout << "     LAM Offset Angle (Elev) " << lamOffset.elevation << std::endl;
	//std::cout << "     BAM Offset Angle (Az) " << bamOffset.azimuth << std::endl;
	//std::cout << "     BAM Offset Angle (Elev) " << bamOffset.elevation << std::endl;
	//std::cout << "     BAM Width " << bamWidth << std::endl;
	//std::cout << "     Active " << isActive << std::endl;

	exconSelectedSightEntityID = entityId;
	exconSelectedSightID = sightId;

}

void DDSSightUpdate::SightPositionAndOrientation(
	const unsigned long& entityId,
	const unsigned long& sightId,
	S_SIGHT_POSITION position,
	S_SIGHT_ORIENTATION orientation)
{
	//std::cout << std::endl << " *** DDSSightUpdate::SelectedSight ***" << std::endl;
	//std::cout << "     Entity ID " << entityId << std::endl;
	//std::cout << "     Sight ID " << sightId << std::endl;
	//std::cout << "     Sight Position X " << position.ECEF_X << "Y " << position.ECEF_Y << "Z " << position.ECEF_Y << std::endl;
	//std::cout << "     Sight Orientation Psi " << orientation.psi << "Phi " << orientation.phi << "Theta " << orientation.theta << std::endl;
}
