#include "DDSExerciseControl.h"
#include "TrainerMessageData.h"

void DDSExerciseControl::StartExerciseGeneration(void)
{
	// *************** Message dealt with in StealthView component ************************
	//exgenStart = true;
}

void DDSExerciseControl::LeaveExerciseGeneration(void)
{
	// *************** Message dealt with in StealthView component ************************
	//exgenLeave = true;
}

void DDSExerciseControl::LoadExercise(const std::string& exerciseName, const std::string& exerciseFile)
{
	//" *** DDSExerciseControl::LoadExercise ***  " 
	//"     Exercise Name : " << exerciseName 
	//"     Exercise File : " << exerciseFile.substr(0, 10)

	exconLoadExercise = true;
}

void DDSExerciseControl::StartExercise(void)
{
	exconStartExercise = true;
}

void DDSExerciseControl::PauseExercise(void)
{
	exconPauseExercise = true;
}

void DDSExerciseControl::ResumeExercise(void)
{
	exconResumeExercise = true;
}

void DDSExerciseControl::StopExercise(void)
{
	exconStopExercise = true;
}

void DDSExerciseControl::LoadVoiceCommand(const std::string& voiceCommandName, const std::string& voiceCommandFile)
{
	//std::cout << " *** DDSExerciseControl::LoadVoiceCommand ***" << std::endl;
	//std::cout << "     Voice Command Name : " << voiceCommandName << std::endl;
	//std::cout << "     Voice Command File : " << voiceCommandFile.substr(0, 10) << std::endl;
}

void DDSExerciseControl::StartTrainingSession(void)
{
	exconStartTrainingSession = true;
}

void DDSExerciseControl::LeaveTrainingSession(void)
{
	exconLeaveTrainingSession = true;
}

void DDSExerciseControl::CourseComplete(void)
{
	exconCourseComplete = true;
}

void DDSExerciseControl::PhaseComplete(void)
{
	exconPhaseComplete = true;
}

void DDSExerciseControl::DisplayMessage(const std::string& message)
{
	//exconDisplayMessage = message;
	int i = 1;
	i = +1;
}

void DDSExerciseControl::CGFOwnership(const unsigned long& ovEntityId, const unsigned long& numOfEntityIDs, const unsigned long *entityId)
{
	//std::cout << " *** DDSExerciseControl::CGFOwnership ***" << std::endl;
	//std::cout << "     OV Entity ID: " << ovEntityId << std::endl;
	
	exconOVEntityID = ovEntityId;
	for (unsigned long i = 0; i < numOfEntityIDs; i++)
	{
		exconEntityIDs.push_back(entityId[i]);
	}
}