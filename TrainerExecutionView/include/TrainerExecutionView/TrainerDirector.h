/*
*
* Copyright(c) 2018 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/
#pragma once

#include <IMGuiAPI.h>
#include "TrainerView.h"

#ifdef _DEBUG
#define SDKCheck(function) assert(APIRESULT_SUCCESS(function) == TRUE)
#else
#define SDKCheck(function) function
#endif

class TrainerDirector
{
public:
	static TrainerDirector& Get()
	{
		static TrainerDirector trainer_director;
		return trainer_director;
	}

private:
	TrainerDirector() : _api_manager(nullptr), _trainerview_started(false)
	{}

public:
	APIResult Initialize(APIManager_v6* api_manager, NativeModuleHandle proxy_handle);

	void OnBeforeSimulation(float delta);
	void OnAfterSimulation(float delta);
	void OnMissionStart();
	void OnMissionEnd();
	void OnMissionLoad(const char* mission_name);
	void OnRenderMainWindow();

	void OnFire(ObjectHandle_v3 shooter, ObjectHandle_v3 shot);
	void OnAmmoHit(const HitEvent_v2* hit_info);
	void OnObjectHit(const HitEvent_v2* hit_info);
	void OnDamageChange(const ObjectHandle_v3 object, const ObjectHandle_v3 source, const float32_t damage);
	void OnDestruction(const ObjectHandle_v3 object, const ObjectHandle_v3 source);

	bool32_t OnSize(const int32_t client_width, const int32_t client_height);
	bool32_t OnMouseButtonPressed(ButtonID_v1 button_id, int32_t client_x, int32_t client_y);
	bool32_t OnMouseButtonReleased(ButtonID_v1 button_id, int32_t client_x, int32_t client_y);
	bool32_t OnMouseWheel(WheelID_v1 wheel_id, int32_t wheel_delta);
	bool32_t OnKeyPressed(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags);
	bool32_t OnKeyReleased(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags);
	bool32_t OnMouseMove(int32_t client_x, int32_t client_y);
	bool32_t OnMouseDoubleClick(ButtonID_v1 button_id, int32_t client_x, int32_t client_y);
	
	void OnObjectCollision(ObjectHandle_v3 object, ObjectHandle_v3 trigger_object, CollisionEventType_v1 type, CollisionParameters_v1 params, CollisionParameters_v1 trigger_params);
	void OnObjectCreation(ObjectHandle_v3 object);
	void OnObjectDeletion(ObjectHandle_v3 object);

	OSWindowAPI_v3* _main_window;
	TrainerView* _trainer_view;

private:
	void SetStyles();

	APIManager_v6* _api_manager;
	NativeModuleHandle _proxy_handle;

	bool _trainerview_started;
};
