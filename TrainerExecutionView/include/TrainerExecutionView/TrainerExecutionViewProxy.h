/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////
//WARNING
//This file has been autogenerated.
//Do NOT make modifications directly to it as they will be overwritten!
/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////

#ifndef TRAINEREXECUTIONVIEW_PROXY_H
#define TRAINEREXECUTIONVIEW_PROXY_H

#include <ComponentAPI.h>
#include <ApplicationListenerAPI.h>
#include <CollisionAspectListenerAPI.h>
#include <DamageAspectListenerAPI.h>
#include <DebugUIListenerAPI.h>
#include <EnvironmentListenerAPI.h>
#include <MissionListenerAPI.h>
#include <OSServiceListenerAPI.h>
#include <OSWindowListenerAPI.h>
#include <WaypointAspectListenerAPI.h>
#include <WeaponSystemAspectListenerAPI.h>

GEARS_EXPORT void GEARS_API RegisterAPI_v6(APIManager_RegisterAPI_Func_v6 register_api);

#endif
