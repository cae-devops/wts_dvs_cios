#pragma once

#include <map>
//#include "Camera.h"
#include "EntityControl.h"
#include "EntityUpdate.h"
#include "MissionFunctionRequest.h"
#include "Gears.h"
#include "VBSample.h"

#include <CommonTypesAPI.h>

#ifdef _DEBUG
#define SDKCheck(function) assert(APIRESULT_SUCCESS(function) == TRUE)
#else
#define SDKCheck(function) function
#endif

class TrainerView : public VBSample
{
public:
  struct ObjectData
  {
	  GeoPosition_v5 position = { 0.0 , 0.0, 0.0 };
	  RotationalAngles_v3 orientation = { 0.0, 0.0, 0.0 };
	  Vector3f32_v3 velocity = { 0.0, 0.0, 0.0 };
  };

  TrainerView(OSWindowAPI_v3* main_window)
    : VBSample("Trainer")
    , _mission_started(false)
    , _in_mission(false)
    , _vehicle(kNullObjectHandle)
	, _vehicle_turret(kNullObjectHandle)
	, _camera(kNullObjectHandle)
	, _second_shot(kNullObjectHandle)
	, _driver(kNullObjectHandle)
	, _gunner(kNullObjectHandle)
	, _commander(kNullObjectHandle)
	, _selected(kNullObjectHandle)
    , _selectedMarker(-1)
    , _client_width(0)
    , _client_height(0)
    , _main_window(main_window)
  {}

  virtual bool Init() override;
  virtual void OnBeforeSimulation(float delta) override;
  virtual void OnMissionStart() override;
  virtual void OnRenderMainWindow() override;
  virtual void Cleanup() override;
  virtual void OnMissionEnd() override { Cleanup(); }

  bool OnSize(const int32_t client_width, const int32_t client_height) override;
  virtual bool OnMouseButtonPressed(ButtonID_v1 button_id, int32_t client_x, int32_t client_y) override;
  virtual bool OnKeyPressed(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags);
  virtual void OnFire(ObjectHandle_v3 shooter, ObjectHandle_v3 shot) override;
  virtual void OnAmmoHit(const HitEvent_v2* hit_info) override; 
  virtual void OnObjectHit(const HitEvent_v2* hit_info);
  virtual void OnDamageChange(const ObjectHandle_v3 object, const ObjectHandle_v3 source, const float32_t damage);
  virtual void OnDestruction(const ObjectHandle_v3 object, const ObjectHandle_v3 source);
  void SetSelected(ObjectHandle_v3 object);
  void UpdateMarker();
  virtual void CreateTankCrewAndTurret(ObjectHandle_v3 vehicle, GeoPosition_v5 position, unsigned long csaObjID);

  void InitialiseDDSData();
  void ClearArrays();
  void ClearVectors();
  void ClearLinesOfShot();
  bool CreateObject(float64_t ipLat, float64_t ipLon, unsigned long csaObjID, long objectType, double heading);
  std::pair<bool, GeoPosition_v5> NewPosition(const float64_t& ipLat, const float64_t& ipLon);
  void AddNewObject(ObjectHandle_v3 obj, long csaObjectId);
  void DeleteObject(long csaObjId);
  void DeleteAllObjects();
  void ControlObjectPosition(long csaObjId,int entityGoToID);
  ObjectHandle_v3 AttachToGunnerOptics(const ObjectHandle_v3& vehicle, const char* optics_bone_name, const char* optics_memory_point_name);
  long ObjectAlreadyExists(long csaObjId);
  void CreateTankGroup(int32_t no_of_tanks);
  void ControlEntities(float delta);
  void ControlTurrets(void);
  void ControlWeapons(float delta);
  void ControlSights(float delta);
  void ControlExercise(float delta);
  void ControlDamage();
  ObjectHandle_v3 FindTurret(unsigned long entityID);
  int FindVBSIDfromcsaID(unsigned long csaObjId);
  int FindcsaIDfromVBSID(ObjectHandle_v3 vbsObjID);
  void OnLaserHit();

  // Mission function
  void MissionFunctionLOS(float delta); 
  void MissionFunctionENUConvertLLA(float delta, unsigned long numOfLOSReqIds);
  void MissionFunctionIntersectionChecks(float delta);
  void ClearMissionData();

  // CIOS calls
  void EntityUpdatesForCIOS();
  void TurretUpdatesForCIOS(unsigned long csaObjID);
  void WeaponUpdatesForCIOS(unsigned long csaObjID);
  void ProjectileUpdatesForCIOS(unsigned long csaObjID);
  void MissionResponseForCIOS(float delta);
  void SightUpdatesForCIOS(long selectedSightEntityID, ObjectHandle_v3 obj);

  // VBS SQF routines
  void SQFSwitchCamera(const ObjectHandle_v3& object, CameraLinkType_v1 mode);
  void SQFDisableActionMenu();
  void SQFEnabledActionMenu();
  void SQFDisableUserInput();
  void SQFEnableUserInput();
  void QueryLaserRangeFinder(ObjectHandle_v3 _obj);
  void QueryLaserRangePosition(ObjectHandle_v3 _obj);

  void ConvertENUToLLA(const double& X, const double& Y, const double& Z);
  void ConvertLLAToENU();

private:
  bool enable_user_input = false;
  GeoPosition_v5 posresult = {};

  bool DDSinitialised = false;
  bool _mission_started, _in_mission;
  Vbs3ApplicationState_v1 state;
  bool initialiseDDS = false;
  bool initialiseDatabase = false;
  GeoPosition_v5 ownship_position = { 0, 0, 0 };
  ObjectHandle_v3 _selected = kNullObjectHandle;
  OSWindowAPI_v3* _main_window;
  ObjectHandle_v3 _dead_player = kNullObjectHandle;
  ObjectHandle_v3 _ownship = kNullObjectHandle;
  ObjectHandle_v3 _ownship_turret = kNullObjectHandle;
  ObjectHandle_v3 _nullhandle = kNullObjectHandle;
  int64_t _selectedMarker =0;
  int32_t _client_width = 0;
  int32_t _client_height = 0;

  bool32_t enable = true;
  bool32_t disable = false;

  std::vector<ObjectHandle_v3> _vbs_object_identifier;
  std::vector<long> _csa_object_identifier;
  std::map<long, ObjectData> _csa_object_data;
  std::vector<long> _object_with_turret;
  std::vector<ObjectHandle_v3> _turret_objects;
  std::vector<ObjectHandle_v3> _object_driver;
  std::vector<ObjectHandle_v3> _object_gunner;
  std::vector<ObjectHandle_v3> _object_commander;
  std::vector<ObjectHandle_v3> _tank_group;
  int _no_of_tanks = 0;
  ObjectHandle_v3 _vehicle = kNullObjectHandle;
  ObjectHandle_v3 _gunner = kNullObjectHandle;
  ObjectHandle_v3 _driver = kNullObjectHandle;
  ObjectHandle_v3 _commander = kNullObjectHandle;
  ObjectHandle_v3 _vehicle_turret = kNullObjectHandle;
  ObjectHandle_v3 _camera = kNullObjectHandle;
  ObjectHandle_v3 _second_shot = kNullObjectHandle;
  bool32_t gunner_enabled = false;
  bool32_t optics_enabled = false;

  //Weapon stuff
  int _rarden = -1;
  int _chaingun = -1;
  bool laserFiredfromKeyboard = false;
  bool ovTurretHeadingUpdated = false;
  // Values are set in CIOS
  unsigned short weaponID_rarden = 23;
  unsigned short weaponID_chaingun = 24;
  unsigned short ammotype_APDS = 18;
  unsigned short ammotype_HEI = 17;
  unsigned short ammotype_chaingun = 19;
  unsigned short ammotype_smoke = 20;
  // Values are offsets in the model file cae_FV510_base_x magazines array
  int32_t cae_mag_3rnd_30mm_APDS_rarden = 37;
  int32_t cae_mag_3rnd_30mm_HEI_rarden = 2;
  int32_t cae_mag_90rnd_30mm_APDS_rarden = 1;
  int32_t cae_mag_90rnd_30mm_HEI_rarden = 0;

  BallisticTrajectory_v3 trajectory = kBallisticTrajectory_low;
  RotationalAngles_v3 aiming_orientation = { 0,0,0 };
  TrajectoryInfo_v3 trajectory_info = { 0,0,0 };
  ObjectHandle_v3 shot_objectID = kNullObjectHandle;
  GeoPosition_v3 shot_positionv3 = { 0,0,0 };
  GeoPosition_v5 shot_Start_Positionv5 = { 0,0,0 };
  GeoPosition_v5 shot_Flying_Position = { 0,0,0 };
  GeoPosition_v5 Lastime_shot_Flying_Position = { 0,0,0 };
  GeoPosition_v5 detonation_position = { 0,0,0 };
  GeoPosition_v5 laser_hit_position = { 0,0,0 };
  float64_t laser_return_position[3] = { 0,0,0 };
  Vector3f64_v3 map_position = { 0,0,0 };
  bool shot_detonated = false;
  bool shot_fired = false;
  bool target_endpos = false;
  GeoPosition_v3 target_position_v3 = { 0,0,0 };
  ObjectHandle_v3  target_hit_ID = kNullObjectHandle;
  Vector3f32_v3 target_velocity = { 0,0,0 };
  int _num_shots_fired, _num_object_hits, _num_ricochets;
  // For drawing lines of shot
  std::vector<int64_t> _primitives;
  std::map<int64_t, GeoPosition_v3> _last_positions;
  bool draw_laser_lines = false;
  bool draw_hit_lines = false;

  unsigned long exconWeaponUpdateFireID = 0;
  unsigned long exconWeaponUpdateFireWeaponTypeID = 0;
  unsigned long exconWeaponUpdateFireAmmoTypeID = 0;
  unsigned long exconWeaponUpdateFireProjID = 0;
  unsigned long exconWeaponUpdateFireBurstID = 0;
  unsigned long exconWeaponUpdateFireBurstAmmoTypeID = 0;
  unsigned long exconWeaponUpdateFireBurstProjID = 0;
  unsigned long numOfProjectiles = 0;

  // Multi Line Of Sight 
  unsigned long EndposOffset = 0; 
  unsigned long numOfLOSReqIds[100];
  unsigned long LOSRequestID = 0;
  unsigned long LOSRequestStartID = 0;
  MissionFunctionRequest::S_ENU_POSITION ENURequestStartpos;
  std::vector<MissionFunctionRequest::S_ENU_POSITION> ENURequestEndpos;
  unsigned long LOSRequestEndID = 0;
  std::vector<unsigned long> LOSRequestEndposIDs;

  GeoPosition_v5 LLARequestStartpos;
  std::vector<GeoPosition_v5> LLARequestEndpos;

  // Intersection checks
  int32_t noOfIntersections = 1;
  IntersectionInput_v5 intersectionInput;
  Intersection_v5 intersectionOutput;
  GeoPosition_v5 intersectionPosition[100];
  ObjectHandle_v3 intersectionObject[100];
  Vector3f64_v3 intersectionLocal_position[100];
  float64_t intersectionDistance[100];
  IntersectionType_v4 intersectionType[100];
  
  // Sight stuff
  ObjectHandle_v3 sight_invis_object = kNullObjectHandle;
  const char* SQFSwitchCameraView_mode = nullptr;
  const char* kOpticsViewMode = "\"GUNNER\"";
  const char* kExternalViewMode = "\"EXTERNAL\"";
  const char* kInternalViewMode = "\"INTERNAL\"";
  int64_t _lrf_range;
  unsigned short initialiseSightToUnity = 0;
  unsigned short sightID_unity = 15;
  unsigned short sightID_x8 = 16;
  long last_selected_sight = 0;
  int32_t sight_device_index = 0;
  int32_t sight_mode_index = 0;

  // ECEF Lat Lon ENU stuff
  float64_t lat = 0.0;
  float64_t lon = 0.0;
  float64_t alt = 0.0;
  float64_t radsToMilliRads = 1000.0; 
  EntityUpdate::S_ENU_POSITION entityUpdatePosition = { 0.0,0.0,0.0 };
  GeoPosition_v5 latLongPosition = { 0.0 , 0.0, 0.0 };
  double ECEF_x = 0.0;
  double ECEF_y = 0.0;
  double ECEF_z = 0.0;
  double* ptr_ECEF_x = &ECEF_x;
  double* ptr_ECEF_y = &ECEF_y;
  double* ptr_ECEF_z = &ECEF_z;
  double ENU_x = 0.0;
  double ENU_y = 0.0;
  double ENU_z = 0.0;
  double* ptr_ENU_x = &ENU_x;
  double* ptr_ENU_y = &ENU_y;
  double* ptr_ENU_z = &ENU_z;

  GeoPosition_v5 publishedSightPosition = { 0.0 , 0.0, 0.0 };
  RotationalAngles_v3 publishedSightOrientation = { 0.0 , 0.0, 0.0 };

  void UpdateEntityTurretHeading(const ObjectHandle_v3& turretObject, const RotationalAngles_v3& currentEntityOrientation, RotationalAngles_v3& currentTurretOrientation, const double& relativeTurretHeading);
  void UpdateEntityWeaponElevation(const ObjectHandle_v3& turretObject, const RotationalAngles_v3& currentEntityOrientation, RotationalAngles_v3& currentTurretOrientation, const double& relativeWeaponElevation);
  void UpdateOVTurretHeading(const unsigned long ovCSAObjId, const double& relativeTurretHeading);
  void UpdateOVWeaponElevation(const unsigned long ovCSAObjId, const double& relativeWeaponElevation);

  void FireOVLaser(void);

};
