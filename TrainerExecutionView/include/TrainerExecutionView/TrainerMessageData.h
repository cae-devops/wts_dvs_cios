#pragma once

#ifndef TRAINERMESSAGEDATA_H_
#define TRAINERMESSAGEDATA_H_
#include <vector>
#include <map>
#include <set>
#include "EntityControl.h"
#include "WeaponControl.h"
#include "MissionFunctionRequest.h"
#include "MissionFunctionResponse.h"
#include "SightUpdate.h"
#include "LockObject.h"
#include "SharedPtr.h"

// Entity Control
extern bool addEntities;
extern std::vector<long> entityAddEntityTypeId;
extern std::vector<long> entityAddObjectIDs; 
extern std::vector<EntityControl::S_OBJECT_LOCATION_AND_HEADING> entityAddLocationAndHeading;
extern long entityRemoveObjectIdentifier;
extern std::vector<long> entityGoToObjectIdentifier;
extern std::vector<EntityControl::S_ECEF_POSITION> entityGoToLocation;
extern std::vector<double> entityGoToSpeed;
extern std::vector<double> entityGoToDwellTime;
extern unsigned long exconStopEntityMovementID;
extern unsigned long exconMakeInvulnerableEventID;
extern unsigned long exconMakeVulnerableEventID;
extern unsigned long exconKillEntityID;

extern unsigned short exconEntityDamageStatusID;
extern std::map<unsigned long, unsigned short> entityDamageInput;

// Exercise Control
extern std::string exconExerciseName;
extern std::string exconExerciseFile;
extern bool exconLoadExercise;
extern bool exconStartExercise;
extern bool exconStopExercise;
extern bool exconResumeExercise;
extern bool exconPauseExercise;
extern bool exconStartTrainingSession;
extern bool exconLeaveTrainingSession;
extern bool exconCourseComplete;
extern bool exconPhaseComplete;
extern std::string exconDisplayMessage;
extern std::vector<long> exconEntityIDs;
extern unsigned long exconOVEntityID;

//Turret Control
extern long exconTurretFireLaserID;
extern int exconTurretControlType;
extern unsigned long exconTurretRestockAmmoID; 

struct WeaponElevation
{
	unsigned long weaponID;
	double weaponElevation;
};

extern std::map<unsigned long, WeaponElevation> weaponElevationUpdateRequests;
extern bool weaponElevationUpdateRequestsPending;

extern bool turretHeadingUpdateRequestsPending;
extern std::map<unsigned long, double> turretHeadingUpdateRequests;

extern bool laserFireRequestsPending;
extern std::set<unsigned long> laserFireRequests;

//Sight Update
extern long exconSelectedSightEntityID;
extern long exconSelectedSightID;
extern unsigned long exconSightPosEntityId;
extern unsigned long exconSightPosSightId;
extern double exconBoreSightLineError;
extern double exconBoreSightElevationError;
typedef enum SightDeviceType
{
	kSightDeviceType_x8,
	kSightDeviceType_nvg,
	kSightDeviceType_unity,
} SightDeviceType;

//Weapon Control
extern unsigned short exconWeaponControlWeaponID; 
extern unsigned short exconWeaponControlFireID;
extern WeaponControl::S_POSITION_AND_ORIENTATION exconWeaponControlFireBarrelbase;
extern std::vector<unsigned long> exconWeaponUpdateFireBurstProjID;
extern double exconWeaponControlFireChassisHeading;
extern unsigned short exconWeaponControlSelectWeaponID;
extern unsigned short exconWeaponControlSelectAmmoTypeID;
extern unsigned long exconWeaponUpdateFireID;
extern unsigned long exconWeaponUpdateFireWeaponTypeID;
extern unsigned long exconWeaponUpdateFireAmmoTypeID;
extern unsigned long exconWeaponUpdateFireProjID;
extern unsigned long exconWeaponUpdateFireBurstID;
extern unsigned long exconWeaponUpdateFireBurstAmmoTypeID; 
extern bool exconWeaponAmmoChange;

// Mission Function
extern std::vector<unsigned long> missfuncnumOfLOSReqIds;
extern std::vector<unsigned long> missfuncLOSRequestID;
extern std::vector<unsigned long> missfuncLOSRequestStartID;
extern std::vector<MissionFunctionRequest::S_ENU_POSITION> missfuncLOSRequestStartpos;
extern std::vector<unsigned long> missfuncLOSRequestEndID;

extern std::vector<unsigned long> missfuncLOSRequestEndposIDs;
extern std::vector<MissionFunctionRequest::S_ENU_POSITION> missfuncLOSRequestEndpos; 

extern std::vector<unsigned long> missfuncCancelLOSRequestID;

// Conversion values
extern double radsToDegs;
extern double degsToRads;

//Critical section for protecting input
extern SharedPtr<LockObject> MessageLock;

#endif
