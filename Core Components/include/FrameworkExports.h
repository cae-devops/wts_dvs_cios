////////////////////////////////////////////////////////////////////////////
//
// JBTSC/0129 CC-PG � Crown copyright 2012
//
///////////////////////////////////////////////////////////////////////////
//
#ifndef _FrameworkExports_h_
#define _FrameworkExports_h_

#if defined(FRAMEWORK_EXPORTS)
#define FRAMEWORK __declspec(dllexport)
#else
#define FRAMEWORK __declspec(dllimport)
#endif

#endif // _FrameworkExports_h_