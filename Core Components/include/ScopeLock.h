////////////////////////////////////////////////////////////////////////////
//
// JBTSC/0129 CC-PG � Crown copyright 2012
//
///////////////////////////////////////////////////////////////////////////

#if !defined(EA_E9FD0C0B_4BF4_48c1_9B97_F815B49225D7__INCLUDED_)
#define EA_E9FD0C0B_4BF4_48c1_9B97_F815B49225D7__INCLUDED_

#include "FrameworkExports.h"
#include "SharedPtr.h"


class LockObject;

class FRAMEWORK ScopeLock
{
public:
    ScopeLock(SharedPtr<LockObject> accessLock);
    virtual ~ScopeLock();
private:
    SharedPtr<LockObject> mAccessLock;

};
#endif // !defined(EA_E9FD0C0B_4BF4_48c1_9B97_F815B49225D7__INCLUDED_)
