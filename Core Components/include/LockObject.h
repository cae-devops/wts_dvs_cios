////////////////////////////////////////////////////////////////////////////
//
// JBTSC/0129 CC-PG � Crown copyright 2012
//
///////////////////////////////////////////////////////////////////////////

#if !defined(EA_90EC1584_C735_4926_AACA_3447344298DE__INCLUDED_)
#define EA_90EC1584_C735_4926_AACA_3447344298DE__INCLUDED_

/**
 * A class to wrap the Windows Critical Section type
 */
#include "FrameworkExports.h"
#include <windows.h>

class FRAMEWORK LockObject
{

public:
	LockObject();
	virtual ~LockObject();

    void lock();
    void unlock();

private:
    CRITICAL_SECTION mAccessLock;

};
#endif // !defined(EA_90EC1584_C735_4926_AACA_3447344298DE__INCLUDED_)
