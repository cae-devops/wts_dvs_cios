////////////////////////////////////////////////////////////////////////////
//
// JBTSC/0129 CC-PG � Crown copyright 2012
//
///////////////////////////////////////////////////////////////////////////

#if !defined(EA_BC661B11_11C2_4af9_9EBE_008716C41334__INCLUDED_)
#define EA_BC661B11_11C2_4af9_9EBE_008716C41334__INCLUDED_

#include "Counted.h"
#include <stdexcept>

#pragma warning (disable:4251)

// Forward references
template<class X> class WeakPtr;
template<class Y> class SharedFromThis;

// Helper functions to allow classes to share themselves
template<class T>
void enableSharedFromThis(SharedFromThis<T>* shared,Counted* counter)
{
    if(shared != 0) shared->mWeakThis.internalAssign(shared,counter);
}

// This version is necesary for cases where the concrete class
// does not derive from SharedFromThis<T>
inline void enableSharedFromThis(...)
{
}

template<class T>
class SharedPtr
{
public:

    // The default constructor creates an SharedPtr in which the pointer to the counted
    // object is set to zero, making it in effect the null object.
    SharedPtr()
    :mCounted(0)
    ,mInstance(0)
    {
    }

    // The one-argument constructor stores the passed pointer, creates an instance of 
    // the counted class then increments the reference count on this object.
    explicit SharedPtr(T* newType)
    :mCounted(0)
    ,mInstance(0)
    {
        mCounted = new Counted();
        mCounted->getRef();
        mInstance = newType;
        enableSharedFromThis(newType,mCounted);
    }

    explicit SharedPtr(WeakPtr<T>& rhs)
    :mCounted(0)
    ,mInstance(0)
    {
        mInstance = rhs.mInstance;
        mCounted = rhs.mCounted;
        if(!isNull()) mCounted->getRef();
    }

    template<class U>
    explicit SharedPtr(WeakPtr<U>& rhs)
    :mCounted(0)
    ,mInstance(0)
    {
        if((mInstance = dynamic_cast<T*>(rhs.mInstance)) != 0)
        {
            mCounted = rhs.mCounted;
            if(!isNull()) mCounted->getRef();
        }
    }

    // For the copy constructor, we must store the pointer to the counted object from
    // the argument to the constructor, and increment its reference count: 
    SharedPtr(const SharedPtr<T>& rhs)
    :mCounted(0)
    ,mInstance(0)
    {
        if(!rhs.isNull())
        {
            rhs.mCounted->getRef();
            mCounted = rhs.mCounted;
            mInstance = rhs.mInstance;
        }
    }

    template<typename U>
    SharedPtr(const SharedPtr<U>& rhs)
    :mCounted(0)
    ,mInstance(0)
    {
        if((mInstance = dynamic_cast<T*>(rhs.mInstance)) != 0)
        {
            mCounted = rhs.mCounted;
            if(!isNull()) mCounted->getRef();
        }
    }

    // The destructor calls unBind to decrement the reference count and delete the
    // counted object when appropriate
    ~SharedPtr()
    {
        unBind();
    }

    // The assignment operator deals with self-assignment by incrementing the argument (rhs)
    // reference count before decrementing the count for the current (lhs) object.
    // This will leave the reference count unchanged and consequently prevent deletion of the counted instance. 
    SharedPtr<T>& operator=(const SharedPtr<T>& rhs)
    {
        if(!rhs.isNull()) rhs.mCounted->getRef();
        unBind();
        mInstance = rhs.mInstance;
        mCounted = rhs.mCounted;
        return *this;
    }
    template<typename U>
    SharedPtr<T>& operator=(const SharedPtr<U>& rhs)
    {
        unBind();
        if(!rhs.isNull())
        {
            rhs.mCounted->getRef();
            if((mInstance = dynamic_cast<T*>(rhs.mInstance)) != 0)
            {
                mCounted = rhs.mCounted;
            }
            else
            {
                // unsuccessful cast, so release the ref
                rhs.mCounted->freeRef();
                unBind();
            }
        }
        return *this;
    }

    SharedPtr<T>& operator=(const SharedFromThis<T>& rhs)
    {
        //if(!rhs.isNull()) rhs.mCountedInstance->getRef();
        unBind();
        mInstance = rhs.mWeakThis->mInstance;
        mCounted = rhs.mCounted;
        return *this;
    }

    // The overloaded dereferencing operator simply returns the object held in the
    // currently referenced counted instance. We throw an exception if an attempt
    // is made to apply the operator to a null object variable. 
    T* operator->()
    {
        if(isNull())
        {
            throw std::runtime_error("SharedPtr::operator-> Null pointer");
        }
        return mInstance;
    }
    const T* operator->() const
    {
        if(isNull())
        {
            throw std::runtime_error("SharedPtr::operator-> Null pointer");
        }
        return mInstance;
    }

    T& operator*() const
    {
        if(isNull())
        {
            throw std::runtime_error("SharedPtr::operator* Null pointer");
        }
        return *(mInstance);
    }

    // friend bool operator==(const SharedPtr<T>& lhs, const SharedPtr<T>& rhs)
    bool operator==(const SharedPtr<T>& rhs) const
    {
        return mInstance == rhs.mInstance;
    }

    // friend bool operator!=(const SharedPtr<T>& lhs, const SharedPtr<T>& rhs);
    bool operator!=(const SharedPtr<T>& rhs) const
    {
        return mInstance != rhs.mInstance;
    }

    //friend bool operator>(const SharedPtr<T>& lhs, const SharedPtr<T>& rhs);
    //friend bool operator<(const SharedPtr<T>& lhs, const SharedPtr<T>& rhs);
    bool operator<(const SharedPtr<T>& rhs) const
    {
        return mInstance < rhs.mInstance;
    }

    // ...and a setNull() function to lose the binding to the current object. 
    void setNull()
    {
        unBind();
    }

    bool isNull() const
    {
        return (mCounted == 0);
    }

private:

    // The member function unBind is called whenever the SharedPtr instance loses a
    // reference to the wrapped counted instance. If in the process the reference
    // count becomes zero, the wrapped instance can safely be deleted.
    // The SharedPtr's pointer to the counted instance is set to zero to indicate
    // that the current variable now references the Null object. 
    void unBind()
    {
        if(mCounted != 0 && mCounted->freeRef() == 0)
        {
            mCounted->getRef();
            // Delete the instance before the reference counter
            // in case the class is SharedFromThis and performs de-registration
            delete mInstance;
            delete mCounted;
            mCounted = 0;
            mInstance = 0;
        }
        else
        {
            mInstance = 0;
            mCounted = 0;
        }
    }

    Counted* mCounted;
    T* mInstance;
    template<typename Y> friend class WeakPtr;
    template<typename Y> friend class SharedPtr;

};

template<class T, class U> SharedPtr<T> dynamic_pointer_cast(const SharedPtr<U>& rhs)
{
    return SharedPtr<T>(dynamic_cast<T*>(rhs.mInstance));
}


// These should work but cause a LNK2001 unresolved external error
//template<class T>
//inline bool operator<(const SharedPtr<T>& lhs,const SharedPtr<T>& rhs)
//{
//    return lhs.mCountedInstance->mInstance < rhs.mCountedInstance->mInstance;
//}
//template<class T>
//bool operator == (const SharedPtr<T>& lhs,
//                const SharedPtr<T>& rhs)
//{
//    return lhs.mCountedInstance->mInstance == rhs.mCountedInstance->mInstance;
//    // or *(lhs.mCountedInstance->mInstance) == *(rhs.....)
//};
//template<class T>
//bool operator > (const SharedPtr<T>& lhs,
//               const SharedPtr<T>& rhs)
//{
//    return lhs.mCountedInstance->mInstance > rhs.mCountedInstance->mInstance;
//};
//
//template<class T>
//bool operator != (const SharedPtr<T>& lhs,
//                const SharedPtr<T>& rhs)
//{
//    return !(lhs == rhs);
//};

#endif // !defined(EA_BC661B11_11C2_4af9_9EBE_008716C41334__INCLUDED_)





