////////////////////////////////////////////////////////////////////////////
//
// JBTSC/0129 CC-PG � Crown copyright 2012
//
///////////////////////////////////////////////////////////////////////////

#if !defined(EA_9F718DCB_21C4_4f61_B8B6_4354973E8540__INCLUDED_)
#define EA_9F718DCB_21C4_4f61_B8B6_4354973E8540__INCLUDED_

#include "FrameworkExports.h"

template <class T> class SharedPtr;

class FRAMEWORK Counted
{
    template<typename T> friend class SharedPtr;

private:

    volatile long mSharedCount;

    Counted();

    ~Counted();

    long getRef();

    long freeRef();

    Counted& operator=(Counted& rhs);


};
#endif // !defined(EA_9F718DCB_21C4_4f61_B8B6_4354973E8540__INCLUDED_)
