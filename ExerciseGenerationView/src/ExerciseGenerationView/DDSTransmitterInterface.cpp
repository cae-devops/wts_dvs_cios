#define _DDS_DOMAIN_PART_INIT_BUILD_

#include "DDSTransmitterInterfaceExport.h"
#include "DDSTransmitterInterface.h"

// Transmitters
#include "ControlStatusTransmitter.h"
#include "EntityControlTransmitter.h"
#include "EntityUpdateTransmitter.h"
#include "ExerciseControlTransmitter.h"
#include "ExerciseControlResponseTransmitter.h"
#include "ExerciseGenerationTransmitter.h"
#include "ExerciseGenerationResponseTransmitter.h"
#include "StealthControlTransmitter.h"
#include "StealthUpdateTransmitter.h" 
#include "TurretUpdateTransmitter.h"
#include "TurretControlTransmitter.h"
#include "WeaponControlTransmitter.h"
#include "WeaponControlResponseTransmitter.h"
#include "WeaponUpdateTransmitter.h"

#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>


DDSTransmitterInterface::DDSTransmitterInterface(void) : domainId(1)
{ 
	/* create the participant - Transmitters */
	/* To customize participant QoS, use the configuration file USER_QOS_PROFILES.xml */

	transmitterParticipant = DDSTheParticipantFactory->create_participant(domainId, DDS_PARTICIPANT_QOS_DEFAULT,NULL, DDS_STATUS_MASK_NONE);

	controlStatusTransmitter		   = new ControlStatusTransmitter(transmitterParticipant);
	exerciseControlTransmitter         = new ExerciseControlTransmitter(transmitterParticipant);
	exerciseControlResponseTransmitter = new ExerciseControlResponseTransmitter(transmitterParticipant);
	exerciseGenerationTransmitter      = new ExerciseGenerationTransmitter(transmitterParticipant);
	exerciseGenerationResponseTransmitter = new ExerciseGenerationResponseTransmitter(transmitterParticipant);
	stealthControlTransmitter          = new StealthControlTransmitter(transmitterParticipant);
	stealthUpdateTransmitter           = new StealthUpdateTransmitter(transmitterParticipant);
	turretUpdateTransmitter            = new TurretUpdateTransmitter(transmitterParticipant);
	turretControlTransmitter           = new TurretControlTransmitter(transmitterParticipant);
	weaponControlTransmitter           = new WeaponControlTransmitter(transmitterParticipant);
	weaponControlResponseTransmitter   = new WeaponControlResponseTransmitter(transmitterParticipant);
	weaponUpdateTransmitter            = new WeaponUpdateTransmitter(transmitterParticipant);

}

ControlStatusTransmitter*              DDSTransmitterInterface::GetControlStatusTransmitter()
{
	return controlStatusTransmitter;
};

ExerciseControlTransmitter*            DDSTransmitterInterface::GetExerciseControlTransmitter()
{
	return exerciseControlTransmitter;
};

ExerciseControlResponseTransmitter*    DDSTransmitterInterface::GetExerciseControlResponseTransmitter()
{
	return exerciseControlResponseTransmitter;
};

ExerciseGenerationTransmitter*         DDSTransmitterInterface::GetExerciseGenerationTransmitter()
{
	return exerciseGenerationTransmitter;
};

ExerciseGenerationResponseTransmitter* DDSTransmitterInterface::GetExerciseGenerationResponseTransmitter()
{
	return exerciseGenerationResponseTransmitter;
};

StealthControlTransmitter*             DDSTransmitterInterface::GetStealthControlTransmitter()
{
	return stealthControlTransmitter;
};

StealthUpdateTransmitter*              DDSTransmitterInterface::GetStealthUpdateTransmitter()
{
	return stealthUpdateTransmitter;
};

TurretUpdateTransmitter*               DDSTransmitterInterface::GetTurretUpdateTransmitter()
{
	return turretUpdateTransmitter;
};

TurretControlTransmitter*              DDSTransmitterInterface::GetTurretControlTransmitter()
{
	return turretControlTransmitter;
};

WeaponControlTransmitter*              DDSTransmitterInterface::GetWeaponControlTransmitter()
{
	return weaponControlTransmitter;
};

WeaponControlResponseTransmitter*      DDSTransmitterInterface::GetWeaponControlResponseTransmitter()
{
	return weaponControlResponseTransmitter;
};

WeaponUpdateTransmitter*               DDSTransmitterInterface::GetWeaponUpdateTransmitter()
{
	return weaponUpdateTransmitter;
};





/* Shutdown and Delete Transmitter Participant and all its entities */
int DDSTransmitterInterface::TransmitterParticipantShutdown(void)
{
	DDS_ReturnCode_t retcode;
	int status = 0;

	std::cout << std::endl << "**** TransmitterParticipantShutdown ****" << std::endl;
	if (transmitterParticipant != NULL)
	{
		retcode = transmitterParticipant->delete_contained_entities();
		if (retcode != DDS_RETCODE_OK) 
		{
			std::cout << "TransmitterParticipantShutdown: delete_contained_entities() error" << retcode << std::endl;
			status = -1;
		}

		retcode = DDSTheParticipantFactory->delete_participant(transmitterParticipant);
		
		if (retcode != DDS_RETCODE_OK) 
		{
			std::cout << "TransmitterParticipantShutdown: delete_participant() error " << retcode << std::endl;
			status = -1;
		}
	}

	/* RTI Connext provides finalize_instance() method on
	domain participant factory for people who want to release memory used
	by the participant factory. Uncomment the following block of code for
	clean destruction of the singleton. */

	retcode = DDSDomainParticipantFactory::finalize_instance();
	
	if (retcode != DDS_RETCODE_OK) 
	{
		std::cout << "TransmitterParticipantShutdown: finalize_instance() error " << retcode << std::endl;
		status = -1;
	}
	return status;
}