#include "ExerciseGenerationView.h"
#include "Version.h"
#include "Gears.h"
#include "GenerationDirector.h"

//Note:
//This file is partially autogenerated
//It is safe to make modifications to most of the file, however please don't modify the API function signatures
APIResult GEARS_API Component_Initialize(_In_ const char* component_folder)
{
  //GEARS NOTE: Use this function to perform component initialization.
  //Access to other components is unavailable.
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API Component_OnStart(_In_ APIManager_v6* api_manager, _In_ NativeModuleHandle proxy_handle)
{
  //GEARS NOTE: Use this function to connect to other components.
  //Using the specified api_manager variable, this component can request access
  //to APIs that have been registered with Gears.

  //The Gears class will take care of requesting APIs for you. See Gears.h for more details.
  Gears::InitializeAPIs(api_manager, proxy_handle);

  return GenerationDirector::Get().Initialize(api_manager, proxy_handle);
}

APIResult GEARS_API Component_OnStop()
{
  //GEARS NOTE: Use this function to perform "last chance" functionality.
  //All components are still accessible.

  //Release all APIs and set them to nullptr.
  Gears::ReleaseAPIs();

  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API Component_Shutdown()
{
  //GEARS NOTE: Use this function to cleanup any resources that were allocated.
  //This component's shared library is about to be unloaded.
  //Access to other components is unavailable.
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API Component_IsParallelizable(_Out_ bool32_t* parallelizable)
{
  //GEARS NOTE: Set parallelizable to TRUE to enable this component's OnStart to be called from a spawned thread
  *parallelizable = FALSE;
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API Component_GetName(_Inout_ int32_t* name_length, _Out_opt_ char* name)
{
  //GEARS NOTE: Use this function to fill out the name string with the name of the component.
  //Defaulted to return the official component name
  static const char* kComponentName = "ExerciseGenerationView";

  if(name_length == nullptr)
  {
    return kAPIResult_ParamsInvalid;
  }

  if(name == nullptr)
  {
    *name_length = static_cast<int32_t>(strlen(kComponentName) + 1);
  }
  else
  {
    strcpy_s(name, *name_length, kComponentName);
  }
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API Component_GetVersion(_Inout_ int32_t* version_length, _Out_opt_ char* version)
{
  //GEARS NOTE: Use this function to fill out the version string with the version of the component.
  //Defaulted to return the auto generated/incremented version from Version.h
  static const char* kVersion = VERSION;

  if(version_length == nullptr)
  {
    return kAPIResult_ParamsInvalid;
  }

  if(version == nullptr)
  {
    *version_length = static_cast<int32_t>(strlen(kVersion) + 1);
  }
  else
  {
    strcpy_s(version, *version_length, kVersion);
  }
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API ApplicationListener_OnBeforeSimulation(_In_ float32_t delta)
{
	GenerationDirector::Get().OnBeforeSimulation(delta);
	return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API ApplicationListener_OnAfterSimulation(_In_ float32_t delta)
{
	GenerationDirector::Get().OnAfterSimulation(delta);
	return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API CollisionAspectListener_OnObjectCollision(_In_ ObjectHandle_v3 object, _In_ ObjectHandle_v3 trigger_object, _In_ CollisionEventType_v1 type, _In_ CollisionParameters_v1 params, _In_ CollisionParameters_v1 trigger_params)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API CollisionAspectListener_OnComponentCollision(_In_ ObjectHandle_v3 object, _In_ const char* component_name, _In_ ObjectHandle_v3 trigger_object, _In_ CollisionEventType_v1 type)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API DamageAspectListener_OnDamageChange(_In_ ObjectHandle_v3 object, _In_ ObjectHandle_v3 source, _In_ float32_t damage)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API DamageAspectListener_OnDestruction(_In_ ObjectHandle_v3 object, _In_ ObjectHandle_v3 source)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API DamageAspectListener_OnHit(_In_ const HitEvent_v2* hit_info)
{
  return kAPIResult_GeneralSuccess;
}

void GEARS_API DebugUIListener_OnRenderMainWindow()
{
	GenerationDirector::Get().OnRenderMainWindow();
}

void GEARS_API DebugUIListener_OnRenderCustomWindow()
{
}

void GEARS_API DebugUIListener_OnRenderHMDWindow()
{
}

void GEARS_API EnvironmentListener_OnTimeChanged(_In_ const DateTime_v3 time)
{
}

void GEARS_API EnvironmentListener_OnWindDirectionChanged(_In_ float32_t azimuth)
{
}

void GEARS_API EnvironmentListener_OnWindSpeedChanged(_In_ float32_t speed)
{
}

void GEARS_API EnvironmentListener_OnOvercastChanged(_In_ float32_t overcast)
{
}

void GEARS_API EnvironmentListener_OnFogChanged(_In_ float32_t density, _In_ float32_t decay, _In_ float32_t altitude)
{
}

void GEARS_API EnvironmentListener_OnRainChanged(_In_ float32_t density)
{
}

APIResult GEARS_API MissionListener_OnMissionStart(_In_ bool32_t restart)
{
	GenerationDirector::Get().OnMissionStart();
	return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API MissionListener_OnMissionEnd(_In_ bool32_t restart)
{
	GenerationDirector::Get().OnMissionEnd();
	return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API MissionListener_OnMissionLoad(_In_ const char* mission_name)
{
	GenerationDirector::Get().OnMissionLoad(mission_name);
	return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API MissionListener_OnMissionUnload(_In_ const char* mission_name)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API MissionListener_OnPlayerSwitchedUnits(_In_ ObjectHandle_v3 old_entity, _In_ ObjectHandle_v3 new_entity)
{
  return kAPIResult_GeneralSuccess;
}

BOOL GEARS_API OSServiceListener_OnMouseButtonPressed(WindowID window_id, ButtonID_v1 button_id, int32_t client_x, int32_t client_y, BOOL handled)
{
	GenerationDirector::Get().OnMouseButtonReleased(button_id, client_x, client_y);
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnMouseButtonReleased(WindowID window_id, ButtonID_v1 button_id, int32_t client_x, int32_t client_y, BOOL handled)
{
	GenerationDirector::Get().OnMouseButtonReleased(button_id, client_x, client_y);
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnMouseWheel(WindowID window_id, WheelID_v1 wheel_id, int32_t wheel_delta, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnKeyPressed(WindowID window_id, KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags, BOOL handled)
{
	GenerationDirector::Get().OnKeyPressed(key_id, system_key, scan_code, scan_flags);
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnKeyReleased(WindowID window_id, KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags, BOOL handled)
{
	GenerationDirector::Get().OnKeyReleased(key_id, system_key, scan_code, scan_flags);
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnChar(WindowID window_id, int32_t char_id, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnIMEChar(WindowID window_id, int32_t ime_char_id, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnActivate(WindowID window_id, BOOL activate_flag, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnClose(WindowID window_id, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnSize(WindowID window_id, int32_t client_width, int32_t client_height, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnSetCursor(WindowID window_id, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnRelativeMouseMove(WindowID window_id, int32_t move_x, int32_t move_y, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnAbsoluteMouseMove(WindowID window_id, int32_t client_x, int32_t client_y, BOOL handled)
{
  return FALSE;
}

BOOL GEARS_API OSServiceListener_OnMouseDoubleClick(WindowID window_id, ButtonID_v1 button_id, int32_t client_x, int32_t client_y, BOOL handled)
{
	GenerationDirector::Get().OnMouseDoubleClick(button_id, client_x, client_y);
  return FALSE;
}

APIResult GEARS_API OSWindowListener_OnMouseButtonPressed(_In_ OSWindowAPI_v3* api, _In_ ButtonID_v1 button_id, _In_ int32_t client_x, _In_ int32_t client_y, _In_ uint64_t flags, _Inout_ bool32_t* handled)
{
	GenerationDirector::Get().OnMouseButtonPressed(button_id, client_x, client_y);
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnMouseButtonReleased(_In_ OSWindowAPI_v3* api, _In_ ButtonID_v1 button_id, _In_ int32_t client_x, _In_ int32_t client_y, _In_ uint64_t flags, _Inout_ bool32_t* handled)
{
	GenerationDirector::Get().OnMouseButtonReleased(button_id, client_x, client_y);
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnMouseDoubleClick(_In_ OSWindowAPI_v3* api, _In_ ButtonID_v1 button_id, _In_ int32_t client_x, _In_ int32_t client_y, _In_ uint64_t flags, _Inout_ bool32_t* handled)
{
	GenerationDirector::Get().OnMouseDoubleClick(button_id, client_x, client_y);
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnMouseWheel(_In_ OSWindowAPI_v3* api, _In_ WheelID_v1 wheel_id, _In_ int32_t wheel_delta, _In_ uint64_t flags, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnRelativeMouseMove(_In_ OSWindowAPI_v3* api, _In_ int32_t move_x, _In_ int32_t move_y, _In_ uint64_t flags, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnAbsoluteMouseMove(_In_ OSWindowAPI_v3* api, _In_ int32_t client_x, _In_ int32_t client_y, _In_ uint64_t flags, _Inout_ bool32_t* handled)
{
	GenerationDirector::Get().OnMouseMove(client_x, client_y);
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnKeyPressed(_In_ OSWindowAPI_v3* api, _In_ KeyID key_id, _In_ bool32_t system_key, _In_ uint16_t scan_code, _In_ uint16_t scan_flags, _Inout_ bool32_t* handled)
{
	GenerationDirector::Get().OnKeyPressed(key_id, system_key, scan_code, scan_flags);
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnKeyReleased(_In_ OSWindowAPI_v3* api, _In_ KeyID key_id, _In_ bool32_t system_key, _In_ uint16_t scan_code, _In_ uint16_t scan_flags, _Inout_ bool32_t* handled)
{
	GenerationDirector::Get().OnKeyReleased(key_id, system_key, scan_code, scan_flags);
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnChar(_In_ OSWindowAPI_v3* api, _In_ int32_t char_id, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnIMEChar(_In_ OSWindowAPI_v3* api, _In_ int32_t ime_char_id, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnActivate(_In_ OSWindowAPI_v3* api, _In_ bool32_t activate_flag, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnClose(_In_ OSWindowAPI_v3* api, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnMove(_In_ OSWindowAPI_v3* api, _In_ int32_t screen_x, _In_ int32_t screen_y, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnSize(_In_ OSWindowAPI_v3* api, _In_ int32_t client_width, _In_ int32_t client_height, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnSetCursor(_In_ OSWindowAPI_v3* api, _In_ GUIRegion_v1 gui_region_code, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnGetMinMaxInfo(_In_ OSWindowAPI_v3* api, _Inout_ int32_t* min_window_width, _Inout_ int32_t* min_window_height, _Inout_ int32_t* max_window_width, _Inout_ int32_t* max_window_height, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnDropItems(_In_ OSWindowAPI_v3* api, _In_ const char** items, _In_ int32_t items_count, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnTouch(_In_ OSWindowAPI_v3* api, _In_ TouchEvent_v2 touch, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnWindowProcedure(_In_ OSWindowAPI_v3* api, _In_ NativeWindowHandle native_window_handle, _In_ uint32_t message, _In_ void* wparam, _In_ void* lparam, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnOLEDragEnter(_In_ OSWindowAPI_v3* api, _In_ int32_t screen_x, _In_ int32_t screen_y, _In_ void* data_object, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnOLEDragLeave(_In_ OSWindowAPI_v3* api, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API OSWindowListener_OnOLEDrop(_In_ OSWindowAPI_v3* api, _In_ int32_t screen_x, _In_ int32_t screen_y, _In_ void* data_object, _Inout_ bool32_t* handled)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API WeaponSystemAspectListener_OnFire(_In_ ObjectHandle_v3 shooter, _In_ ObjectHandle_v3 shot)
{
  return kAPIResult_GeneralSuccess;
}

APIResult GEARS_API WeaponSystemAspectListener_OnAmmoHit(_In_ const HitEvent_v2* hit_info)
{
  return kAPIResult_GeneralSuccess;
}

