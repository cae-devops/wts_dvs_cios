#define _DDS_DOMAIN_PART_INIT_BUILD_

#include "DDSReceiverInterfaceExport.h"
#include "DDSReceiverInterface.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "ndds/ndds_c.h"
#include "ndds/ndds_cpp.h"
#include "ControlStatusReceiver.h"
#include "EntityControlReceiver.h"
#include "EntityUpdateReceiver.h"
#include "ExerciseControlReceiver.h"
#include "ExerciseControlResponseReceiver.h"
#include "ExerciseGenerationReceiver.h"
#include "ExerciseGenerationResponseReceiver.h"
#include "StealthControlReceiver.h"
#include "StealthUpdateReceiver.h"
#include "TurretUpdateReceiver.h"
#include "TurretControlReceiver.h"
#include "WeaponControlReceiver.h"
#include "WeaponControlResponseReceiver.h"
#include "WeaponUpdateReceiver.h"


DDSReceiverInterface::DDSReceiverInterface() : domainId(1)
{ 
	/* create the participant - Receivers */
	/* To customize participant QoS, use the configuration file USER_QOS_PROFILES.xml */

	receiverParticipant = DDSTheParticipantFactory->create_participant(domainId, DDS_PARTICIPANT_QOS_DEFAULT, NULL, DDS_STATUS_MASK_NONE);

	controlStatusReceiver			   = new ControlStatusReceiver(receiverParticipant);
	entityControlReceiver              = new EntityControlReceiver(receiverParticipant);
	entityUpdateReceiver               = new EntityUpdateReceiver(receiverParticipant);
	exerciseControlReceiver            = new ExerciseControlReceiver(receiverParticipant);
	exerciseControlResponseReceiver    = new ExerciseControlResponseReceiver(receiverParticipant);
	exerciseGenerationReceiver         = new ExerciseGenerationReceiver(receiverParticipant);
	exerciseGenerationResponseReceiver = new ExerciseGenerationResponseReceiver(receiverParticipant);
	stealthControlReceiver             = new StealthControlReceiver(receiverParticipant);
	stealthUpdateReceiver              = new StealthUpdateReceiver(receiverParticipant);
	turretUpdateReceiver               = new TurretUpdateReceiver(receiverParticipant);
	turretControlReceiver              = new TurretControlReceiver(receiverParticipant);
	weaponControlReceiver              = new WeaponControlReceiver(receiverParticipant);
	weaponControlResponseReceiver      = new WeaponControlResponseReceiver(receiverParticipant);
	weaponUpdateReceiver               = new WeaponUpdateReceiver(receiverParticipant);
}


ControlStatusReceiver* DDSReceiverInterface::GetControlStatusReceiver()
{
	return controlStatusReceiver;
};

EntityControlReceiver* DDSReceiverInterface::GetEntityControlReceiver()
{
	return entityControlReceiver;
};

EntityUpdateReceiver* DDSReceiverInterface::GetEntityUpdateReceiver()
{
	return entityUpdateReceiver;
};

ExerciseControlReceiver* DDSReceiverInterface::GetExerciseControlReceiver()
{
	return exerciseControlReceiver;
};

ExerciseControlResponseReceiver* DDSReceiverInterface::GetExerciseControlResponseReceiver()
{
	return exerciseControlResponseReceiver;
};

ExerciseGenerationReceiver* DDSReceiverInterface::GetExerciseGenerationReceiver()
{
	return exerciseGenerationReceiver;
};
ExerciseGenerationResponseReceiver* DDSReceiverInterface::GetExerciseGenerationResponseReceiver()
{
	return exerciseGenerationResponseReceiver;
};
StealthControlReceiver* DDSReceiverInterface::GetStealthControlReceiver()
{
	return stealthControlReceiver;
};
StealthUpdateReceiver* DDSReceiverInterface::GetStealthUpdateReceiver()
{
	return stealthUpdateReceiver;
};
TurretUpdateReceiver* DDSReceiverInterface::GetTurretUpdateReceiver()
{
	return turretUpdateReceiver;
};
TurretControlReceiver* DDSReceiverInterface::GetTurretControlReceiver()
{
	return turretControlReceiver;
};
WeaponControlReceiver* DDSReceiverInterface::GetWeaponControlReceiver()
{
	return weaponControlReceiver;
};
WeaponControlResponseReceiver* DDSReceiverInterface::GetWeaponControlResponseReceiver()
{
	return weaponControlResponseReceiver;
};
WeaponUpdateReceiver* DDSReceiverInterface::GetWeaponUpdateReceiver()
{
	return weaponUpdateReceiver;
};


/* Shutdown and Delete Receiver Participant and all its entities */
int DDSReceiverInterface::ReceiverParticipantShutdown(void)
{
	DDS_ReturnCode_t retcode;
	int status = 0;

	std::cout << std::endl << "**** ReceivererParticipantShutdown ****" << std::endl;
	if (receiverParticipant != NULL)
	{
		retcode = receiverParticipant->delete_contained_entities();
		if (retcode != DDS_RETCODE_OK) 
		{
			std::cout << "ReceivererParticipantShutdown: delete_contained_entities() error" << retcode << std::endl;
			status = -1;
		}

		retcode = DDSTheParticipantFactory->delete_participant(receiverParticipant);
		
		if (retcode != DDS_RETCODE_OK) 
		{
			std::cout << "ReceivererParticipantShutdown: delete_participant() error " << retcode << std::endl;
			status = -1;
		}
	}

	/* RTI Connext provides finalize_instance() method on
	domain participant factory for people who want to release memory used
	by the participant factory. Uncomment the following block of code for
	clean destruction of the singleton. */

	retcode = DDSDomainParticipantFactory::finalize_instance();

	if (retcode != DDS_RETCODE_OK) 
	{
		std::cout << "ReceivererParticipantShutdown: finalize_instance() error" << retcode << std::endl;
		status = -1;
	}

	return status;
}