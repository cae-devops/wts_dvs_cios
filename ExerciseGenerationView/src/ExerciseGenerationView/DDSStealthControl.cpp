#include "DDSStealthControl.h"
#include "GenerationMessageData.h"
#include "ScopeLock.h"

// As we are currently not modelling a Stealth entity and a separate independant camera
// these concepts are currently combined and all message input and output are mapped accordingly

void DDSStealthControl::AlignStealthCameraToHeading(const double &Heading)
{
	ScopeLock lock(MessageLock);
	// " *** DDSStealthControl::AlignStealthCameraToHeading ***"  " Heading : " ;

	stealthHeading = Heading;
	stealthRotation = true;
}

void DDSStealthControl::AlignStealthCameraToLocation(const S_ECEF_POSITION& position)
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSStealthControl::AlignStealthCameraToLocation ***" << std::endl;
	//std::cout << "     Position X " << position.ECEF_X << std::endl;
	//std::cout << "     Position Y " << position.ECEF_Y << std::endl;
	//std::cout << "     Position Z " << position.ECEF_Z << std::endl;

	stealthAlignmentLocation = { position.ECEF_X, position.ECEF_Y, position.ECEF_Z };
	stealthAlignCamera = true;
}

void DDSStealthControl::AttachViewToCommandersSightOnEntity(const unsigned short& entityId)
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSStealthControl::AttachViewToCommandersSightOnEntity ***" << std::endl << "     EntityID : " << entityId << std::endl;
	//Needs more work here
//	setViewToEntityID = entityId;
}

void DDSStealthControl::AttachViewToGunnersSightOnEntity(const unsigned short& entityId)
{
	//NOT IMPLEMENTED
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSStealthControl::AttachViewToGunnersSightOnEntity ***" << std::endl << "     EntityID : " << entityId << std::endl;
}

void DDSStealthControl::AttachViewToStealthEntity(void)
{
	//NOT IMPLEMENTED
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSStealthControl::AttachViewToStealthEntity ***" << std::endl;
}

void DDSStealthControl::DisableTerrainFollowingMode(void)
{
	//NOT IMPLEMENTED
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSStealthControl::DisableTerrainFollowingMode ***" << std::endl;
}

void DDSStealthControl::EnableTerrainFollowingMode(void)
{
	//NOT IMPLEMENTED
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSStealthControl::EnableTerrainFollowingMode ***" << std::endl;
}

void DDSStealthControl::RepositionStealthBody(const S_ECEF_POSITION& position, const double& heading)
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSStealthControl::RepositionStealthBody ***" << std::endl;
	//std::cout << "     Position X " << position.ECEF_X << std::endl;
	//std::cout << "     Position Y " << position.ECEF_Y << std::endl;
	//std::cout << "     Position Z " << position.ECEF_Z << std::endl;
	//std::cout << "     Heading " << heading << std::endl;

	stealthObjectLocation = { position.ECEF_X, position.ECEF_Y, position.ECEF_Z };
	stealthHeading = heading;
	stealthRepositionCamera = true;
	stealthReorientateCamera = true;
}

void DDSStealthControl::UpdateStealthBodyControlDemands(const double& requiredSpeed, const double& headingRate, const double& pitchRate)
{
	ScopeLock lock(MessageLock);
	if (stealthSpeedInput != requiredSpeed)
	{
		stealthSpeedInput = requiredSpeed;
		stealthSpeedChange = true;
	}
	stealthHeadingRate.push_back(SmoothInput(headingRate));
	stealthPitchRate.push_back(SmoothInput(pitchRate));
	stealthRotation = true;
}

void DDSStealthControl::UpdateStealthCameraControlDemands(const double& headingRate, const double& pitchRate)
{
	ScopeLock lock(MessageLock);
	//  " *** DDSStealthControl::UpdateStealthCameraControlDemands ***" 
	// Input values 1 to -1
	stealthHeadingRate.push_back(SmoothInput(headingRate));
	stealthPitchRate.push_back(SmoothInput(pitchRate));
	stealthRotation = true;
}

void DDSStealthControl::UpdateStealthCameraElevationOffset(const double& heightOffset)
{
	ScopeLock lock(MessageLock);
	//  " *** DDSStealthControl::UpdateStealthCameraElevationOffset ***" 
	// Input values 1 to -1
	double adjustedHeightOffset = Interpolate(1.0, 0.0, heightOffset, 1.0, -1.0);
	stealthHeightOffset.push_back(adjustedHeightOffset);
	stealthHeightChange = true;
}

void DDSStealthControl::UpdateStealthCameraZoom(const double& zoomLevel)
{
	ScopeLock lock(MessageLock);
	// " *** DDSStealthControl::UpdateStealthCameraZoom ***" 
	// zoomlevel 0 to 1
	stealthZoomLevel.push_back(zoomLevel);
	stealthZoom = true;
}


double DDSStealthControl::Interpolate(const double& requiredRangeMax, const double& requiredRangeMin, const double& inputValue, const double& inputRangeMax,
										const double& inputRangeMin) const
{
	// calculate a value between requiredRangeMax & requiredRangeMin
	double outputValue = 0.0;
	// just to avoid divide by zero..
	if (inputRangeMax - inputRangeMin != 0.0)
		outputValue = (((inputValue - inputRangeMin) / (inputRangeMax - inputRangeMin)) * (requiredRangeMax - requiredRangeMin)) + requiredRangeMin;

	return outputValue;
}


double DDSStealthControl::SmoothInput(const double& inputValue) const
{
	double smoothedValue = inputValue * inputValue;
	if (inputValue < 0.0)
		smoothedValue *= -1.0;
	return smoothedValue;
}