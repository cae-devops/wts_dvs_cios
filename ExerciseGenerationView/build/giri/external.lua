-- ///////////////////////////////////
-- ///////////////////////////////////
-- ///////////////////////////////////
-- WARNING
-- This file has been auto-generated.
-- Do NOT make modifications directly to it as they will be overwritten!
-- ///////////////////////////////////
-- ///////////////////////////////////
-- ///////////////////////////////////

-- Project names:
local base_name = 'ExerciseGenerationView'
local component_project_name = base_name
local library_project_name = base_name .. 'Library'
local unittest_project_name = base_name .. 'UnitTest'

-- Projects:
if _ACTION == 'vs2010' then
externalproject( component_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '32a3cfc5-2864-4006-b44f-8bdf5141718d' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '1256e9d1-87b4-428c-ab7d-a87cc31588a1' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2010' )
  uuid ( '8653dca7-7d7c-4193-a801-60098ae999e7' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2012' then
externalproject( component_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '32a3cfc5-2864-4006-b44f-8bdf5141718d' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '1256e9d1-87b4-428c-ab7d-a87cc31588a1' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2012' )
  uuid ( '8653dca7-7d7c-4193-a801-60098ae999e7' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2013' then
externalproject( component_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '32a3cfc5-2864-4006-b44f-8bdf5141718d' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '1256e9d1-87b4-428c-ab7d-a87cc31588a1' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2013' )
  uuid ( '8653dca7-7d7c-4193-a801-60098ae999e7' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2015' then
externalproject( component_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '32a3cfc5-2864-4006-b44f-8bdf5141718d' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '1256e9d1-87b4-428c-ab7d-a87cc31588a1' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2015' )
  uuid ( '8653dca7-7d7c-4193-a801-60098ae999e7' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2017' then
externalproject( component_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '32a3cfc5-2864-4006-b44f-8bdf5141718d' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '1256e9d1-87b4-428c-ab7d-a87cc31588a1' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '8653dca7-7d7c-4193-a801-60098ae999e7' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

if _ACTION == 'vs2019' then
externalproject( component_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '32a3cfc5-2864-4006-b44f-8bdf5141718d' )
  kind ( 'SharedLib' )
  language ( 'C++' )

externalproject( library_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '1256e9d1-87b4-428c-ab7d-a87cc31588a1' )
  kind ( 'StaticLib' )
  language ( 'C++' )

externalproject( unittest_project_name )
  location ( project_root .. 'vs2017' )
  uuid ( '8653dca7-7d7c-4193-a801-60098ae999e7' )
  kind ( 'ConsoleApp' )
  language ( 'C++' )
end

