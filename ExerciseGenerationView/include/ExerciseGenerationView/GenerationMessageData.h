#pragma once

#ifndef GENERATIONMESSAGEDATA_H_
#define GENERATIONMESSAGEDATA_H_
#include <map>
#include <vector>
#include "EntityControl.h"
#include "ExerciseGeneration.h"
#include "StealthUpdate.h"
#include "LockObject.h"
#include "SharedPtr.h"

// Entity Control
extern bool addEntities;
extern std::vector<long> entityAddEntityTypeId;
extern std::vector<long> entityAddObjectIDs; 
extern std::vector<EntityControl::S_OBJECT_LOCATION_AND_HEADING> entityAddLocationAndHeading;
extern long entityRemoveObjectIdentifier;
extern std::vector<long> entityGoToObjectIdentifier;
extern std::vector<EntityControl::S_ECEF_POSITION> entityGoToLocation;
extern std::vector<double> entityGoToSpeed;
extern std::vector<double> entityGoToDwellTime;
extern unsigned long exconStopEntityMovementID;
extern unsigned long exconMakeInvulnerableEventID;
extern unsigned long exconMakeVulnerableEventID;
extern unsigned long exconKillEntityID;

extern unsigned short exconEntityDamageStatusID;
extern std::map<unsigned long, unsigned short> entityDamageInput;
extern bool exgenStart;
extern bool exgenLeave;
extern bool exconStartTrainingSession;

// Exercise Generation
extern std::vector < ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER> exgenAddObjectIdentifier;
extern std::vector<long> exgenObjectSubTypeId;
extern std::vector<ExerciseGeneration::S_OBJECT_LOCATION_AND_HEADING> exgenAddObjectLocationAndHeading;
extern std::vector<int> exgenRemoveObjectIDs;
extern std::vector<int> exgenAddObjectIDs;
extern ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER exgenUpdateObjectIdentifier;
extern ExerciseGeneration::S_OBJECT_LOCATION_AND_HEADING exgenUpdateObjectLocationAndHeading;
extern ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER exgenSelectObjectIdentifier;
extern ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER exgenDeselectObjectIdentifier; 
extern bool addObjects;
extern bool removeAllObjects;
extern std::string exgenTrackName;
extern std::string exgenTrackFile;
extern bool exgenPauseTrack;
extern bool exgenResumeTrack;
extern bool exgenStartTrack;
extern bool exgenStopTrack;

// Stealth Control
extern double stealthSpeedInput;
extern std::vector<double> stealthHeightOffset;
extern std::vector<double> stealthHeadingRate;
extern std::vector<double> stealthPitchRate;
extern std::vector<double> stealthZoomLevel;
extern double stealthHeading;
extern unsigned short setViewToEntityID;
extern bool stealthRepositionCamera;
extern bool stealthReorientateCamera;
extern bool stealthAlignCamera;
extern bool stealthHeightChange;
extern bool stealthSpeedChange;
extern bool stealthZoom;
extern bool stealthRotation;

extern StealthUpdate::S_ECEF_POSITION stealthObjectLocation;
extern StealthUpdate::S_ECEF_POSITION stealthAlignmentLocation;

// Conversion values
extern double radsToDegs;
extern double degsToRads;

//Critical section for protecting input
extern SharedPtr<LockObject> MessageLock;


#endif
