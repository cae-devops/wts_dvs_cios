#pragma once

#include "Gears.h"

#include <CommonTypesAPI.h>

#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstring>
#include <filesystem>
#include <functional>
#include <iterator>
#include <limits>
#include <random>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#ifdef _DEBUG
#define SDKCheck(function) assert(APIRESULT_SUCCESS(function) == TRUE)
#else
#define SDKCheck(function) function
#endif

namespace std
{
  template<> struct hash<ObjectHandle_v3>
  {
    size_t operator()(const ObjectHandle_v3& s) const
    {
      return hash<int64_t>()(s.id);
    }
  };
} // namespace std

static const auto PI = std::acos(-1.0); // Double Pi
static const auto PIf = std::acos(-1.0F); // Float Pi
static const auto FullCircleDeg = 360.0; // The arc of a circle in Double degrees
static const auto FullCircleDegf = 360.0F; // The arc of a circle in Float degrees
static const auto SemiCircleDeg = FullCircleDeg / 2.0; // The arc of a semicircle in Double degrees
static const auto SemiCircleDegf = FullCircleDegf / 2.0F; // The arc of a semicircle in Float degrees
static const auto Deg2Rad = PI / SemiCircleDeg; // The ratio of Double radians to degrees
static const auto Rad2Deg = SemiCircleDeg / PI; // The ratio of Double degrees to radians
static const auto M2Deg = 1.0 / 111111.0;

namespace details
{
  template<typename T>
  typename std::enable_if<std::is_same<T, float>::value || std::is_same<T, double>::value, T>::type nextafter(T param)
  {
    if (param < (std::numeric_limits<T>::max)())
    {
      typename std::conditional<sizeof(T) == sizeof(uint32_t), uint32_t, uint64_t>::type result;

      std::memcpy(reinterpret_cast<void*>(&result), reinterpret_cast<void*>(&param), sizeof(result));

      if (param > T(0))
      {
        ++result;
      }
      else
      {
        --result;
      }
      std::memcpy(reinterpret_cast<void*>(&param), reinterpret_cast<void*>(&result), sizeof(param));
    }
    return param;
  }

  template<typename T>
  typename std::enable_if<std::is_integral<typename std::remove_reference<T>::type>::value, T>::type RandomHelper(const T min, const T max, std::mt19937& param)
  {
    return std::uniform_int_distribution<T>(min, max)(param);
  }
  template<typename T>
  typename std::enable_if<std::is_floating_point<typename std::remove_reference<T>::type>::value, T>::type RandomHelper(const T min, const T max, std::mt19937& param)
  {
    return std::uniform_real_distribution<T>(min, nextafter(max))(param);
  }
  template<>
  inline bool RandomHelper<bool>(const bool min, const bool max, std::mt19937& param)
  {
    return std::uniform_int_distribution<int>(min, max)(param) != 0;
  }
} // namespace details

template<typename T>
typename std::enable_if<std::is_arithmetic<typename std::remove_reference<T>::type>::value, T>::type Random(const T min, const T max)
{
  if (min == max)
  {
    return min;
  }
  else
  {
    assert(min < max);

    const auto seed = std::random_device()();
    std::mt19937 param(seed);
    
    return details::RandomHelper(min, max, param);
  }
}

// Returns random color with alpha 1.
inline Color_v3 GetRandomColor()
{
  const Color_v3 result = { Random(0.0F, 1.0F), // r
                            Random(0.0F, 1.0F), // g
                            Random(0.0F, 1.0F), // b
                            1.0F };             // a

  return result;
}

// Converting between Color struct and color as float array.
inline bool ColorEdit4(const char* label, const bool32_t show_alpha, Color_v3* in_out_param)
{
  float32_t col[4U] = { in_out_param->r, in_out_param->g, in_out_param->b, in_out_param->a };
  bool32_t value_changed;

  SDKCheck(Gears::API::IMGuiAPIv1()->ColorEdit4(label, show_alpha, col, &value_changed));

  if(value_changed == FALSE)
  {
    return false;
  }
  else
  {
    in_out_param->r = col[0U];
    in_out_param->g = col[1U];
    in_out_param->b = col[2U];
    in_out_param->a = col[3U];
    return true;
  }
}

// Converting between Color struct and color as float array.
inline bool ColorEdit3(const char* label, Color_v3* in_out_param)
{
  float32_t col[3U] = { in_out_param->r, in_out_param->g, in_out_param->b };
  bool32_t value_changed;

  SDKCheck(Gears::API::IMGuiAPIv1()->ColorEdit3(label, col, &value_changed));

  if(value_changed == FALSE)
  {
    return false;
  }
  else
  {
    in_out_param->r = col[0U];
    in_out_param->g = col[1U];
    in_out_param->b = col[2U];
    return true;
  }
}

template<typename T>
T Clamp(const T val, const T min, const T max)
{
  return (std::min)(max, (std::max)(min, val));
}

#define GetVectorFromObjectFunc(vector, func, object) \
{ \
  int32_t count = 0; \
  SDKCheck(func(object, nullptr, &count)); \
  if (count != 0) {\
    vector.resize(count); \
    SDKCheck(func(object, &vector[0], &count)); \
  } \
}

template <class T> std::string GetString(T func)
{
  std::string str;
  int32_t size = 0;
  SDKCheck(func(nullptr, &size));
  str.resize(size);
  SDKCheck(func(&str[0], &size));
  return str.substr(0, str.size() - 1); // Remove ending 0
}

template <class Func, class Object> std::string GetString(Func func, Object object)
{
  std::string str;
  int32_t size = 0;
  SDKCheck(func(object, nullptr, &size));
  str.resize(size);
  SDKCheck(func(object, &str[0], &size));
  return str.substr(0, str.size() - 1); // Remove ending 0
}

inline bool operator == (const ObjectHandle_v3& a, const ObjectHandle_v3& b) { return a.id == b.id && a.type == b.type; }
inline bool operator != (const ObjectHandle_v3& a, const ObjectHandle_v3& b) { return a.id != b.id || a.type != b.type; }
inline bool operator < (const ObjectHandle_v3& a, const ObjectHandle_v3& b) { return a.type < b.type || a.type == b.type && a.id < b.id; }

inline double PseudoDistance(const GeoPosition_v5& pos1, const GeoPosition_v5& pos2)
{
  double latDiff = pos1.latitude - pos2.latitude;
  double longDiff = pos1.longitude - pos2.longitude;
  return std::sqrt(latDiff*latDiff + longDiff*longDiff);
}

// Returns value indicating whether the handle is valid and in case of invalid handle,
// the value of is set to kNullObjectHandle
inline bool ValidateHandle(ObjectHandle_v3& handle)
{
  bool32_t valid;
  SDKCheck(Gears::API::ObjectAPIv1()->IsValid(handle, &valid));
  if (!valid) handle = kNullObjectHandle;
  return valid != 0;
}

inline GeoPosition_v5 GetPosition(const ObjectHandle_v3& handle)
{
  GeoPosition_v5 result = {};

  SDKCheck(Gears::API::TransformationAspectAPIv4()->GetPosition(handle, &result));

  return result;
}

inline Vector3f64_v3 GetLocalPosition(const ObjectHandle_v3& handle)
{
	Vector3f64_v3 result = {};

	//SDKCheck(Gears::API::TransformationAspectAPIv4()->GetLocalPosition(handle, &result));
	Gears::API::TransformationAspectAPIv4()->GetLocalPosition(handle, &result);
	return result;
}


inline RotationalAngles_v3 GetOrientation(const ObjectHandle_v3& handle)
{
  RotationalAngles_v3 result = {};

  SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(handle, &result));

  return result;
}

// Gets world position of start and end of a ray originating in camera position and going through given pixel on screen.
inline std::pair<GeoPosition_v5, GeoPosition_v5> GetIntersectionLine(const ScreenPosition_v3& coords, const int32_t client_width, const int32_t client_height, const float length = 1000.0F)
{
  GeoPosition_v5 line_start = {};
  GeoPosition_v5 line_end = {};

  if (coords.x >= 0.0F && coords.x <= client_width && coords.y >= 0.0F && coords.y <= client_height)
  {
    SDKCheck(Gears::API::WorldAPIv5()->ScreenToWorldRay(coords, 0, length, kAltitudeReference_geoid, &line_start, &line_end));
  }

  return std::make_pair(line_start, line_end);
}

inline std::string ToLower(const char* param)
{
  const auto size = std::strlen(param);
  std::string result(size, '\0');
  
  std::transform(param, std::next(param, size), result.begin(), [](const unsigned char i) { return static_cast<char>(std::tolower(i)); });

  return result;
}

inline std::vector<std::string> GetFiles(const char* directory, const std::vector<std::string>& lowercase_extensions)
{
  std::vector<std::string> result;

#if _MSC_VER < 1900
  for (std::tr2::sys::directory_iterator it(directory); it != std::tr2::sys::directory_iterator(); ++it)
  {
    const auto file_name = it->path();

    if (!std::tr2::sys::is_directory(file_name))
    {
      const auto extension = ToLower(file_name.extension().data());

      if (std::find(lowercase_extensions.cbegin(), lowercase_extensions.cend(), extension) != lowercase_extensions.cend())
      {
        result.push_back(file_name.filename());
      }
    }
  }
#else
  for (std::experimental::filesystem::directory_iterator it(directory); it != std::experimental::filesystem::directory_iterator(); ++it)
  {
    const auto file_name = it->path();

    if (!std::experimental::filesystem::is_directory(file_name))
    {
      const auto extension = ToLower(file_name.extension().string().data());

      if (std::find(lowercase_extensions.cbegin(), lowercase_extensions.cend(), extension) != lowercase_extensions.cend())
      {
        result.push_back(file_name.filename().string());
      }
    }
  }
#endif
  return result;
}

class Sample
{
public:
  explicit Sample(const char* name) : _name(name)
  {
  }

  virtual ~Sample() {}
  const char* GetName() { return _name.c_str(); }

  virtual bool Init() { return true; }
  virtual void OnBeforeSimulation(float delta) {}
  virtual void OnAfterSimulation(float delta) {}
  virtual void OnMissionStart() {}
  virtual void OnMissionEnd() {}
  virtual void OnMissionLoad(const char* mission_name) {}
  virtual void Cleanup() { Gears::API::MissionAPIv3()->EndMission(); }

  virtual bool OnSize(const int32_t client_width, const int32_t client_height) { return false; }
  virtual bool OnMouseButtonPressed(ButtonID_v1 button_id, int32_t client_x, int32_t client_y) { return false; }
  virtual bool OnMouseButtonReleased(ButtonID_v1 button_id, int32_t client_x, int32_t client_y) { return false; }
  virtual bool OnMouseWheel(WheelID_v1 wheel_id, int32_t wheel_delta) { return false; }
  virtual bool OnKeyPressed(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags) { return false; }
  virtual bool OnKeyReleased(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags) { return false; }
  virtual bool OnMouseMove(const ScreenPosition_v3& client_xy) { return false; }
  virtual bool OnMouseDoubleClick(ButtonID_v1 button_id, int32_t client_x, int32_t client_y) { return false; }
  virtual void OnFire(ObjectHandle_v3 shooter, ObjectHandle_v3 shot) { }
  virtual void OnAmmoHit(const HitEvent_v2* hit_info) {}
  virtual void OnObjectHit(const HitEvent_v2* hit_info) {}
  virtual bool OnItemDrop(const char** items, int32_t items_count) { return false; }
  virtual void OnRenderMainWindow() { }

  virtual void OnObjectCollision(ObjectHandle_v3 object, ObjectHandle_v3 trigger_object, CollisionEventType_v1 type, CollisionParameters_v1 params, CollisionParameters_v1 trigger_params) {}
  virtual void OnObjectCreation(ObjectHandle_v3 object) {}
  virtual void OnObjectDeletion(ObjectHandle_v3 object) {}
  virtual void OnWaypointCompletion(ObjectHandle_v3 group, ObjectHandle_v3 waypoint) {}

  virtual std::string OnComponentObjectFunction(ObjectHandle_v3 object, const char* input) { return std::string(); }

private:
  std::string _name;
};
