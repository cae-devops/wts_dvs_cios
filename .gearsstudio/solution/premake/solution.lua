--
-- Copyright(c) 2020 Bohemia Interactive Simulations, Inc.
-- http://www.bisimulations.com
--
-- For information about the licensing and copyright of this software please
-- contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
--

-- ///////////////////////////////////
-- ///////////////////////////////////
-- ///////////////////////////////////
-- WARNING
-- This file has been auto-generated.
-- Do NOT make modifications directly to it as they will be overwritten!
-- ///////////////////////////////////
-- ///////////////////////////////////
-- ///////////////////////////////////

-- Custom Command Line Parameter: --solution_name
-- This parameter will contain the desired solution name.
newoption
{
  trigger     = 'solution_name',
  value       = 'string',
  description = 'The desired solution name'
}

-- Configuration settings:
config = {
  location = '../' .. _ACTION,
  debug_level = 0,
}
local solution_name = _OPTIONS['solution_name'] or 'GearsSolution'
local project_script = 'giri/external.lua'
local debug_targetsuffix = '_d'
local project_path_array = {
  ['ExerciseGenerationView'] = 'C:/Bohemia Interactive Simulations/Gears Studio/Products/WTS_DVS_CIOS-dev/ExerciseGenerationView/build/',
  ['TrainerExecutionView'] = 'C:/Bohemia Interactive Simulations/Gears Studio/Products/WTS_DVS_CIOS-dev/TrainerExecutionView/build/'
  }

--------------------------------------------------------
-- [Premake Override Begin]
-- Note: For this code to work, it must appear before
--       the Solution is defined.
--------------------------------------------------------

-- @brief FixTable
--        Modifies Premake filters in the following ways:
--        1. Any settings applied to the "Debug" configuration will also be applied to
--        configurations that match the "Debug*" filter.
--        2. Any settings applied to the "Release" configuration will also be applied to
--        configurations that match the "Release*" filter.
function FixTable(term_table)
  for i, term in ipairs(term_table) do
    if (type(term) == 'table') then
      FixTable(term_table[i])
    elseif (type(term) == 'string') then
      if (term:lower() == 'debug') then
        term_table[i] = 'Debug*'
      elseif (term:lower() == 'configurations:debug') then
        term_table[i] = 'configurations:Debug*'
      elseif (term:lower() == 'release') then
        term_table[i] = 'Release*'
      elseif (term:lower() == 'configurations:release') then
        term_table[i] = 'configurations:Release*'
      end
    end
  end
end

-- @brief Premake Override: configset.addFilter
--        Hooks into the Premake function "configset.addFilter" so that Gears can have
--        greater control over the way configurations are filtered.
premake.override(premake.configset, 'addFilter', function(base, cset, terms, basedir, unprefixed)
  if terms and type(terms) == 'table' then
    FixTable(terms)
  end
  return base(cset, terms, basedir, unprefixed)
end)

--------------------------------------------------------
-- [Premake Override End]
--------------------------------------------------------

-- @brief IncludePremakeFile
function IncludePremakeFile(premake_path)
  -- If the dependency path is valid...
  if true == os.isfile(premake_path) then
    -- Include the dependency settings.
    dofile( premake_path )
  end
end

-- Solution settings.
solution ( solution_name )
  -- These settings apply to all project within the solution.
  location( config.location )
  configurations { 'Debug', 'Release', 'ReleaseWithSymbols' }
  platforms { 'x64' }
  disablewarnings { '4100' }
  flags {
    'MultiProcessorCompile',                          -- Enable Visual Studio to use multiple compiler processes when building.
    'NoPCH',                                          -- Disable precompiled header support.
    }

  configuration { 'Debug' }
    targetsuffix( debug_targetsuffix )                -- Specify the file name suffix for the compiled binary target.
    optimize 'Off'                                    -- Specify the level and type of optimization used while building
                                                      -- the target configuration. Off == No optimization will be performed.
    symbols 'On'                                      -- Turn on debug symbol table generation.

  configuration { 'Release' }
    optimize 'Speed'                                  -- Specify the level and type of optimization used while building
                                                      -- the target configuration. Speed == Optimize for the best performance.
    flags {
      'LinkTimeOptimization',                         -- Enable link-time (i.e. whole program) optimizations.
      }

  configuration { 'ReleaseWithSymbols' }
    optimize 'Off'                                    -- Specify the level and type of optimization used while building
                                                      -- the target configuration. Off == No optimization will be performed.
    symbols 'On'                                      -- Turn on debug symbol table generation.
    runtime 'Release'                                 -- Specify the runtime library to use. In "ReleaseWithSymbols" configuration,
                                                      -- this must be set manually to avoid mismatched runtime errors.
    removeflags {
      'LinkTimeOptimization',                         -- Disable link-time (i.e. whole program) optimizations.
      }

  configuration {}                                    -- Clear the configuration in order to prevent setting misalignment.

  for component_name, project_path_iterator in pairs(project_path_array) do
    project_root = project_path_iterator

    group (component_name)                            -- Start a 'workspace group', a virtual folder to contain one or more projects.

    -- Also include the project settings.
    IncludePremakeFile(project_path_iterator .. project_script)

    group ('')                                        -- Clear the group in order to prevent project misalignment.
  end
